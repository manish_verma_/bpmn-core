package org.flowable.variable.api.types;

public class SecuredType {

    private String value;

    public SecuredType(String value) {
        if (value == null) {
            throw new NullPointerException();
        }
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    // Reference
    // https://www.mkyong.com/java/java-how-to-overrides-equals-and-hashcode/
    @Override
    public int hashCode() {
        return 31 * 17 + this.getValue().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof SecuredType))
            return false;
        SecuredType object = (SecuredType) obj;
        return this.getValue().equals(object.getValue());
    }
}
