package org.flowable.variable.api.persistence.entity;


public interface Globalvariable {

    String getDataType();
    void setDataType(String type);
    String getProcessIdentifier();
    void setProcessIdentifier(String processIdentifier);
    
}
