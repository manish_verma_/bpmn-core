/**
 * 
 */
package org.flowable.variable.service.impl.types;

import org.flowable.variable.api.types.SecuredType;
import org.flowable.variable.api.types.ValueFields;
import org.flowable.variable.api.types.VariableType;

import com.lendin.ib.security.BaseSecurity;

/**
 * @author manjunathch
 *
 */
public class EncryptedType implements VariableType {

    public static final String TYPE_NAME = "encrypted";
    private final int maxLength;
    private BaseSecurity<String> baseSecurity;

    public EncryptedType(int maxLength, BaseSecurity<String> baseSecurity) {
        this.maxLength = maxLength;
        this.baseSecurity = baseSecurity;
    }

    @Override
    public String getTypeName() {
        return TYPE_NAME;
    }

    @Override
    public boolean isCachable() {
        return false;
    }

    @Override
    public Object getValue(ValueFields valueFields) {
        if (valueFields.getTextValue() != null && !valueFields.getTextValue().isEmpty())
            // decrypt here
            return new SecuredType(baseSecurity.decrypt(valueFields.getTextValue())).getValue();
        else
            return valueFields.getTextValue();
    }

    @Override
    public void setValue(Object objValue, ValueFields valueFields) {
        String value = (objValue == null) ? null : ((SecuredType) objValue).getValue();
        // encrypt here
        if (value != null && !value.isEmpty())
            valueFields.setTextValue(baseSecurity.encrypt(value));
        else
            valueFields.setTextValue(value);
    }

    @Override
    public boolean isAbleToStore(Object value) {
        if (value == null) {
            return true;
        }
        if (SecuredType.class.isAssignableFrom(value.getClass())) {
            String stringValue = ((SecuredType) value).getValue();
            return stringValue.length() <= maxLength;
        }
        return false;
    }

}
