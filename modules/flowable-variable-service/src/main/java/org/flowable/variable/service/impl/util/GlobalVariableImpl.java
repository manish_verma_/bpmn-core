package org.flowable.variable.service.impl.util;

import org.flowable.variable.api.persistence.entity.Globalvariable;

public class GlobalVariableImpl implements Globalvariable {

    public String processIdentifier;
    public String DataType;
    
    @Override
    public String getProcessIdentifier() {
        return processIdentifier;
    }
    
    @Override
    public void setProcessIdentifier(String processIdentifier) {
        this.processIdentifier = processIdentifier;
    }
    @Override
    public String getDataType() {
        return DataType;
    }
    @Override
    public void setDataType(String dataType) {
        DataType = dataType;
    }
}
