package org.flowable.http.bpmn.ib;

import org.flowable.engine.test.Deployment;
import org.flowable.http.bpmn.HttpServiceTaskTestCase;
import org.flowable.task.api.Task;
import org.junit.jupiter.api.Test;

public class HttpServiceTaskIBIntegrationTest extends HttpServiceTaskTestCase {

    @Test
    @Deployment
    public void testpostUsage() {
        String procId = runtimeService.startProcessInstanceByKey("lallalnnsdfdrs").getId();
        Task task = taskService.createTaskQuery().processInstanceId(procId).active().list().get(0);
        Object value = taskService.getVariable(task.getId(), "IB_Status_Code");
        assertEquals("200", value);
        taskService.complete(task.getId());
        assertProcessEnded(procId);

    }
}