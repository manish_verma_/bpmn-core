package com.kuliza.lendin.http.json.handler;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.flowable.common.engine.api.FlowableException;
import org.flowable.common.engine.api.variable.VariableContainer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class IbJsonProcessor {

    public static String jsonBodyProcessor(String jsonBody, final VariableContainer exec) {
        if (jsonBody != null && jsonBody.isEmpty())
            return jsonBody;
        ObjectMapper mapper = new ObjectMapper();
        try {
            Map<String, Object> requestBodyMap = mapper.readValue(jsonBody, new TypeReference<Map<String, Object>>() {
            });
            mapIterateAndSubstute(requestBodyMap, exec);
            return new ObjectMapper().writeValueAsString(requestBodyMap);
        } catch (Exception e) {
            throw new FlowableException("Error occurred while processing http task in jsonBodyProcessor" + jsonBody, e);
        }
    }

    public static void mapIterateAndSubstute(Map<String, Object> mymap, final VariableContainer execution) {
        if (mymap.entrySet().isEmpty())
            return;
        for (Entry<String, Object> entry : mymap.entrySet()) {
            if (entry.getValue() instanceof String) {
                entry.setValue(execution.getVariable(entry.getValue().toString()));
            } else if (entry.getValue() instanceof List) {
                List list = (List) entry.getValue();
                for (Object object : list) {
                    mapIterateAndSubstute((Map<String, Object>) object, execution);
                }
            }

        }
    }

    public static void jsonResponseProcessor(String jsonResponseMap, String jsonApiResponse, VariableContainer exec) {
        if (jsonResponseMap != null && jsonResponseMap.isEmpty() && jsonApiResponse != null && jsonApiResponse.isEmpty())
            return;
        ObjectMapper mapper = new ObjectMapper();
        try {
            Map<String, Object> responseBodyMap = mapper.readValue(jsonResponseMap, new TypeReference<Map<String, Object>>() {
            });
            JsonNode apiResponseRootNode = new ObjectMapper().readTree(jsonApiResponse);
            mapIterateAndAddToProcess(responseBodyMap, apiResponseRootNode, exec);
            return;
        } catch (Exception e) {
            throw new FlowableException("Error occurred while processing http task in jsonResponseProcessor" + jsonApiResponse, e);
        }

    }

    public static void mapIterateAndAddToProcess(Map<String, Object> responsebodyMap, JsonNode rootNode, VariableContainer execution) {
        if (responsebodyMap.entrySet().isEmpty())
            return;
        for (Entry<String, Object> entry : responsebodyMap.entrySet()) {
            JsonNode dataNode = rootNode.path(entry.getValue().toString());
            if (dataNode.isMissingNode())
                continue;
            else if (dataNode.isContainerNode())
                try {
                    execution.setVariable(entry.getKey(), new ObjectMapper().writeValueAsString(dataNode));
                } catch (JsonProcessingException e) {
                    // TODO Auto-generated catch block
                    throw new FlowableException("Error occurred while processing http task in mapIterateAndAddToProcess" + rootNode, e);

                }
            else if (dataNode.isBoolean())
                execution.setVariable(entry.getKey(), dataNode.asBoolean());
            else if (dataNode.isLong())
                execution.setVariable(entry.getKey(), dataNode.asLong());
            else if (dataNode.isDouble())
                execution.setVariable(entry.getKey(), dataNode.asDouble());
            else
                execution.setVariable(entry.getKey(), dataNode.asText());

        }
    }
}
