package org.flowable.spring.boot;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author ManC
 */
@ConfigurationProperties(prefix = "lend.in")
public class LendInProperties {

    private String ibHostUrl;
    private String ibCompanySlug;
    private String ibPublicKey;
    private String ibInitVector;
    private String privateKey;

    public String getIbHostUrl() {
        return ibHostUrl;
    }

    public void setIbHostUrl(String ibHostUrl) {
        this.ibHostUrl = ibHostUrl;
    }

    public String getIbCompanySlug() {
        return ibCompanySlug;
    }

    public void setIbCompanySlug(String ibCompanySlug) {
        this.ibCompanySlug = ibCompanySlug;
    }

    public String getIbPublicKey() {
        return ibPublicKey;
    }

    public void setIbPublicKey(String ibPublicKey) {
        this.ibPublicKey = ibPublicKey;
    }

    public String getIbInitVector() {
        return ibInitVector;
    }

    public void setIbInitVector(String ibInitVector) {
        this.ibInitVector = ibInitVector;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

}
