package com.lendin.bpmn.services;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lendin.bpmn.exceptions.ResourceNotFoundException;
import com.lendin.bpmn.models.ProcessVariables;
import com.lendin.bpmn.models.ProcessVariables.Category;
import com.lendin.bpmn.repositories.ProcessVariableRepository;
import com.lendin.bpmn.utils.IDDataMixIn;

@Service
public class ProcessVariableService {

    @Autowired
    private ProcessVariableRepository processVariableRepository;
    @Autowired
    private ObjectMapper objectMapper;

    public List<ProcessVariables> bulkInsertOrUpdate(List<ProcessVariables> processVariables) {
        return processVariableRepository.saveAll(processVariables);
    }

    public List<ProcessVariables> getAll() {
        return processVariableRepository.findAll();
    }

    public ProcessVariables getById(long id) {
        return processVariableRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("ProcessVariables", "id", id));
    }

    public List<ProcessVariables> getByCategory(Category category) {
        return processVariableRepository.findByCategoryAndisDeleted(category);
    }

    public ProcessVariables addProcessVariable(ProcessVariables key) {
        return processVariableRepository.save(key);
    }

    public ProcessVariables updateProcessVariable(ProcessVariables key, long id) {
        getById(id);
        key.setId(id);
        return processVariableRepository.save(key);
    }

    public void removeProcessVariable(long id) {
        ProcessVariables variable = getById(id);
        variable.setProcessIdentifier(variable.getProcessIdentifier() + "deleted" + id);
        processVariableRepository.save(variable);
        processVariableRepository.deleteById(id);
    }

    public byte[] exportVariablesToJSON() throws JsonProcessingException {
        List<ProcessVariables> variablesList = getAll();
        objectMapper.addMixIn(ProcessVariables.class, IDDataMixIn.class);
        return objectMapper.writeValueAsBytes(variablesList);

    }

    @Transactional
    public ResponseEntity<String> importProductFromJSON(byte[] content) throws JsonParseException, JsonMappingException, IOException {
        // TODO Can improve performance here, but this functionality is used
        // only once, so not worrying now.
        List<ProcessVariables> variableObjects = objectMapper.readValue(content, new TypeReference<List<ProcessVariables>>() {
        });
        variableObjects.stream().forEach(processVariable -> {
            ProcessVariables variable = processVariableRepository.findOneByProcessIdentifierAndDeletedAt(processVariable.getProcessIdentifier(), 0);
            if (variable != null) {
                variable.setCategory(processVariable.getCategory());
                variable.setDataType(processVariable.getDataType());
                variable.setName(processVariable.getName());
                variable.setDescription(processVariable.getDescription());
                variable.setStepIndex(processVariable.getStepIndex());
                variable.setIsCreditVariable(processVariable.getIsCreditVariable());
                processVariableRepository.save(variable);
            } else {
                processVariableRepository.save(processVariable);
            }
        });
        return new ResponseEntity<String>("Imported successfully", HttpStatus.OK);

    }
}
