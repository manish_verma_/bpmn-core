package com.lendin.bpmn.services;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lendin.bpmn.exceptions.ResourceNotFoundException;
import com.lendin.bpmn.models.MetaKey;
import com.lendin.bpmn.models.ProcessVariables;
import com.lendin.bpmn.repositories.MetaKeyRepository;
import com.lendin.bpmn.utils.IDDataMixIn;

@Service
public class MetaKeyService {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MetaKeyRepository metaKeyRepository;

    public List<MetaKey> bulkInsertOrUpdate(List<MetaKey> metaKeys) {
        return metaKeyRepository.saveAll(metaKeys);
    }

    public List<MetaKey> getAll() {
        return metaKeyRepository.findAll();
    }

    public MetaKey getById(long id) {
        return metaKeyRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("MetaKey", "id", id));
    }

    public MetaKey addMetaKey(MetaKey key) {
        return metaKeyRepository.save(key);
    }

    public MetaKey updateMetaKey(MetaKey key, long id) {
        getById(id);
        key.setId(id);
        return metaKeyRepository.save(key);
    }

    public void removeMetaKey(long id) {
        getById(id);
        metaKeyRepository.deleteById(id);
    }

    public byte[] exportVariablesToJSON() throws JsonProcessingException {
        List<MetaKey> variablesList = getAll();
        objectMapper.addMixIn(ProcessVariables.class, IDDataMixIn.class);
        return objectMapper.writeValueAsBytes(variablesList);
    }

    public ResponseEntity<String> importProductFromJSON(byte[] content) throws JsonParseException, JsonMappingException, IOException {
        // TODO Can improve performance here, but this functionality is used
        // only once, so not worrying now.
        List<MetaKey> metaKeyObjects = objectMapper.readValue(content, new TypeReference<List<MetaKey>>() {
        });
        metaKeyObjects.stream().forEach(metaKeyObj -> {
            MetaKey metakey = metaKeyRepository.findOneByMetaKeyAndDeletedAt(metaKeyObj.getMetaKey(), 0);
            if (metakey != null) {
                metakey.setMetaJson(metaKeyObj.getMetaJson());
                metaKeyRepository.save(metakey);
            } else {
                metaKeyRepository.save(metaKeyObj);
            }
        });
        return new ResponseEntity<String>("Imported successfully", HttpStatus.OK);

    }
}
