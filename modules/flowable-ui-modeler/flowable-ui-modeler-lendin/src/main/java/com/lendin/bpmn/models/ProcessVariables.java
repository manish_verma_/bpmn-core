package com.lendin.bpmn.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonFilter;

@Entity
@Table(name = "kul_process_variables", uniqueConstraints = @UniqueConstraint(columnNames = { "processIdentifier", "deletedAt" }))
public class ProcessVariables extends BaseModel {

    public enum Category {
        FORMDATA, CONSTANT, API, OTHER
    }

    public enum DataTypes {
        STRING, BOOLEAN, DATE, DOUBLE, INTEGER, LONG, ENCRYPTED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, unique = true)
    private String processIdentifier;

    @Enumerated(EnumType.STRING)
    private Category category;

    private int stepIndex = 0;

    @Column(nullable = true)
    private String description;

    @Enumerated(EnumType.STRING)
    private DataTypes dataType;

    @Column(nullable = false)
    private int isCreditVariable = 0;

    public DataTypes getDataType() {
        return dataType;
    }

    public void setDataType(DataTypes dataType) {
        this.dataType = dataType;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProcessIdentifier() {
        return processIdentifier;
    }

    public void setProcessIdentifier(String processIdentifier) {
        this.processIdentifier = processIdentifier;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getStepIndex() {
        return stepIndex;
    }

    public void setStepIndex(int stepIndex) {
        this.stepIndex = stepIndex;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIsCreditVariable() {
        return isCreditVariable;
    }
    public void setIsCreditVariable(int isCreditVariable) {
        this.isCreditVariable = isCreditVariable;
    }
    public ProcessVariables() {
        super();
    }

    public ProcessVariables(long id, String name, String processIdentifier, Category category, int stepIndex, String description) {
        super();
        this.id = id;
        this.name = name;
        this.processIdentifier = processIdentifier;
        this.category = category;
        this.stepIndex = stepIndex;
        this.description = description;
    }
    public ProcessVariables(long id, String name, String processIdentifier, Category category, int stepIndex, String description, DataTypes dataType,
        int isCreditVariable) {
        this(id, name, processIdentifier, category, stepIndex, description);
        this.dataType = dataType;
        this.isCreditVariable = isCreditVariable;
    }

}
