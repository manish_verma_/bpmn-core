package com.lendin.bpmn.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lendin.bpmn.component.ConfigComponent;
import com.lendin.bpmn.utils.Constants;

@RestController
@RequestMapping(Constants.API_BASE_URI + Constants.CONFIG_URI)
public class GetConfigController {

    @Autowired
    private ConfigComponent configcomponnet;

    @GetMapping
    public ResponseEntity<Map<String, String>> retrieveAllMetaKeys() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("ib_url", configcomponnet.getIbHostUrl());
        map.put("company_slug", configcomponnet.getIbCompanySlug());
        map.put("company_id", configcomponnet.getIbCompanyId());
        map.put("masters_url", configcomponnet.getMastersHostUrl());
        return new ResponseEntity<Map<String, String>>(map, HttpStatus.OK);
    }

}
