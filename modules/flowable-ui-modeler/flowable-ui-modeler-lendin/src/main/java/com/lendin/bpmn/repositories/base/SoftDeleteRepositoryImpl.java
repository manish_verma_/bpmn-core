package com.lendin.bpmn.repositories.base;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

public class SoftDeleteRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID>
		implements SoftDeleteRepository<T, ID> {
	private EntityManager entityManager;
	private final JpaEntityInformation<T, ?> entityInformation;
	private static final String DELETED_FIELD = "deletedAt";

	public SoftDeleteRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
		super(entityInformation, entityManager);
		this.entityManager = entityManager;
		this.entityInformation = entityInformation;
	}

	@Transactional
	@Override
	public void deleteById(ID id) {
		T deleted = entityManager.find(this.getDomainClass(), id);
		if (deleted != null) {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();

			CriteriaUpdate<T> update = cb.createCriteriaUpdate((Class<T>) this.getDomainClass());

			Root<T> root = update.from((Class<T>) this.getDomainClass());

			update.set(DELETED_FIELD, System.currentTimeMillis() / 1000L);

			final List<Predicate> predicates = new ArrayList<Predicate>();

			if (entityInformation.hasCompositeId()) {
				for (String s : entityInformation.getIdAttributeNames())
					predicates.add(cb.equal(root.<ID>get(s),
							entityInformation.getCompositeIdAttributeValue(entityInformation.getId(deleted), s)));
				update.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
			} else
				update.where(cb.equal(root.<ID>get(entityInformation.getIdAttribute().getName()),
						entityInformation.getId(deleted)));

			entityManager.createQuery(update).executeUpdate();

		}
		return;
	}

}
