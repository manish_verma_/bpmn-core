package com.lendin.bpmn.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "kul_meta_key", uniqueConstraints = @UniqueConstraint(columnNames = { "metaKey", "deletedAt" }))
public class MetaKey extends BaseModel {

    public MetaKey() {
        super();
    }

    // GenerationType.IDENTITY doesn't support hibernate batch operations, so
    // using
    // SEQUENCE
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Column(nullable = false, unique = true)
    private String metaKey;

    @Lob
    private String metaJson;

    public String getMetaJson() {
        return metaJson;
    }

    public void setMetaJson(String metaJson) {
        this.metaJson = metaJson;
    }

    public String getMetaKey() {
        return metaKey;
    }

    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }

    public MetaKey(long id, String metaKey, String metaJson) {
        super();
        this.id = id;
        this.metaKey = metaKey;
        this.metaJson = metaJson;
    }
}
