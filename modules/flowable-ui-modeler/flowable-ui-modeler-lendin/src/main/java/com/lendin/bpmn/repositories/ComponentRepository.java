package com.lendin.bpmn.repositories;

import org.springframework.stereotype.Repository;

import com.lendin.bpmn.models.Component;
import com.lendin.bpmn.repositories.base.SoftDeleteRepository;

@Repository
public interface ComponentRepository extends SoftDeleteRepository<Component, Long> {

}
