package com.lendin.bpmn.component;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "lend.in")
@Configuration
public class ConfigComponent {

    private String ibHostUrl;
    private String ibCompanySlug;
    private String ibCompanyId;
    private String mastersHostUrl;

    public String getIbHostUrl() {
        return ibHostUrl;
    }
    public void setIbHostUrl(String ibHostUrl) {
        this.ibHostUrl = ibHostUrl;
    }
    public String getIbCompanySlug() {
        return ibCompanySlug;
    }
    public void setIbCompanySlug(String ibCompanySlug) {
        this.ibCompanySlug = ibCompanySlug;
    }
    public String getIbCompanyId() {
        return ibCompanyId;
    }
    public void setIbCompanyId(String ibCompanyId) {
        this.ibCompanyId = ibCompanyId;
    }
    public String getMastersHostUrl() {
        return mastersHostUrl;
    }
    public void setMastersHostUrl(String mastersHostUrl) {
        this.mastersHostUrl = mastersHostUrl;
    }
}
