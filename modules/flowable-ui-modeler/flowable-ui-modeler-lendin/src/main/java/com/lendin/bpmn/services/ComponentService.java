package com.lendin.bpmn.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lendin.bpmn.exceptions.ResourceNotFoundException;
import com.lendin.bpmn.models.Component;
import com.lendin.bpmn.repositories.ComponentRepository;

@Service
public class ComponentService {

	@Autowired
	private ComponentRepository componentRepo;

	public List<Component> bulkInsertOrUpdate(List<Component> components) {
		return componentRepo.saveAll(components);
	}

	public List<Component> getAll() {
		return componentRepo.findAll();
	}

	public Component getById(long id) {
		return componentRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Component", "id", id));
	}

	public Component addComponent(Component key) {
		return componentRepo.save(key);
	}

	public Component updateComponent(Component key, long id) {
		getById(id);
		key.setId(id);
		return componentRepo.save(key);
	}

	public void removeComponent(long id) {
		getById(id);
		componentRepo.deleteById(id);
	}

}
