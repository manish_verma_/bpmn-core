package com.lendin.bpmn.repositories.base;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.lendin.bpmn.utils.Constants;

/*
 * Only important repository methods are overridden here, in future based on
 * Requirements we may have to override here.  
 * 
 * */
@NoRepositoryBean
public interface SoftDeleteRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {

	@Override
	@Transactional
	@Query(Constants.QUERY_FIND_BY_ID)
	Optional<T> findById(@Param("id")ID id);

	@Override
	@Transactional(readOnly = true)
	@Query(Constants.QUERY_FIND_ALL)
	List<T> findAll();

	@Override
	@Transactional(readOnly = true)
	@Query(Constants.QUERY_COUNT)
	long count();

	void deleteById(@Param("id")ID id);
}
