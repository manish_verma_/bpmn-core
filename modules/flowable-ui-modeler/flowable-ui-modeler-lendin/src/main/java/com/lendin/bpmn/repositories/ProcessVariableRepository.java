package com.lendin.bpmn.repositories;

import com.lendin.bpmn.models.ProcessVariables;
import com.lendin.bpmn.models.ProcessVariables.Category;
import com.lendin.bpmn.repositories.base.SoftDeleteRepository;
import java.util.List;
import com.lendin.bpmn.utils.Constants;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessVariableRepository extends SoftDeleteRepository<ProcessVariables, Long> {

    @Query(Constants.GET_VARIABLE_BY_CATEGORY)
    List<ProcessVariables> findByCategoryAndisDeleted(@Param("category") Category category);
    ProcessVariables findOneByProcessIdentifierAndDeletedAt(String processIdentifier, int isDeleted);
}
