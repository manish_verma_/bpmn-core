package com.lendin.bpmn.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lendin.bpmn.models.Component;
import com.lendin.bpmn.services.ComponentService;
import com.lendin.bpmn.utils.Constants;

@RestController
@RequestMapping(Constants.API_BASE_URI + Constants.COMPONENT_URI)
public class ComponentController {

	@Autowired
	private ComponentService componentService;

	@GetMapping
	public ResponseEntity<List<Component>> retrieveAllComponents() {
		return new ResponseEntity<List<Component>>(componentService.getAll(), HttpStatus.OK);
	}

	// Get a single Component
	@GetMapping(Constants.ID_URL)
	public ResponseEntity<Component> retrieveComponent(@PathVariable long id) throws Exception {
		return new ResponseEntity<Component>(componentService.getById(id), HttpStatus.OK);
	}

	// Create a new Component
	@PostMapping
	public ResponseEntity<Component> createComponent(@RequestBody Component component) {
		return new ResponseEntity<Component>(componentService.addComponent(component), HttpStatus.CREATED);
	}

	// Update a Component
	@PutMapping(Constants.ID_URL)
	public ResponseEntity<Component> updateComponent(@RequestBody Component component, @PathVariable long id) {
		return new ResponseEntity<Component>(componentService.updateComponent(component, id), HttpStatus.OK);
	}

	// Delete a Component
	@DeleteMapping(Constants.ID_URL)
	public ResponseEntity<Component> deleteComponent(@PathVariable long id) {
		componentService.removeComponent(id);
		return new ResponseEntity<Component>(HttpStatus.NO_CONTENT);
	}

	@PostMapping(Constants.BULK_URI)
	public ResponseEntity<List<Component>> createComponent(@RequestBody List<Component> component) {
		return new ResponseEntity<List<Component>>(componentService.bulkInsertOrUpdate(component), HttpStatus.OK);
	}
}
