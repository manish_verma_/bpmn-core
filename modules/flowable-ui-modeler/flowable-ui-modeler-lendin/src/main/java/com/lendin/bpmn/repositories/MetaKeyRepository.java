package com.lendin.bpmn.repositories;

import org.springframework.stereotype.Repository;

import com.lendin.bpmn.models.MetaKey;
import com.lendin.bpmn.repositories.base.SoftDeleteRepository;

@Repository
public interface MetaKeyRepository extends SoftDeleteRepository<MetaKey, Long> {

    MetaKey findOneByMetaKeyAndDeletedAt(String metaKey, int isDeleted);

}
