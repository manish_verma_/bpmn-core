package com.lendin.bpmn.controllers;

import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lendin.bpmn.models.ProcessVariables;
import com.lendin.bpmn.models.ProcessVariables.Category;
import com.lendin.bpmn.services.ProcessVariableService;
import com.lendin.bpmn.utils.Constants;

@RestController
@RequestMapping(Constants.API_BASE_URI + Constants.PROCESS_VARIABLE_URI)
public class ProcessVariableController {

    private static final Logger logger = LoggerFactory.getLogger(ProcessVariableController.class);
    @Autowired
    private ProcessVariableService processVariableService;

    @GetMapping
    public ResponseEntity<List<ProcessVariables>> retrieveAllProcessVariables() {
        return new ResponseEntity<List<ProcessVariables>>(processVariableService.getAll(), HttpStatus.OK);
    }

    // Get a single ProcessVariable
    @GetMapping(Constants.ID_URL)
    public ResponseEntity<ProcessVariables> retrieveProcessVariables(@PathVariable long id) throws Exception {
        return new ResponseEntity<ProcessVariables>(processVariableService.getById(id), HttpStatus.OK);
    }

    // Get all ProcessVariables by Category
    @GetMapping(Constants.PROCESS_VARIABLE_CATEGORY_URL)
    public ResponseEntity<List<ProcessVariables>> retrieveComponentByCategory(@PathVariable Category category) {
        return new ResponseEntity<List<ProcessVariables>>(processVariableService.getByCategory(category), HttpStatus.OK);
    }

    // Create a new ProcessVariable
    @PostMapping
    public ResponseEntity<ProcessVariables> createProcessVariable(@RequestBody ProcessVariables processVariable) {
        return new ResponseEntity<ProcessVariables>(processVariableService.addProcessVariable(processVariable), HttpStatus.CREATED);
    }

    // Update a ProcessVariable
    @PutMapping(Constants.ID_URL)
    public ResponseEntity<ProcessVariables> updateProcessVariables(@RequestBody ProcessVariables processVariable, @PathVariable long id) {
        return new ResponseEntity<ProcessVariables>(processVariableService.updateProcessVariable(processVariable, id), HttpStatus.OK);
    }

    // Delete a ProcessVariable
    @DeleteMapping(Constants.ID_URL)
    public ResponseEntity<ProcessVariables> deleteProcessVariables(@PathVariable long id) {
        processVariableService.removeProcessVariable(id);
        return new ResponseEntity<ProcessVariables>(HttpStatus.NO_CONTENT);
    }

    @PostMapping(Constants.BULK_URI)
    public ResponseEntity<List<ProcessVariables>> createProcessVariableBulk(@RequestBody List<ProcessVariables> processVariables) {
        return new ResponseEntity<List<ProcessVariables>>(processVariableService.bulkInsertOrUpdate(processVariables), HttpStatus.OK);
    }

    @PostMapping(Constants.ANY_EXPORT)
    public ResponseEntity<byte[]> exportProcessVariable(HttpServletRequest request) {
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
        try {
            byte[] jsonBytes = processVariableService.exportVariablesToJSON();
            if (jsonBytes.length > 0) {
                HttpHeaders responseHeaders = new HttpHeaders();
                LocalDate date = LocalDate.now();
                responseHeaders.add("Content-Disposition", "attachment; filename=processVariables" + date + ".json");
                responseHeaders.add("Content-Type", "application/octet-stream");
                return new ResponseEntity<byte[]>(jsonBytes, responseHeaders, HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Not able to export processVariables", e);
        }
        return responseEntity;
    }
    @PostMapping(path = Constants.ANY_IMPORT, consumes = "multipart/form-data", produces = "application/json")
    public ResponseEntity<String> importProcessVariable(@RequestPart("file") @Valid MultipartFile file, HttpServletRequest request) {
        if (!file.isEmpty()) {
            try {
                return processVariableService.importProductFromJSON(file.getBytes());

            } catch (Exception e) {
                logger.error("Exception in import processVariables ", e);
            }
        }
        return new ResponseEntity<String>("Unable to import message", HttpStatus.NOT_FOUND);
    }
}
