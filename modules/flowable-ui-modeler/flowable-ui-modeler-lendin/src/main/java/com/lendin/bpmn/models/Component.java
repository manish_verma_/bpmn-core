package com.lendin.bpmn.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "kul_component", uniqueConstraints = @UniqueConstraint(columnNames = { "formType", "deletedAt" }))
public class Component extends BaseModel {

	// GenerationType.IDENTITY doesn't support hibernate batch operations, so using
	// SEQUENCE
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}

	public List<MetaKey> getMetaKeys() {
		return metaKeys;
	}

	public void setMetaKeys(List<MetaKey> metaKeys) {
		this.metaKeys = metaKeys;
	}

	@Column(nullable = false)
	private String formType;

	@Column(nullable = false)
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@ManyToMany
	@JoinTable(name = "kul_component_metakey", joinColumns = @JoinColumn(name = "component_id"), inverseJoinColumns = @JoinColumn(name = "metaKey_id"))
	private List<MetaKey> metaKeys = new ArrayList<>();

	public Component() {
		super();
	}

	public Component(long id, String name, String type, List<MetaKey> metaKeys) {
		super();
		this.id = id;
		this.formType = name;
		this.metaKeys = metaKeys;
		this.type = type;
	}
}
