package com.lendin.bpmn.utils;

public class Constants {

	// Don't let this class instance to be created anywhere.
	private Constants() {
		super();

	}

	public static final String JPA_TINY_INT_DEFAULT = "tinyint(1) default 0";
	public static final String JPA_ID_ASC = "id ASC";

	// DB related constants
	public static final String BOOLEAN_DEFAULT_FALSE = "boolean default false";

	// Queries for base model
	public static final String QUERY_FIND_ALL = "select e from #{#entityName} e where e.deletedAt = 0";
	public static final String QUERY_COUNT = "select count(e) from #{#entityName} e where e.deletedAt = 0";
	public static final String QUERY_DELETE = "update #{#entityName} e set e.deletedAt= :deletedAt where e.id = :id";
	public static final String QUERY_FIND_BY_ID = "select e from #{#entityName} e where e.id=:id and e.deletedAt = 0";
	// ********* Common Constants for All ******************
	public static final String EMPTY_STRING = "";

	public static final String ID_URL = "/{id}";
	public static final String API_BASE_URI = "/app/rest/api";
	public static final String META_KEY_URI = "/metakey";
	public static final String BULK_URI = "/bulk";
	public static final String COMPONENT_URI = "/component";
	public static final String CONFIG_URI = "/config";
	
	public static final String PROCESS_VARIABLE_URI = "/process-variable";
	public static final String PROCESS_VARIABLE_CATEGORY_URL = "/category/{category}";
	public static final String ANY_EXPORT ="/export-all";
    public static final String ANY_IMPORT ="/import-all";

	public static final Integer MAX_NAME_LENGTH = 100;

	public static final String LIST_META_KEYS = "List all meta keys";
	public static final String GET_META_KEY = "Get single meta key";
	public static final String CREATE_META_KEY = "Create meta key";
	public static final String UPDATE_META_KEY = "Update a meta key";
	public static final String FIELDS_META_KEY = "Fields needed to create meta key";
	public static final String META_KEY_ID_NOT_FOUND = "Company not found, id:";
	public static final String DELETE_META_KEY = "Delete a meta key";
	public static final String CREATE_BULK_META_KEY = "Create bulk meta key";

	public static final String GET_VARIABLE_BY_CATEGORY = "select e from #{#entityName} e where e.deletedAt = 0 and e.category = :category";
}
