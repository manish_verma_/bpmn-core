package com.lendin.bpmn.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lendin.bpmn.utils.Constants;

@MappedSuperclass
public class BaseModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@OrderBy(value = Constants.JPA_ID_ASC)
	private long id;

	@Column(nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date created;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date modified;

	@Column(nullable = false)
	private int deletedAt = 0;

	@JsonIgnore
	public int getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(int deletedAt) {
		this.deletedAt = deletedAt;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@JsonIgnore
	public Date getCreated() {
		return created == null ? null : (Date) created.clone();
	}

	@JsonIgnore
	public Date getModified() {
		return modified == null ? null : (Date) modified.clone();
	}

	@PrePersist
	public void prePersist() {
		created = new Date();
		modified = new Date();
	}

	@PreUpdate
	public void preUpdate() {
		modified = new Date();

	}
}
