package com.lendin.bpmn.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class IDDataMixIn {

    @JsonIgnore
    long id;
}
