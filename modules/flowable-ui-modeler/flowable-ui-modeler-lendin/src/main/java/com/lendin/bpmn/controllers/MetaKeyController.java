package com.lendin.bpmn.controllers;

import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lendin.bpmn.models.MetaKey;
import com.lendin.bpmn.services.MetaKeyService;
import com.lendin.bpmn.utils.Constants;

@RestController
@RequestMapping(Constants.API_BASE_URI + Constants.META_KEY_URI)
public class MetaKeyController {

    private static final Logger logger = LoggerFactory.getLogger(MetaKeyController.class);

    @Autowired
    private MetaKeyService metaKeyService;

    @GetMapping
    public ResponseEntity<List<MetaKey>> retrieveAllMetaKeys() {
        return new ResponseEntity<List<MetaKey>>(metaKeyService.getAll(), HttpStatus.OK);
    }

    // Get a single metaKey
    @GetMapping(Constants.ID_URL)
    public ResponseEntity<MetaKey> retrieveMetaKey(@PathVariable long id) throws Exception {
        return new ResponseEntity<MetaKey>(metaKeyService.getById(id), HttpStatus.OK);
    }

    // Create a new metaKey
    @PostMapping
    public ResponseEntity<MetaKey> createMetaKey(@RequestBody MetaKey metaKey) {
        return new ResponseEntity<MetaKey>(metaKeyService.addMetaKey(metaKey), HttpStatus.CREATED);
    }

    // Update a metaKey
    @PutMapping(Constants.ID_URL)
    public ResponseEntity<MetaKey> updateMetaKey(@RequestBody MetaKey metaKey, @PathVariable long id) {
        return new ResponseEntity<MetaKey>(metaKeyService.updateMetaKey(metaKey, id), HttpStatus.OK);
    }

    // Delete a metaKey
    @DeleteMapping(Constants.ID_URL)
    public ResponseEntity<MetaKey> deleteMetaKey(@PathVariable long id) {
        metaKeyService.removeMetaKey(id);
        return new ResponseEntity<MetaKey>(HttpStatus.NO_CONTENT);
    }

    @PostMapping(Constants.BULK_URI)
    public ResponseEntity<List<MetaKey>> createMetaKeyBulk(@RequestBody List<MetaKey> metaKeys) {
        return new ResponseEntity<List<MetaKey>>(metaKeyService.bulkInsertOrUpdate(metaKeys), HttpStatus.OK);
    }

    @PostMapping(Constants.ANY_EXPORT)
    public ResponseEntity<byte[]> exportProcessVariable(HttpServletRequest request) {
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
        try {
            byte[] jsonBytes = metaKeyService.exportVariablesToJSON();
            if (jsonBytes.length > 0) {
                HttpHeaders responseHeaders = new HttpHeaders();
                LocalDate date = LocalDate.now();
                responseHeaders.add("Content-Disposition", "attachment; filename=processVariables" + date + ".json");
                responseHeaders.add("Content-Type", "application/octet-stream");
                return new ResponseEntity<byte[]>(jsonBytes, responseHeaders, HttpStatus.OK);
            }
        } catch (Exception e) {
            logger.error("Not able to export processVariables", e);
        }
        return responseEntity;
    }
    @PostMapping(path = Constants.ANY_IMPORT, consumes = "multipart/form-data", produces = "application/json")
    public ResponseEntity<String> importProcessVariable(@RequestPart("file") @Valid MultipartFile file, HttpServletRequest request) {
        if (!file.isEmpty()) {
            try {
                return metaKeyService.importProductFromJSON(file.getBytes());

            } catch (Exception e) {
                logger.error("Exception in import processVariables ", e);
            }
        }
        return new ResponseEntity<String>("Unable to import message", HttpStatus.NOT_FOUND);
    }

}
