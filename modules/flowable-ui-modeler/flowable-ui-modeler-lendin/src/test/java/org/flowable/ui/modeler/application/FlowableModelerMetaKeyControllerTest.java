package org.flowable.ui.modeler.application;

import static org.junit.Assert.assertEquals;

import com.lendin.bpmn.controllers.MetaKeyController;
import com.lendin.bpmn.models.MetaKey;
import com.lendin.bpmn.services.MetaKeyService;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FlowableModelerMetaKeyControllerTest {


  String sampleMetaJson = "{\"type\":\"long\",\"form_type\":\"slider\",\"default\":\"70000\",\"order\":1,\"step\":10000,\"min\":5,\"max\":20,\"required\":true,\"disabled\":false,\"placeholder\":false,\"api\":{},\"validator\":\"[]\",\"colWidth\":12}";
  String sampleMetaKey = "loanAmount";
  String sampleType = "long";
  long sampleId = 1;

  MetaKey sampleMeta = new MetaKey(sampleId, sampleMetaKey, sampleMetaJson);
  List<MetaKey> sampleMetaList = Arrays.asList(sampleMeta);


  ResponseEntity<MetaKey> sampleMetaRetrieveResponse = new ResponseEntity<MetaKey>(sampleMeta, HttpStatus.OK);
  ResponseEntity<MetaKey> sampleMetaCreateResponse = new ResponseEntity<MetaKey>(sampleMeta, HttpStatus.CREATED);
  ResponseEntity<MetaKey> sampleMetaDeleteResponse = new ResponseEntity<MetaKey>(HttpStatus.NO_CONTENT);
  ResponseEntity<List<MetaKey>> sampleMetaListRetrieveResponse = new ResponseEntity<List<MetaKey>>(sampleMetaList, HttpStatus.OK);
  
  @Mock
  private MetaKeyService metaKeyServiceMock;

  @InjectMocks
  private MetaKeyController metaKeyController;

  @Test
  public void retrieveAllMetaKeysTest() {
    Mockito.when(metaKeyServiceMock.getAll()).thenReturn(sampleMetaList);
    assertEquals(sampleMetaListRetrieveResponse, metaKeyController.retrieveAllMetaKeys());
  }

  @Test
  public void retrieveComponentTest() throws Exception  {
    Mockito.when(metaKeyServiceMock.getById(sampleId)).thenReturn(sampleMeta);
    assertEquals(sampleMetaRetrieveResponse, metaKeyController.retrieveMetaKey(sampleId));
  }

  @Test
  public void createMetaKeyTest() {
    Mockito.when(metaKeyServiceMock.addMetaKey(sampleMeta)).thenReturn(sampleMeta);
    assertEquals(sampleMetaCreateResponse, metaKeyController.createMetaKey(sampleMeta));
  }

  @Test
  public void updateMetaKeyTest() {
    Mockito.when(metaKeyServiceMock.updateMetaKey(sampleMeta, sampleId)).thenReturn(sampleMeta);
    assertEquals(sampleMetaRetrieveResponse, metaKeyController.updateMetaKey(sampleMeta, sampleId));
  }

  @Test
  public void deleteMetaKeyTest() {
    assertEquals(sampleMetaDeleteResponse, metaKeyController.deleteMetaKey(sampleId));
  }

  @Test
  public void createMetaKeyBulkTest() {
    Mockito.when(metaKeyServiceMock.bulkInsertOrUpdate(sampleMetaList)).thenReturn(sampleMetaList);
    assertEquals(sampleMetaListRetrieveResponse, metaKeyController.createMetaKeyBulk(sampleMetaList));
  }


}
