package org.flowable.ui.modeler.application;

import static org.junit.Assert.assertEquals;

import com.lendin.bpmn.exceptions.ResourceNotFoundException;
import com.lendin.bpmn.models.Component;
import com.lendin.bpmn.models.MetaKey;
import com.lendin.bpmn.repositories.ComponentRepository;
import com.lendin.bpmn.services.ComponentService;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FlowableModelerComponentServiceTest {

//  @Autowired
//  private EntityManagerFactory entityManagerFactory;

  @Mock
  private EntityManager entityManagerMock;

//  EntityManager sampleEntityManager = entityManagerFactory.createEntityManager();

  String sampleMetaJson = "{\"type\":\"long\",\"form_type\":\"slider\",\"default\":\"70000\",\"order\":1,\"step\":10000,\"min\":5,\"max\":20,\"required\":true,\"disabled\":false,\"placeholder\":false,\"api\":{},\"validator\":\"[]\",\"colWidth\":12}";
  String sampleMetaKey = "loanAmount";
  String sampleType = "long";
  long sampleId = 1;
  MetaKey sampleMeta = new MetaKey(sampleId, sampleMetaKey, sampleMetaJson);
  List<MetaKey> sampleMetaList = Arrays.asList(sampleMeta);

  Component sampleComponent = new Component(sampleId, sampleMetaKey, sampleType, sampleMetaList);
  List<Component> sampleComponentList = Arrays.asList(sampleComponent);

  @Mock
  private ComponentRepository componentRepositoryMock;

  @InjectMocks
  private ComponentService componentService;

//  @Test
//  public void retrieveAllComponentsTest() throws Exception {
//    Mockito.when(entityManagerMock.merge(sampleComponent)).thenReturn(sampleComponent);
//    assertEquals(sampleComponentList, componentService.bulkInsertOrUpdate(sampleComponentList));
//  }

  @Test
  public void getAllTest() throws Exception {
    Mockito.when(componentRepositoryMock.findAll()).thenReturn(sampleComponentList);
    assertEquals(sampleComponentList, componentService.getAll());
  }

  @Test
  public void addComponentTest() throws Exception {
    Mockito.when(componentRepositoryMock.save(sampleComponent)).thenReturn(sampleComponent);
    assertEquals(sampleComponent, componentService.addComponent(sampleComponent));
  }

  @Test(expected = ResourceNotFoundException.class)
  public void getByIdTest() {
    Mockito.when(componentService.getById(sampleId)).thenThrow(new ResourceNotFoundException("Component", "id", sampleId));
  }

  @Test(expected = ResourceNotFoundException.class)
  public void updateProcessVariableTest() throws Exception {
    Mockito.when(componentRepositoryMock.save(sampleComponent)).thenReturn(sampleComponent);
    Mockito.when(componentService.getById(sampleId)).thenThrow(new ResourceNotFoundException("Component", "id", sampleId));
    assertEquals(sampleComponent, componentService.updateComponent(sampleComponent, sampleId));
  }

  @Test(expected = ResourceNotFoundException.class)
  public void removeComponentTest() throws Exception {
    Mockito.when(componentService.getById(sampleId)).thenThrow(new ResourceNotFoundException("Component", "id", sampleId));
  }
}
