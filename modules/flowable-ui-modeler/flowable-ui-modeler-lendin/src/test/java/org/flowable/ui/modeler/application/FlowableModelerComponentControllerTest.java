package org.flowable.ui.modeler.application;

import static org.junit.Assert.assertEquals;

import com.lendin.bpmn.controllers.ComponentController;
import com.lendin.bpmn.controllers.MetaKeyController;
import com.lendin.bpmn.models.Component;
import com.lendin.bpmn.models.MetaKey;
import com.lendin.bpmn.services.ComponentService;
import com.lendin.bpmn.services.MetaKeyService;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FlowableModelerComponentControllerTest {

  String sampleMetaJson = "{\"type\":\"long\",\"form_type\":\"slider\",\"default\":\"70000\",\"order\":1,\"step\":10000,\"min\":5,\"max\":20,\"required\":true,\"disabled\":false,\"placeholder\":false,\"api\":{},\"validator\":\"[]\",\"colWidth\":12}";
  String sampleMetaKey = "loanAmount";
  String sampleType = "long";
  long sampleId = 1;
  MetaKey sampleMeta = new MetaKey(sampleId, sampleMetaKey, sampleMetaJson);
  List<MetaKey> sampleMetaList = Arrays.asList(sampleMeta);

  Component sampleComponent = new Component(sampleId, sampleMetaKey, sampleType, sampleMetaList);
  List<Component> sampleComponentList = Arrays.asList(sampleComponent);

  ResponseEntity<MetaKey> sampleMetaRetrieveResponse = new ResponseEntity<MetaKey>(sampleMeta, HttpStatus.OK);
  ResponseEntity<List<MetaKey>> sampleMetaListRetrieveResponse = new ResponseEntity<List<MetaKey>>(sampleMetaList, HttpStatus.OK);

  ResponseEntity<Component> sampleComponentRetrieveResponse = new ResponseEntity<Component>(sampleComponent, HttpStatus.OK);
  ResponseEntity<Component> sampleComponentCreateResponse = new ResponseEntity<Component>(sampleComponent, HttpStatus.CREATED);
  ResponseEntity<Component> sampleComponentDeleteResponse = new ResponseEntity<Component>(HttpStatus.NO_CONTENT);
  ResponseEntity<List<Component>> sampleComponentListRetrieveResponse = new ResponseEntity<List<Component>>(sampleComponentList, HttpStatus.OK);

  @Mock
  private ComponentService componentServiceMock;

  @InjectMocks
  private ComponentController componentController;

  @Test
  public void retrieveAllComponentsTest() throws Exception {
    Mockito.when(componentServiceMock.getAll()).thenReturn(sampleComponentList);
    assertEquals(sampleComponentListRetrieveResponse, componentController.retrieveAllComponents());
  }

  @Test
  public void retrieveComponentTest() throws Exception  {
    Mockito.when(componentServiceMock.getById(sampleId)).thenReturn(sampleComponent);
    assertEquals(sampleComponentRetrieveResponse, componentController.retrieveComponent(sampleId));
  }

  @Test
  public void createComponentTest() {
    Mockito.when(componentServiceMock.addComponent(sampleComponent)).thenReturn(sampleComponent);
    assertEquals(sampleComponentCreateResponse, componentController.createComponent(sampleComponent));
  }

  @Test
  public void updateComponentTest() {
    Mockito.when(componentServiceMock.updateComponent(sampleComponent, sampleId)).thenReturn(sampleComponent);
    assertEquals(sampleComponentRetrieveResponse, componentController.updateComponent(sampleComponent, sampleId));
  }

  @Test
  public void deleteComponentTest() {
    assertEquals(sampleComponentDeleteResponse, componentController.deleteComponent(sampleId));
  }

  @Test
  public void createComponentListTest() {
    Mockito.when(componentServiceMock.bulkInsertOrUpdate(sampleComponentList)).thenReturn(sampleComponentList);
    assertEquals(sampleComponentListRetrieveResponse, componentController.createComponent(sampleComponentList));
  }


}
