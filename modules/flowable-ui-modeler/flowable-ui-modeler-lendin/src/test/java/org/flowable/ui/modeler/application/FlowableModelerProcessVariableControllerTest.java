package org.flowable.ui.modeler.application;

import static org.junit.Assert.assertEquals;

import com.lendin.bpmn.controllers.ProcessVariableController;
import com.lendin.bpmn.models.Component;
import com.lendin.bpmn.models.ProcessVariables;
import com.lendin.bpmn.models.ProcessVariables.Category;
import com.lendin.bpmn.services.ProcessVariableService;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FlowableModelerProcessVariableControllerTest {


  String sampleName = "loanAmount";
  String sampleDescription = "This is loan amount with type long to store final loan amount";
  String sampleProcessIdentifier = "testJourney";
  Category sampleCategory = Category.FORMDATA;
  int sampleStepIndex = 1;
  long sampleId = 1;
  ProcessVariables.DataTypes sampleDataType = ProcessVariables.DataTypes.STRING;
  int sampleIsCreditVariable = 0;

  ProcessVariables sampleProcessVariable = new ProcessVariables(sampleId, sampleName, sampleProcessIdentifier, sampleCategory, sampleStepIndex, sampleDescription,sampleDataType,sampleIsCreditVariable);
  List<ProcessVariables> sampleProcessVariableList = Arrays.asList(sampleProcessVariable);

  ResponseEntity<ProcessVariables> sampleProcessVariableRetrieveResponse = new ResponseEntity<ProcessVariables>(sampleProcessVariable, HttpStatus.OK);
  ResponseEntity<ProcessVariables> sampleProcessVariableCreateResponse = new ResponseEntity<ProcessVariables>(sampleProcessVariable, HttpStatus.CREATED);
  ResponseEntity<ProcessVariables> sampleProcessVariableDeleteResponse = new ResponseEntity<ProcessVariables>(HttpStatus.NO_CONTENT);
  ResponseEntity<List<ProcessVariables>> sampleProcessVariableListRetrieveResponse = new ResponseEntity<List<ProcessVariables>>(sampleProcessVariableList, HttpStatus.OK);

  @Mock
  private ProcessVariableService processVariableServiceMock;

  @InjectMocks
  private ProcessVariableController processVariableController;

  @Test
  public void retrieveAllProcessVariableKeysTest() {
    Mockito.when(processVariableServiceMock.getAll()).thenReturn(sampleProcessVariableList);
    assertEquals(sampleProcessVariableListRetrieveResponse, processVariableController.retrieveAllProcessVariables());
  }

  @Test
  public void retrieveComponentByCategoryTest() {
    Mockito.when(processVariableServiceMock.getByCategory(sampleCategory)).thenReturn(sampleProcessVariableList);
    assertEquals(sampleProcessVariableListRetrieveResponse, processVariableController.retrieveComponentByCategory(sampleCategory));
  }

  @Test
  public void createProcessVariableTest() {
    Mockito.when(processVariableServiceMock.addProcessVariable(sampleProcessVariable)).thenReturn(sampleProcessVariable);
    assertEquals(sampleProcessVariableCreateResponse, processVariableController.createProcessVariable(sampleProcessVariable));
  }

  @Test
  public void updateProcessVariablesTest() {
    Mockito.when(processVariableServiceMock.updateProcessVariable(sampleProcessVariable, sampleId)).thenReturn(sampleProcessVariable);
    assertEquals(sampleProcessVariableRetrieveResponse, processVariableController.updateProcessVariables(sampleProcessVariable, sampleId));
  }

  @Test
  public void deleteProcessVariablesTest() {
    assertEquals(sampleProcessVariableDeleteResponse, processVariableController.deleteProcessVariables(sampleId));
  }

  @Test
  public void createProcessVariableBulkTest() {
    Mockito.when(processVariableServiceMock.bulkInsertOrUpdate(sampleProcessVariableList)).thenReturn(sampleProcessVariableList);
    assertEquals(sampleProcessVariableListRetrieveResponse, processVariableController.createProcessVariableBulk(sampleProcessVariableList));
  }

}
