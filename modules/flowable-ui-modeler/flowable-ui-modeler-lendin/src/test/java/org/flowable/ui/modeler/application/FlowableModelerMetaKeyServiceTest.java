package org.flowable.ui.modeler.application;

import static org.junit.Assert.assertEquals;

import com.lendin.bpmn.exceptions.ResourceNotFoundException;
import com.lendin.bpmn.models.Component;
import com.lendin.bpmn.models.MetaKey;
import com.lendin.bpmn.repositories.ComponentRepository;
import com.lendin.bpmn.repositories.MetaKeyRepository;
import com.lendin.bpmn.services.MetaKeyService;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FlowableModelerMetaKeyServiceTest {

  String sampleMetaJson = "{\"type\":\"long\",\"form_type\":\"slider\",\"default\":\"70000\",\"order\":1,\"step\":10000,\"min\":5,\"max\":20,\"required\":true,\"disabled\":false,\"placeholder\":false,\"api\":{},\"validator\":\"[]\",\"colWidth\":12}";
  String sampleMetaKey = "loanAmount";
  String sampleType = "long";
  long sampleId = 1;

  MetaKey sampleMeta = new MetaKey(sampleId, sampleMetaKey, sampleMetaJson);
  List<MetaKey> sampleMetaList = Arrays.asList(sampleMeta);
  
  @Mock
  private MetaKeyRepository metaKeyRepositoryMock;

  @InjectMocks
  private MetaKeyService metaKeyService;

//  @Test
//  public void retrieveAllComponentsTest() throws Exception {
//    Mockito.when(entityManagerMock.merge(sampleComponent)).thenReturn(sampleComponent);
//    assertEquals(sampleComponentList, metaKeyService.bulkInsertOrUpdate(sampleComponentList));
//  }

  @Test
  public void getAllTest() throws Exception {
    Mockito.when(metaKeyRepositoryMock.findAll()).thenReturn(sampleMetaList);
    assertEquals(sampleMetaList, metaKeyService.getAll());
  }

  @Test
  public void addMetaKeyTest() throws Exception {
    Mockito.when(metaKeyRepositoryMock.save(sampleMeta)).thenReturn(sampleMeta);
    assertEquals(sampleMeta, metaKeyService.addMetaKey(sampleMeta));
  }

  @Test(expected = ResourceNotFoundException.class)
  public void getByIdTest() {
    Mockito.when(metaKeyService.getById(sampleId)).thenThrow(new ResourceNotFoundException("MetaKey", "id", sampleId));
  }

  @Test(expected = ResourceNotFoundException.class)
  public void updateProcessVariableTest() throws Exception {
    Mockito.when(metaKeyRepositoryMock.save(sampleMeta)).thenReturn(sampleMeta);
    Mockito.when(metaKeyService.getById(sampleId)).thenThrow(new ResourceNotFoundException("MetaKey", "id", sampleId));
    assertEquals(sampleMeta, metaKeyService.updateMetaKey(sampleMeta, sampleId));
  }

  @Test(expected = ResourceNotFoundException.class)
  public void removeComponentTest() throws Exception {
    Mockito.when(metaKeyService.getById(sampleId)).thenThrow(new ResourceNotFoundException("MetaKey", "id", sampleId));
  }

}
