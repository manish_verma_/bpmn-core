package org.flowable.ui.modeler.application;

import static org.junit.Assert.assertEquals;

import com.lendin.bpmn.exceptions.ResourceNotFoundException;
import com.lendin.bpmn.models.ProcessVariables;
import com.lendin.bpmn.models.ProcessVariables.Category;
import com.lendin.bpmn.repositories.MetaKeyRepository;
import com.lendin.bpmn.repositories.ProcessVariableRepository;
import com.lendin.bpmn.services.ProcessVariableService;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FlowableModelerProcessVariableServiceTest {

  String sampleName = "loanAmount";
  String sampleDescription = "This is loan amount with type long to store final loan amount";
  String sampleProcessIdentifier = "testJourney";
  Category sampleCategory = Category.FORMDATA;
  int sampleStepIndex = 1;
  long sampleId = 1;
  ProcessVariables.DataTypes sampleDataType = ProcessVariables.DataTypes.STRING;
  int sampleIsCreditVariable = 0;

  ProcessVariables sampleProcessVariable = new ProcessVariables(sampleId, sampleName, sampleProcessIdentifier, sampleCategory, sampleStepIndex, sampleDescription,sampleDataType,sampleIsCreditVariable);
  List<ProcessVariables> sampleProcessVariableList = Arrays.asList(sampleProcessVariable);

  @Mock
  private ProcessVariableRepository processVariableRepositoryMock;

  @InjectMocks
  private ProcessVariableService processVariableService;

//  @Test
//  public void retrieveAllComponentsTest() throws Exception {
//    Mockito.when(entityManagerMock.merge(sampleComponent)).thenReturn(sampleComponent);
//    assertEquals(sampleComponentList, processVariableService.bulkInsertOrUpdate(sampleComponentList));
//  }

  @Test
  public void getAllTest() throws Exception {
    Mockito.when(processVariableRepositoryMock.findAll()).thenReturn(sampleProcessVariableList);
    assertEquals(sampleProcessVariableList, processVariableService.getAll());
  }

  @Test
  public void addProcessVariableTest() throws Exception {
    Mockito.when(processVariableRepositoryMock.save(sampleProcessVariable)).thenReturn(sampleProcessVariable);
    assertEquals(sampleProcessVariable, processVariableService.addProcessVariable(sampleProcessVariable));
  }

  @Test
  public void getByCategoryTest() {
    Mockito.when(processVariableRepositoryMock.findByCategoryAndisDeleted(sampleCategory)).thenReturn(sampleProcessVariableList);
    assertEquals(sampleProcessVariableList, processVariableService.getByCategory(sampleCategory));
  }

  @Test(expected = ResourceNotFoundException.class)
  public void getByIdTest() {
    Mockito.when(processVariableService.getById(sampleId)).thenThrow(new ResourceNotFoundException("ProcessVariables", "id", sampleId));
  }

  @Test(expected = ResourceNotFoundException.class)
  public void updateProcessVariableTest() throws Exception {
    Mockito.when(processVariableRepositoryMock.save(sampleProcessVariable)).thenReturn(sampleProcessVariable);
    Mockito.when(processVariableService.getById(sampleId)).thenThrow(new ResourceNotFoundException("ProcessVariables", "id", sampleId));
    assertEquals(sampleProcessVariable, processVariableService.updateProcessVariable(sampleProcessVariable, sampleId));
  }

  @Test(expected = ResourceNotFoundException.class)
  public void removeComponentTest() throws Exception {
    Mockito.when(processVariableService.getById(sampleId)).thenThrow(new ResourceNotFoundException("ProcessVariables", "id", sampleId));
  }

}
