/* Copyright 2005-2015 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

"use strict";

var FLOWABLE = FLOWABLE || {};
var MASTER = MASTER || {};

var pathname = window.location.pathname
  .replace(/^(\/[^\/]*)(\/.*)?$/, "$1")
  .replace(/\/$/, "");

FLOWABLE.CONFIG = {
  onPremise: true,
  contextRoot: pathname,
  webContextRoot: pathname,
  datesLocalization: false
};

// ====== Master Config =======
var masterOriginUrl = `${window.location.origin}/masters-0.0.1-SNAPSHOT`;
// var masterOriginUrl = "http://dev-los.getlend.in:8080/masters-0.0.1-SNAPSHOT";

MASTER.CONFIG = {
  onPremise: true,
  contextRoot: masterOriginUrl,
  webContextRoot: masterOriginUrl,
  datesLocalization: false
};

function checkStatus(response) {
  console.debug('checkStatus response', response);

  if (
    (response.status >= 200 && response.status < 300) ||
    response.status === 401 ||
    response.status === 400
  ) {
    return response;
  }
  console.debug('error in checkStatus', response);
  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

function parseJSON(response) {
  if (
    response.status === 204 ||
    response.status === 205 ||
    response.status === 401
  ) {
    return { status: response.status };
  }
  return response.json();
  // return response.text().then(function(text) {
  //   return text ? JSON.parse(text) : {}
  // });
}


const getMasterUrl = async () => {
  const url = `${window.location.origin}/wf-pcp/app/rest/api/config`;
  // const url = `http://localhost:8090/wf-pcp/app/rest/api/config`;
  const options = {
    method: 'GET',
  };
  const response = await fetch(url, options)
    .then(checkStatus)
    .then(parseJSON)
    .then(parsedJSON => {
      console.log(`Fetching ${url} with`, options, 'Response -->', parsedJSON);
      return parsedJSON;
    })
    .catch(error => {
      console.debug(`Fetching ${url} with`, options, 'API error --->', error);
    });

  masterOriginUrl = response.masters_url;
  MASTER.CONFIG = {
    onPremise: true,
    contextRoot: masterOriginUrl,
    webContextRoot: masterOriginUrl,
    datesLocalization: false
  };
}

getMasterUrl();