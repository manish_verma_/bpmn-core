/**
 *
 * CheckboxField
 *
 */

import React from 'react';
import { Checkbox } from 'antd';

export default class CheckboxField extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    const { label, value, disabled } = this.props;
    return <Checkbox disabled={disabled} checked={value}>{label}</Checkbox>;
  }
}

