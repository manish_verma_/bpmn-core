/**
 *
 * Asynchronously loads the component for ConfigureHttpTask
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
