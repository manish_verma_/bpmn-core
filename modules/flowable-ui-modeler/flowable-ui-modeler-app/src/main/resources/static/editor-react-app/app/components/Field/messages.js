/*
 * Field Messages
 *
 * This contains all the text for the Field component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.Field';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the Field component!',
  },
});
