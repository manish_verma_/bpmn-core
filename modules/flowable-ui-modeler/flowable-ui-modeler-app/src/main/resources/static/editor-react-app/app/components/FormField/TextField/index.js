/**
 *
 * TextField
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { Input, InputNumber } from 'antd';
import Field from 'components/Field';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

function TextField(props) {
  const {
    floatingLabelText: labelSecondary,
    errorMessage,
    helperText,
    label,
    ...otherProps
  } = props;
  let InputComponent = otherProps.type === 'number' ? InputNumber : Input;

  return (
    <Field
      label={label || labelSecondary}
      errorMessage={errorMessage}
      helperText={helperText}
    >
      <InputComponent {...otherProps} />
    </Field>
  );
}

TextField.propTypes = {};

export default TextField;
