/**
 *
 * ConfigureHttpTask
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { Drawer, Input, Select, Spin, Button, Icon, Popconfirm, Dropdown, Menu, Tooltip } from 'antd';
const {Option} = Select;

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectConfigureHttpTask from './selectors';
import reducer from './reducer';
import saga from './saga';

import * as commonUtils from '../../utilities/commonUtils';
import * as notifUtils from '../../utilities/notificationUtils';
import * as appActions from '../AppWrapper/actions';

import { LinkText, iconStyles, wrapRowStyle, wrapColWithIconStyle, headingHintText } from './styles';
import A from '../../components/A';
import { setGlobalVariablesList } from '../../utilities/constants';
import cloneDeep from 'lodash/cloneDeep';

const extractIBHost = (ibURL) => {
    var subArr  = ibURL.split('/');
    return subArr[0] + '//' + subArr[2];
}

const errorNotifObj = {
  type: 'error',
  messages: {
    title: 'Error!',
    description:
      'There is some error in completing the request. Please check the data before submitting',
  },
};

/* eslint-disable react/prefer-stateless-function */
export class ConfigureHttpTask extends React.PureComponent {


  constructor(props) {
    super(props);
    this.state = {
      drawerVisible: false,
      gVariables: [],
      ibConsts: {},
      integrations: [],
      apis: [],
      selectedIntegration: {},
      selectedAPI: {},
      itgFetching: false,
      apisFetching: false,
      apiDetailsFetching: false,
      
      integrationObj: {},

      availableResponseBodyKeys: [],
      availableResponseBodyKeys: [],
      customRequestMapping: [],
      customResponseMapping: []
    };
  }

  componentDidMount() {
    // Get all the IB constants
    this.getIBConsts();
    // get the current available list of variables
    this.getGlobalVariables();
  }


  // Process the received integrationObj in respective handlers (if these property exist)
  // {
  //     integrationSlug: 'string',
  //     apiSlug: 'string',
  //     requestBodyMap: 'stringified JSON',
  //     responseBodyMap: 'stringified JSON'
  // }

  resetDrawer = () => {
    // reset whole selected state properties
    this.setState({
      apis: [],
      selectedIntegration: {},
      selectedAPI: {},
      requestBodyMap: {},
      responseBodyMap: {},
      availableRequestBodyKeys: [],
      availableResponseBodyKeys: [],
      customRequestMapping:[],
      customResponseMapping: []
    });
  }

  getBodyObject = (objList) => {
    const bodyObj = {};

    objList.forEach((obj) => {
      if (obj.key) {
        bodyObj[obj.key] = obj.value;
      }
    });
    return bodyObj;
  }

  saveIntegrationObject = () => {
    // Build the object to update the integration configuration
    const updateObj = {
      integrationSlug: this.state.selectedIntegration.slug || '',
      apiSlug: this.state.selectedAPI.slug || '',
      requestBodyMap: JSON.stringify(this.getBodyObject(this.state.customRequestMapping)),
      responseBodyMap: JSON.stringify(this.getBodyObject(this.state.customResponseMapping)),
      requestMethod: this.state.selectedAPI.method,
    }
    // Call the parental callback function to set the respective properties into scope
    this.props.updateIntegrationConfig({...updateObj});
    this.setState({drawerVisible: false});
  }

  intialiseSelectedIntegration = () => {
    const { integrations, integrationObj } = this.state;
    if (integrationObj && !!integrationObj.integrationSlug) {
      const selectedIntegration = integrations.filter((integration) => integration.slug === integrationObj.integrationSlug)[0] || {};
      this.onIntegrationSelect(selectedIntegration.id);
    } else {
      this.resetDrawer();
    }
  }


  // Get all the available integrations for the company
  getAvailableIntegrations = () => {
    let {ib_url, company_id} = this.state.ibConsts;
    const reqData = {
      url: ib_url + '/ib/api/integration/company/' + company_id,
      params: {},
      successCb: response => {
        let integrations = response.map((integration) => {
          return integration.integration;
        })
        this.setState({ integrations, itgFetching: false }, () => {
          this.intialiseSelectedIntegration();
        });

      },
      errorCb: () => {
        // Show the notification for error
        this.setState({itgFetching: false});
        notifUtils.openNotificationWithIcon(errorNotifObj);
      },
    }; 
    this.setState({itgFetching: true}, () => {
      this.props.getRequest({ data: reqData });
    });
  }

  resetAPI = () => {
    this.setState({
      selectedAPI: {},
      customRequestMapping: [],
      customResponseMapping: [],
    })
  }

  intialiseSelectedAPI = () => {
    const { apis, integrationObj, selectedAPI } = this.state;
    let tempAPI = {};
    // const { integrationObj } = this.props;
    // if there is a selected API in the state then reset or if there 
    if (selectedAPI.slug) {
      tempAPI = apis.filter((api) => api.slug === selectedAPI.slug)[0] || {};
    } else {
      if (integrationObj && !!integrationObj.apiSlug) {
        tempAPI = apis.filter((api) => api.slug === integrationObj.apiSlug)[0] || {};
      }
    }
    if (tempAPI.id) {
      this.onAPISelect(tempAPI.id);
    } else {
      this.resetAPI();
    }
  }

  // Change handler for integration
  onIntegrationSelect = (integrationId) => {
    let selectedIntegration = cloneDeep(this.state.integrations.filter((int) => int.id === integrationId)[0] || {});
    // let selectedIntegration = cloneDeep(integration);
    this.setState({ selectedIntegration });
    
    if (integrationId) {
      this.getAPIsForIntegration(integrationId);
    }
  }

  // Get all the available APIs for selected integration
  getAPIsForIntegration = (integrationId) => {
    let {ib_url} = this.state.ibConsts;
    const reqData = {
      url: ib_url + '/ib/api/lendin-api/integration/' + integrationId,
      params: {},
      successCb: response => {
        let apis = response;
        this.setState({ apis, apisFetching: false },  () => {
          this.intialiseSelectedAPI();
        });
      },
      errorCb: () => {
        // Show the notification for error
        notifUtils.openNotificationWithIcon(errorNotifObj);
        this.setState({apisFetching: false});
      },
    }; 
    this.setState({apisFetching: true}, () => {
      this.props.getRequest({ data: reqData });
    });
  }

  // change handler for API selection
  onAPISelect = (apiId) => {
    let selectedAPI = cloneDeep(this.state.apis.filter((api) => api.id === apiId)[0] || {});
    // Set the local selectedAPI object
    this.setState({ selectedAPI });
    // And fetch the API detailed object
    if (apiId) {
      this.getAPIDetails(apiId);
    }
  }

  addCustomMappingObject = (mapType) => { // mapType could be 'request' or 'response'
    let customObj = {key: '', value: ''},
        targetMap = mapType === 'request' ? this.state.customRequestMapping : this.state.customResponseMapping;

    let newMapping;
    if (mapType === 'request') {
      newMapping = cloneDeep([...this.state.customRequestMapping, customObj]);
      this.setState({customRequestMapping: newMapping})
    } else if (mapType === 'response') {
      newMapping = cloneDeep([...this.state.customResponseMapping, customObj]);
      this.setState({customResponseMapping: newMapping})
    }
  }

  formRequestMappingArray = (apiObject) => {
    const { integrationObj, selectedAPI } = this.state;
    let requestKeysArray = apiObject.body || [],
        requestBodyMap = JSON.parse(integrationObj.requestBodyMap),
        responseBodyMap = JSON.parse(integrationObj.responseBodyMap),
        availableRequestBodyKeys = [],
        availableResponseBodyKeys = [];


    let customRequestMapping = [],
        customResponseMapping =[];

    let responseFormat = {};
    if (apiObject.responseBodyFormat) {
      let responseString = apiObject.responseBodyFormat.split("\u21b5").join('');
      responseFormat = commonUtils.IsJsonString(responseString) ? JSON.parse(responseString) : {};
    }

    availableRequestBodyKeys = requestKeysArray.map((keyObj) => keyObj.key);
    availableResponseBodyKeys = Object.keys(responseFormat);

    if (selectedAPI.slug === integrationObj.apiSlug) {
      // Form the requestMap and responseMap
      Object.keys(requestBodyMap).forEach((key) => {
        customRequestMapping.push({key: key, value: requestBodyMap[key] || ''});
      });

      Object.keys(responseBodyMap).forEach((key) => {
        customResponseMapping.push({key: key, value: responseBodyMap[key] || ''});
      });
    }

    this.setState({ 
      availableRequestBodyKeys, 
      availableResponseBodyKeys, 
      customRequestMapping, 
      customResponseMapping
    });

  }

  // Get the selected APIs details to form the request body mapping and response body mapping
  getAPIDetails = (apiId) => {
    let {ib_url} = this.state.ibConsts;
    const reqData = {
      url: ib_url + '/ib/api/lendin-api/' + apiId,
      params: {},
      successCb: response => {
        this.setState({apiDetailsFetching: false}, () => {
          // build the request and response body mapping to be used with Global Variables
          this.formRequestMappingArray(response);
          // this.formResponseMappingArray(response);
        });
      },
      errorCb: () => {
        // Show the notification for error
        notifUtils.openNotificationWithIcon(errorNotifObj);
      },
    }; 
    this.setState({apiDetailsFetching: true}, () => {
      this.props.getRequest({ data: reqData });
    });
  }

  getIBConsts = () => {
    const self = this;
    let url = FLOWABLE.APP_URL.getIBConstsUrl();
    const reqData = {
      url: url,
      params: {},
      successCb: response => {
        let ibConsts = {
          company_slug: response.company_slug, 
          company_id: response.company_id, 
          ib_url: extractIBHost(response.ib_url),
        };
        // set the variables in state
        this.setState({ ibConsts: {...ibConsts} }, () => {
          this.getAvailableIntegrations();
        });
      },
      errorCb: () => {
        // Show the notification for error
        notifUtils.openNotificationWithIcon(errorNotifObj);
      },
    };
    this.props.getRequest({ data: reqData });
  }

  getGlobalVariables = () => {
    const self = this;
    // call REST api to fetch results
    let url = FLOWABLE.APP_URL.getGlobalVariableUrl();
    // Build the request data to get saved variables
    const reqData = {
      url: url,
      params: {},
      successCb: response => {
        if (response && Array.isArray(response)) {
          // set the variables in state
          this.setState({gVariables: cloneDeep(response.map((gv) => gv.processIdentifier))});
          self.props.setGlobalVariables(response);
        }
      },
      errorCb: () => {
        // Show the notification for error
        notifUtils.openNotificationWithIcon(errorNotifObj);
      },
    };
    this.props.getRequest({ data: reqData });
  }

  showDrawer = () => {
    const integrationObj = this.props.getIntegrationObj();
    this.setState({drawerVisible: true, integrationObj}, () => {
      this.intialiseSelectedIntegration();
    });
  }

  onClose = () => {
    this.setState({drawerVisible: false}, () => {
      this.resetDrawer();
    });
  }

  updateCustomRequestMapping = (value, type, index) => {
    const customRequestMapping = cloneDeep(this.state.customRequestMapping);
    customRequestMapping[index][type] = value.replace(/\s/g,'');
    this.setState({ customRequestMapping });
  }

  updateCustomResponseMapping = (value, type, index) => {
    const customResponseMapping = cloneDeep(this.state.customResponseMapping);
    customResponseMapping[index][type] = value.replace(/\s/g,'');
    this.setState({ customResponseMapping });
  }

  removeCustomRequestMapping = (index) => {
    const customRequestMapping = cloneDeep(this.state.customRequestMapping);
    customRequestMapping.splice(index, 1);
    this.setState({ customRequestMapping });
  }

  removeCustomResponseMapping = (index) => {
    const customResponseMapping = cloneDeep(this.state.customResponseMapping);
    customResponseMapping.splice(index, 1);
    this.setState({ customResponseMapping });
  }

  getGVOptions = () => {
    return this.state.gVariables.map((gv) => {
      return (<Option value={gv}>{gv}</Option>);
    });
  }

  selectBodyKey = (value, objType, index, refType) => {
    if (objType === 'request') {
      const customRequestMapping = cloneDeep(this.state.customRequestMapping);
      customRequestMapping[index][refType] = value;
      this.setState({customRequestMapping});
    }
    if (objType === 'response') {
      const customResponseMapping = cloneDeep(this.state.customResponseMapping);
      customResponseMapping[index][refType] = value;
      this.setState({customResponseMapping});
    }
  }

  getMenuOptions = (index, objType, refType) => {
    const refArray = objType === 'request' ? this.state.availableRequestBodyKeys : this.state.availableResponseBodyKeys;

    return (<Menu onClick={(e) => this.selectBodyKey(e.key, objType, index, refType)}>
        {refArray.map((bodyKey) => {
          return (<Menu.Item key={bodyKey}>{bodyKey}</Menu.Item>);
        })}
      </Menu>);
  }

   getIntegrationRequestVarDropdown = (index) => {
    return (
      <Dropdown 
        overlay={this.getMenuOptions(index, 'request', 'key')} 
        style={{display: 'inline-block', marginRight: 6}} 
        trigger={['click']}>
          <Tooltip placement="top" title={'Select from the availble request keys provided by integration'}>
            <Icon type="select" />
          </Tooltip>
      </Dropdown>
    );
  }

  getIntegrationResponseVarDropdown = (index) => {
    return (
      <Dropdown 
        overlay={this.getMenuOptions(index, 'response', 'value')} 
        style={{display: 'inline-block', marginLeft: 6}}
        trigger={['click']}>
          <Tooltip placement="top" title={'Select from the availble response keys provided by integration'}>
            <Icon type="select"  />
          </Tooltip>
      </Dropdown>
    );
  }

  getRequestMapBlock = () => {
    let reqBlock = [];
    let {customRequestMapping} = this.state;

    let customRequestMapBlock = [];
    this.state.customRequestMapping.forEach((customObj, index) => {
      let varDropdown = this.getIntegrationRequestVarDropdown(index);
      customRequestMapBlock.push((
        <div key={'customrequestmapping-' + index} style={wrapRowStyle}>
          <span style={wrapColWithIconStyle}>
            <Input placeholder="key" 
              value={customObj.key} 
              style={{marginLeft: 0}}
              addonBefore={varDropdown}
              onChange={(e) => this.updateCustomRequestMapping(e.target.value, 'key', index)}/>
          </span>

          <span style={{ width: '40%', display: 'inline-block' }}>
            <Select
              showSearch
              style={{ width: '100%' }}
              placeholder="Select a process variable"
              optionFilterProp="children"
              onChange={(val) => this.updateCustomRequestMapping(val, 'value', index)}
              value={customObj.value}>
                {this.getGVOptions()}
            </Select>
          </span>
          
          <Icon type="close-circle" style={{display: 'inline-block', marginLeft: 6}} onClick={(e) => this.removeCustomRequestMapping(index)}/>
          
        </div>
      ));
    })

    return this.state.apiDetailsFetching ?  (<Spin size="small" />) : (
      <div style={{marginBottom: 15}}>
        <h5>
          Set Request mapping:
        </h5> 
        <i style={headingHintText}>Map the request body to Global Variables</i>
        {reqBlock}
        {customRequestMapBlock}
        <div >
          <A onClick={(e) => {this.addCustomMappingObject('request')}}>
              <Icon type="plus-circle" style={iconStyles} />
              <LinkText>Add</LinkText>
          </A>
        </div>
        
      </div>
    );
  }

  getResponseMapBlock = () => {
    let resBlock = [];

    let customResponseMapBlock = [];
    
    this.state.customResponseMapping.forEach((customObj, index) => {
      let varDropdown = this.getIntegrationResponseVarDropdown(index);
      customResponseMapBlock.push((
        <div key={'customresponsemapping-' + index} style={wrapRowStyle}>
          <span style={{ width: '40%', display: 'inline-block', marginRight: 6 }}>
            <Select
              showSearch
              style={{ width: '100%' }}
              placeholder="Select a process variable"
              optionFilterProp="children"
              onChange={(val) => this.updateCustomResponseMapping(val, 'key', index)}
              value={customObj.key}>
                {this.getGVOptions()}
            </Select>
          </span>

          <span style={wrapColWithIconStyle}>
            <Input placeholder="value" 
              value={customObj.value} 
              style={{marginRight: 0}}
              addonAfter={varDropdown}
              onChange={(e) => this.updateCustomResponseMapping(e.target.value, 'value', index)}/>
          </span>
          
          <Icon type="close-circle" style={{display: 'inline-block', marginLeft: 6}} onClick={(e) => this.removeCustomResponseMapping(index)}/>

        </div>
      ));
    })

    return this.state.apiDetailsFetching ? (<Spin size="small" />) : (
      <div style={{marginBottom: 15}}>
        <h5>
          Set Response mapping:
        </h5> 
        <i style={headingHintText}>Map Global Variables to response from integration</i>
        {resBlock}
        {customResponseMapBlock}
        <div >
          <A onClick={(e) => {this.addCustomMappingObject('response')}}>
              <Icon type="plus-circle" style={iconStyles} />
              <LinkText>Add</LinkText>
          </A>
        </div>
        
      </div>
    );
  }

  render() {
    const {integrations, apis, itgFetching, apisFetching} = this.state;

    const itgOtions = integrations.map((integration) => {
              return (<Option value={integration.id}>{integration.name}</Option>)
            });
    const apiOtions = apis.map((api) => {
              return (<Option value={api.id}>{api.name}</Option>)
            });
    const requestMapBlock = this.getRequestMapBlock();
    const responseMapBlock = this.getResponseMapBlock();
    return (
      <div>
        <Button type="primary" onClick={this.showDrawer}>
          <Icon type="plus" /> Configure Integration
        </Button>
        <Drawer
          title="Configure Integration"
          width={520}
          onClose={this.onClose}
          visible={this.state.drawerVisible}
        >
          <div style={{marginBottom: 15}}>
            <h5>Select Integration: </h5>
            <Select
              showSearch
              style={{ width: 200 }}
              placeholder="Select an Integration"
              optionFilterProp="children"
              onChange={(val) => this.onIntegrationSelect(val)}
              notFoundContent={itgFetching ? <Spin size="small" /> : null}
              
              value={this.state.selectedIntegration.id}>
              {itgOtions}
            </Select>
            {itgFetching ? <Spin size="small" /> : null}
          </div>

          <div style={{marginBottom: 15}}>
            <h5>Select API: </h5>
            <Select
              showSearch
              style={{ width: 200 }}
              placeholder="Select an API"
              optionFilterProp="children"
              onChange={(val) => this.onAPISelect(val)}
              notFoundContent={apisFetching ? <Spin size="small" /> : null}
              
              value={this.state.selectedAPI.id}>
              {apiOtions}
            </Select>
            {apisFetching ? <Spin size="small" /> : null}
          </div>

          <div style={{marginBottom: 70}}>

            {requestMapBlock}
            {responseMapBlock}

          </div>

          <div
            style={{
              position: 'absolute',
              right: 0,
              bottom: 0,
              width: '100%',
              borderTop: '1px solid #e9e9e9',
              padding: '10px 16px',
              background: '#fff',
              textAlign: 'right',
            }}
          >
            <Button onClick={this.onClose} style={{ marginRight: 8 }}>
              Cancel
            </Button>
            <Button onClick={this.saveIntegrationObject} type="primary">
              Set
            </Button>
          </div>



          
        </Drawer>
      </div>
      );
  }
}

ConfigureHttpTask.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  configureHttpTask: makeSelectConfigureHttpTask(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: reqObject => dispatch(appActions.getRequest(reqObject)),
    postRequest: reqObject => dispatch(appActions.postRequest(reqObject)),
    putRequest: reqObject => dispatch(appActions.putRequest(reqObject)),
    deleteRequest: reqObject => dispatch(appActions.deleteRequest(reqObject)),
    setGlobalVariables: data => dispatch(appActions.setGlobalVariables(data)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'configureHttpTask', reducer });
const withSaga = injectSaga({ key: 'configureHttpTask', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ConfigureHttpTask);
