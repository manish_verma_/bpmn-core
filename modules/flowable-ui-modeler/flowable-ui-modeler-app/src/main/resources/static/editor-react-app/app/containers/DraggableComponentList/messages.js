/*
 * DraggableComponentList Messages
 *
 * This contains all the text for the DraggableComponentList container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.DraggableComponentList';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the DraggableComponentList container!',
  },
});
