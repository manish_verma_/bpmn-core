import { fromJS } from 'immutable';
import configureHttpTaskReducer from '../reducer';

describe('configureHttpTaskReducer', () => {
  it('returns the initial state', () => {
    expect(configureHttpTaskReducer(undefined, {})).toEqual(fromJS({}));
  });
});
