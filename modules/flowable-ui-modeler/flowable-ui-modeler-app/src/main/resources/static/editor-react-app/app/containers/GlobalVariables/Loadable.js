/**
 *
 * Asynchronously loads the component for GlobalVariables
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
