import { fromJS } from 'immutable';
import globalVariablesReducer from '../reducer';

describe('globalVariablesReducer', () => {
  it('returns the initial state', () => {
    expect(globalVariablesReducer(undefined, {})).toEqual(fromJS({}));
  });
});
