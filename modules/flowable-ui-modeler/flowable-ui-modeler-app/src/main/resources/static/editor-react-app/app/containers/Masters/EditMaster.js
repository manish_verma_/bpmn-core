import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, message, Radio, Divider, Input, Checkbox } from 'antd';

import FileUpload from './FileUpload';
import CodeEditor from '../../components/CodeEditor';
import { UpdateButton, P } from './styles';

const RadioGroup = Radio.Group;

export class EditMaster extends Component {
  static propTypes = {
    record: PropTypes.object,
    getRequest: PropTypes.func,
    putRequest: PropTypes.func,
    form: PropTypes.object,
    isNewMaster: PropTypes.bool,
    fetchMastersData: PropTypes.func,
    toggleEditModal: PropTypes.func,
  };

  state = {
    currentSelected: 0,
    isNewVersion: false,
  };

  componentDidMount() {
    const { isNewMaster } = this.props;
    if (!isNewMaster) this.fetchMasterDetails();
  }

  fetchMasterDetails = () => {
    const { record } = this.props;
    const query = `slug=${record.masterSlug}&isHistory=true`;
    const url = MASTER.APP_URL.getMasterDetails(query);
    const reqData = {
      url,
      params: {},
      successCb: response => {
        if (response.data) {
          this.setState(() => ({
            masterData: response.data,
          }));
        }
      },
      errorCb: () => {
        message.error('error occur in fetching master Details');
      },
    };
    this.props.getRequest({ data: reqData });
  };

  updateDataDefinition = ({ saveToDB }) => {
    const { record, isNewMaster, companyName } = this.props;
    this.props.form.validateFields(
      ['name', 'dataDefinition'],
      (err, values) => {
        if (!err) {
          const code = values.dataDefinition || null;
          if (isNewMaster) {
            let { name } = values;
            name = name.trim();
            const query = `saveToDatabase=${saveToDB}`;
            const url = MASTER.APP_URL.uploadDataDefinition(query);
            let parseCode;
            let errorFlag;
            try {
              parseCode = JSON.parse(code);
            } catch (e) {
              message.error('Input valid JSON');
            }
            if (!errorFlag) {
              const dataDefinition = {
                name,
                companyName,
                data: parseCode,
              };
              // Build the request data to upload dataDefinition
              const reqData = {
                url,
                params: { ...dataDefinition },
                successCb: response => {
                  if (response && saveToDB) {
                    message.success('DataDefinition Upload');
                    this.props.toggleEditModal();
                    this.props.fetchMastersData();
                  } else {
                    message.success(response.message);
                  }
                },
                errorCb: error => {
                  message.error(error.errorMessage);
                },
              };
              this.props.postRequest({ data: reqData });
            }
          } else {
            const { masterData } = this.state;
            const { dataDefinition } = masterData;
            const query = `${record.masterSlug}?saveToDatabase=${saveToDB}`;
            const url = MASTER.APP_URL.updateDataDefinition(query);
            let parseCode;
            let errorFlag;
            try {
              parseCode = JSON.parse(code);
            } catch (e) {
              message.error('Input valid JSON');
              errorFlag = true;
            }
            if (!errorFlag) {
              dataDefinition.data = parseCode;
              // Build the request data to update dataDefinition
              const reqData = {
                url,
                params: { ...dataDefinition },
                successCb: response => {
                  if (response && saveToDB) {
                    message.success('DataDefinition Updated');
                    this.props.toggleEditModal();
                  } else {
                    message.success(response.message);
                  }
                },
                errorCb: error => {
                  message.error(error.errorMessage);
                },
              };
              this.props.putRequest({ data: reqData });
            }
          }
        }
      },
    );
  };

  updateDataStorage = ({ saveToDB }) => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const dataStorage = values.dataStorage || null;
        const { record } = this.props;
        const { masterData, isNewVersion } = this.state;
        let query;
        if (isNewVersion) {
          query = `${
            record.masterSlug
          }?saveToDatabase=${saveToDB}&createNewVersion=true`;
        } else {
          query = `${record.masterSlug}?saveToDatabase=${saveToDB}`;
        }
        const url = MASTER.APP_URL.updateDataStorage(query);
        let parseCode;
        let requestType = 'put';
        let masterDataObj = {};
        let errorFlag;
        try {
          parseCode = JSON.parse(dataStorage);
        } catch (e) {
          message.error('Input valid JSON');
          errorFlag = true;
        }
        if (!errorFlag) {
          parseCode = parseCode || [];
          if (!masterData.dataStorage) {
            requestType = 'post';
            masterDataObj.dataStorage = {
              data: { dataList: parseCode },
            };
          } else {
            masterDataObj = masterData;
            masterDataObj.dataStorage.data.dataList = parseCode;
          }
          const body = masterDataObj.dataStorage.data;
          // Build the request data to update dataDefinition
          const reqData = {
            url,
            params: { data: body },
            successCb: response => {
              if (response && saveToDB) {
                const SuccessMessage = isNewVersion
                  ? 'New Version Created'
                  : 'Data Storage Updated';
                message.success(SuccessMessage);
                this.props.toggleEditModal();
              } else {
                message.success('Data Storage Validation SUCCESS');
              }
            },
            errorCb: () => {
              message.error('Please enter the valid data values');
            },
          };
          if (requestType === 'put') {
            this.props.putRequest({ data: reqData });
          } else {
            this.props.postRequest({ data: reqData });
          }
        }
      }
    });
  };

  onChangeRadioButton = event =>
    this.setState({ currentSelected: event.target.value });

  RenderDataStorage = props => {
    const { dataStorage } = props;
    const { form } = this.props;
    return (
      <div>
        {form.getFieldDecorator('dataStorage', {
          initialValue: dataStorage || '',
          valuePropName: 'code',
        })(<CodeEditor mode="json" onChange={this.onChangeDataStorage} />)}
        <UpdateButton
          type="primary"
          style={{ marginLeft: 10, marginRight: 5 }}
          onClick={() => this.updateDataStorage({ saveToDB: true })}
        >
          Update
        </UpdateButton>
        <UpdateButton
          type="primary"
          onClick={() => this.updateDataStorage({ saveToDB: false })}
        >
          Validation
        </UpdateButton>
      </div>
    );
  };

  handleCheck = () => {
    this.setState(prevState => ({
      isNewVersion: !prevState.isNewVersion,
    }));
  };

  formItemLayout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 10 },
  };

  render() {
    const { form, isNewMaster } = this.props;
    const { masterData } = this.state;
    const name =
      (masterData &&
        masterData.dataDefinition &&
        masterData.dataDefinition.name) ||
      '';
    let dataDefinition = (masterData && masterData.dataDefinition.data) || '';
    let dataStorage =
      (masterData &&
        masterData.dataStorage &&
        masterData.dataStorage.data &&
        masterData.dataStorage.data.dataList) ||
      [];
    dataDefinition =
      typeof dataDefinition !== 'string' &&
      JSON.stringify(dataDefinition, null, 2);
    dataStorage =
      typeof dataStorage !== 'string' && JSON.stringify(dataStorage, null, 2);
    return (
      <div style={{ overflow: 'auto' }}>
        <Form.Item
          label="Master Name"
          {...this.formItemLayout}
          style={{ marginBottom: '-24px' }}
        >
          {form.getFieldDecorator('name', {
            initialValue: name || '',
            validateTrigger: ['onBlur', 'onChange'],
            rules: [
              {
                required: true,
                message: 'Please input master name',
              },
            ],
          })(<Input disabled={!isNewMaster} />)}
        </Form.Item>
        <Divider orientation="left" style={{ marginBottom: '-20px' }}>
          <P>Data Definition</P>
        </Divider>
        {form.getFieldDecorator('dataDefinition', {
          initialValue: dataDefinition || '',
          valuePropName: 'code',
        })(<CodeEditor mode="json" />)}
        <UpdateButton
          type="primary"
          onClick={() => this.updateDataDefinition({ saveToDB: true })}
          style={{ marginLeft: 10, marginRight: 5 }}
        >
          {isNewMaster ? 'Add' : 'Update'}
        </UpdateButton>
        <UpdateButton
          type="primary"
          onClick={() => this.updateDataDefinition({ saveToDB: false })}
        >
          Validation
        </UpdateButton>
        {!isNewMaster && (
          <div>
            <Divider orientation="left">
              <P>Data Storage</P>
            </Divider>
            <div style={{ display: 'flex' }}>
              <RadioGroup
                value={this.state.currentSelected}
                onChange={this.onChangeRadioButton}
                style={{ overflow: 'hidden' }}
              >
                <Radio value={0}>Upload</Radio>
                <Radio value={1}>JSON</Radio>
              </RadioGroup>
              {!isNewMaster && (
                <Checkbox
                  checked={this.state.isNewVersion}
                  onChange={this.handleCheck}
                >
                  Create New Version
                </Checkbox>
              )}
            </div>
            {this.state.currentSelected === 0 ? (
              <FileUpload
                {...this.props}
                masterData={this.state.masterData}
                isNewVersion={this.state.isNewVersion}
              />
            ) : (
              <this.RenderDataStorage dataStorage={dataStorage} />
            )}
          </div>
        )}
      </div>
    );
  }
}

export default Form.create({ name: 'EditMaster' })(EditMaster);
