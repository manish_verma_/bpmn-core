/**
 *
 * AppWrapper
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectAppWrapper from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import * as appActions from './actions';

/* eslint-disable react/prefer-stateless-function */
export class AppWrapper extends React.PureComponent {

  componentWillMount () {
    let currentForm = this.props.currentForm;
    this.props.setCurrentForm({...currentForm});
  }

  render() {
    return (
      <div style={{height: '100%'}} className="" style={{paddingTop: 0, height: '100%'}}>
        {this.props.children}
      </div>
    );
  }
}

AppWrapper.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  appWrapper: makeSelectAppWrapper(),
});

function mapDispatchToProps(dispatch) {
  return {
    setCurrentForm: (data) => dispatch(appActions.setCurrentForm(data)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'appWrapper', reducer });
const withSaga = injectSaga({ key: 'appWrapper', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AppWrapper);
