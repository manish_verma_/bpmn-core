/*
 * EditorFormSave Messages
 *
 * This contains all the text for the EditorFormSave container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.EditorFormSave';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the EditorFormSave container!',
  },
});
