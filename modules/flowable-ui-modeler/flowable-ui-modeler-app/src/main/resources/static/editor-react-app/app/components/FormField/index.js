import React from 'react';
import RenderSelectField from './SelectField';
import RenderTextField from './TextField';
import RenderCheckboxField from './CheckboxField';
import RenderHeadingField from './HeadingField';
import RenderSliderField from './SliderField';
import RenderAccordionField from './AccordionField';
import RenderRadioAccordionField from './RadioAccordionField';
import RenderTableField from './TableField';

export const SelectField = props => <RenderSelectField {...props} />;
export const TextField = props => <RenderTextField {...props} />;
export const CheckboxField = props => <RenderCheckboxField {...props} />;
export const HeadingField = props => <RenderHeadingField {...props} />;
export const SliderField = props => <RenderSliderField {...props} />;
export const AccordionField = props => <RenderAccordionField {...props} />;
export const RadioAccordionField = props => (
  <RenderRadioAccordionField {...props} />
);
export const TableField = props => <RenderTableField {...props} />;

const FieldMap = {
  SelectField,
  TextField,
  CheckboxField,
  HeadingField,
  SliderField,
  AccordionField,
  RadioAccordionField,
  TableField,
};

export default FieldMap;
