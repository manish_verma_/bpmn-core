import { inputComponentStyle } from 'configs/commonStyles';
import styled from 'styled-components';
export const inputStyles = {
  ...inputComponentStyle,
};

export const HeadingWrapper = styled.h5`
  font-size: 14px;
  padding: 10px 0px;
  margin-bottom: 0px;
`;

export const DataWrapper = styled.div`
  background-color: #f9f9f9;
  padding: ${props => (props.level === 0 ? '10px 13px' : '0px')};
`;

export const HeadingText = styled.span`
  display: inline-block;
  margin-right: 5px;
`;

export const iconStyles = {
  verticalAlign: 'middle',
  marginRight: '6px',
};

export const wrapRowStyle ={
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'left',
  marginBottom: '4px',
}

export const wrapColWithIconStyle ={
  width: '40%',
  display: 'flex',
  marginRight: '6px',
  alignItems: 'center',
  justifyContent: 'center',
}

export const headingHintText ={
  fontSize: '10px',
  lineHeight: '2px',
  verticalAlign: 'top',
}

export const LinkText = styled.span`
  vertical-align: middle;
  display: inline-block;
  margin-left: 5px;
`;
