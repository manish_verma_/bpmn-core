/*
 * DraggableComponent Messages
 *
 * This contains all the text for the DraggableComponent component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.DraggableComponent';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the DraggableComponent component!',
  },
});
