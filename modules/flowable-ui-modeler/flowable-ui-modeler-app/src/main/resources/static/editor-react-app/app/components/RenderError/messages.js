/*
 * RenderError Messages
 *
 * This contains all the text for the RenderError component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderError.header',
    defaultMessage: 'This is the RenderError component !',
  },
});
