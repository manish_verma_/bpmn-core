import styled from 'styled-components';
import { Icon } from 'antd';

export const Wrapper = styled.div`
  padding: 10px 0px;
  border-bottom: 1px solid rgba(255,255,255, 0.05);
  cursor: pointer;
  transition: 0.2s all linear;
  cursor: move;
  &:hover {
    background-color: #a8bac1;
    color: #1f3245;
  }
`;

export const Title = styled.span`
  font-size: 14px;
  text-transform: capitalize;
  padding-left: 10px;
`;

// export const ComponentIcon = styled.Icon`
//   margin-top: -2px;
//   vertical-align: middle;
//   margin-left: 9px;
//   font-size: 6px !important;
// `;
