export const componentList = [
{
	id: 'text',
	label: 'Input text',
	metaKeys: [
	{
		id: 'required',
		label: 'Required',
		type: 'bool',
		default: true,
	},
	{
		id: 'order',
		label: 'Order',
		type: 'number',
		default: 0,
	}]
},
{
	id: 'number',
	label: 'Input Number',
	metaKeys: [
	{
		id: 'required',
		label: 'Required',
		type: 'bool',
		default: true
	},
	{
		id: 'order',
		label: 'Order',
		type: 'number',
		default: 0,
	}]
},
{
	id: 'checkbox',
	label: 'Checkbox',
	metaKeys: [
	{
		id: 'required',
		label: 'Required',
		type: 'bool',
		default: true
	},
	{
		id: 'disabled',
		label: 'Disabled',
		type: 'bool',
		default: false
	},
	{
		id: 'order',
		label: 'Order',
		type: 'number',
		default: 0,
	}]
}]