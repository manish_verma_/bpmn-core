/* eslint-disable react/no-multi-comp */
/**
 *
 * GlobalVariables
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import {
  Table,
  Input,
  InputNumber,
  Button,
  Icon,
  Popconfirm,
  Form,
  Checkbox,
  Select,
} from 'antd';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectGlobalVariables from './selectors';
import reducer from './reducer';
import saga from './saga';

import * as commonUtils from '../../utilities/commonUtils';
import * as notifUtils from '../../utilities/notificationUtils';
import * as appActions from '../AppWrapper/actions';

import AddGlobalVariableForm from './AddGlobalVariables';
import { setGlobalVariablesList } from '../../utilities/constants';

import { AddVariableWrapper } from './styles';
import './styles.css';

const { Option } = Select;
const FormItem = Form.Item;
const EditableContext = React.createContext();

const uniqueIdentifier = 'uuid';

const successNotifObj = {
  type: 'success',
  messages: {
    title: 'Success',
    description: 'Your request has been saved successfully.',
  },
};

const errorNotifObj = {
  type: 'error',
  messages: {
    title: 'Error!',
    description:
      'There is some error in completing the request. Please check the data before submitting',
  },
};

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends React.Component {

  state = {
    checked: false
  };

  componentDidMount() {
    if (this.props.record) {
      this.setState(() => ({checked : this.props.record.isCreditVariable}));
    }
  }

  getInput = form => {
    if (this.props.inputType === 'number') {
      return <InputNumber />;
    } 
    if (this.props.inputType === 'checkbox') {
      return (
        <Checkbox
          checked={this.state.checked}
          onChange={event => this.setState(() => ({ checked: event.target.checked }))}
        />
      );
    }
    if (this.props.inputType === 'dropdown') {
      return (
        <Select style={{ width: 180 }} size="small">
          <Option value="STRING">string</Option>
          <Option value="BOOLEAN">boolean</Option>
          <Option value="DATE">date</Option>
          <Option value="DOUBLE">double</Option>
          <Option value="INTEGER">integer</Option>
          <Option value="LONG">long</Option>
          <Option value="ENCRYPTED">encrypted</Option>
        </Select>
      );
    }
    return <Input ref={node => (this.inputCell = node)} size="small" />;
  };

  triggerEdit = record => {
    if (!this.props.editing) {
      this.props.edit(record);
      setTimeout(() => this.inputCell.select());
    }
  };

  render() {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      required,
      checkDuplicatPIDs,
      edit,
      ...restProps
    } = this.props;

    const self = this;
    const rules = [
      {
        required: required,
        message: `Please Input ${title}!`,
      },
    ];
    if (dataIndex === 'processIdentifier') {
      rules.push({
        message: 'Duplicate Process Identifier found',
        validator: (rule, value, cb) =>
          checkDuplicatPIDs(rule, value, cb, record),
      },
      {
        message: 'Only alphanumeric and underscrore(_) characters are allowed',
        validator: (rule, value, cb) => {
          let regExp = new RegExp(/^[a-zA-Z0-9_]+$/);
          if (!regExp.test(value)) {
            cb(new Error('Only alphanumeric and underscrore(_) characters are allowed'));
          }
          // if (value.indexOf(' ') >= 0 || value.indexOf('-') >= 0) {
          //   cb(new Error('No space or \'-\' is allowed'));
          // }
          cb();
        }
      });
    }
    return (
      <EditableContext.Consumer>
        {form => {
          const { getFieldDecorator } = form;
          return (
            <td
              {...restProps}
              onClick={e => {
                this.props.record ? this.triggerEdit(record) : null;
              }}
              className={editing ? 'gv-cell editing-cell' : 'gv-cell'}
              style={{ cursor: 'pointer' }}
            >
              {editing ? (
                <FormItem style={{ margin: 0 }}>
                  {getFieldDecorator(dataIndex, {
                    rules: rules,
                    initialValue: record[dataIndex],
                  })(this.getInput(form))}
                </FormItem>
              ) : (
                restProps.children
              )}
            </td>
          );
        }}
      </EditableContext.Consumer>
    );
  }
}

/* eslint-disable react/prefer-stateless-function */
export class GlobalVariables extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      tableData: [],
      searchText: '',
      editingKey: '',
    };
  }

  componentDidMount() {
    // get the current available list of variables
    this.getVariables();
  }

  // get current list of all variables
  getVariables = () => {
    const self = this;
    // call REST api to fetch results
    let url = FLOWABLE.APP_URL.getGlobalVariableUrl();
    // Build the request data to get saved variables
    const reqData = {
      url: url,
      params: {},
      successCb: response => {
        if (response && Array.isArray(response)) {
          const data = response.map((item, index) => {
            return {
              ...item,
              uuid: commonUtils.generateUUID(),
              isEditing: false,
            };
          });
          // set the table data
          this.setTableData([...data]);
          self.props.setGlobalVariables(response);
        }
      },
      errorCb: () => {
        // Show the notification for error
        notifUtils.openNotificationWithIcon(errorNotifObj);
      },
    };
    this.props.getRequest({ data: reqData });
  };

  // attach all the property for filtering perspective
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  edit = record => {
    if (record.isEditing) {
      return;
    }
    let newData = [...this.state.tableData];
    const item = { ...record, isEditing: true };
    const index = newData.findIndex(item => record.uuid === item.uuid);
    newData.splice(index, 1, {
      ...item,
    });
    this.setTableData(newData);
  };

  cancel = record => {
    let newData = [...this.state.tableData];
    const item = { ...record, isEditing: false };
    const index = newData.findIndex(item => record.uuid === item.uuid);
    if (record.id) {
      newData.splice(index, 1, {
        ...item,
      });
    } else {
      newData.splice(index, 1);
    }
    this.setTableData(newData);
  };

  updateField = (field, newField) => {
    const self = this;
    let url = FLOWABLE.APP_URL.getGlobalVariableIdUrl(field.id);
    if (newField) {
      url = FLOWABLE.APP_URL.getGlobalVariableUrl();
    }
    const { uuid, isEditing } = field;
    let { ...objToSave } = field;
    objToSave = objToSave.isCreditVariable === true ? { ...objToSave, isCreditVariable: 1 } : {...objToSave, isCreditVariable: 0 };

    objToSave.processIdentifier = objToSave.processIdentifier.trim();
    // Build the request data to PUT this variable
    const reqData = {
      url: url,
      params: { ...objToSave },
      successCb: response => {
        if (response) {
          const newData = [...self.state.tableData];
          const index = newData.findIndex(item => field.uuid === item.uuid);
          if (index > -1) {
            const item = newData[index];
            newData.splice(index, 1, {
              ...item,
              ...response,
              isEditing: false,
            });
            self.setTableData(newData);
          } else {
            console.debug('not able to find the respective data item');
          }
        }
      },
      errorCb: error => {
        // Show the notification for error
        notifUtils.openNotificationWithIcon(errorNotifObj);
      },
    };

    newField
      ? this.props.postRequest({ data: reqData })
      : this.props.putRequest({ data: reqData });
  };

  // Save the individual item in backend
  saveField = (form, record) => {
    form.validateFields((error, row) => {
      if (error) {
        return;
      }
      const newData = [...this.state.tableData];
      const objToSave = { ...record, ...row };
      if (record.id) {
        this.updateField(objToSave);
      } else {
        // this a new field;
        this.updateField(objToSave, true);
      }
    });
  };

  // DELETE a variable
  deleteField = field => {
    const self = this;
    let url = FLOWABLE.APP_URL.getGlobalVariableIdUrl(field.id);

    const { uuid, isEditing, ...objToSave } = field;
    // Build the request data to PUT this variable
    const reqData = {
      url: url,
      params: { ...objToSave },
      successCb: response => {
        if (response) {
          const newData = [...self.state.tableData];
          const index = newData.findIndex(item => field.uuid === item.uuid);
          if (index > -1) {
            newData.splice(index, 1);
            self.setTableData(newData);
          } else {
            console.debug('not able to find the respective data item');
          }
        }
      },
      errorCb: error => {
        // Show the notification for error
        notifUtils.openNotificationWithIcon(errorNotifObj);
      },
    };

    this.props.deleteRequest({ data: reqData });
  };

  // define the rendering JSON for keeping all fields editable in the table
  columns = [
    {
      title: 'Process Identifier',
      dataIndex: 'processIdentifier',
      key: 'processIdentifier',
      editable: true,
      required: true,
      ...this.getColumnSearchProps('processIdentifier'),
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      editable: true,
      required: true,
      width: "15%",
      ...this.getColumnSearchProps('name'),
    },
    {
      title: 'DataType',
      dataIndex: 'dataType',
      key: 'dataType',
      editable: true,
      required: true,
      width: "15%",
    },
    {
      title: 'IsCreditVariable',
      dataIndex: 'isCreditVariable',
      key: 'isCreditVariable',
      editable: true,
      width: "15%",
      render: (text, record) => {
        return (
          <Checkbox checked={text} />
        );
      }
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
      editable: true,
      ...this.getColumnSearchProps('description'),
    },
    ,
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      render: (text, record) => {
        const { editingKey } = this.state;
        const editable = record.isEditing;
        return (
          <div>
            {editable ? (
              <span>
                <EditableContext.Consumer>
                  {form => (
                    <a
                      href="javascript:;"
                      onClick={() => this.saveField(form, record)}
                      style={{ marginRight: 8 }}
                    >
                      Save
                    </a>
                  )}
                </EditableContext.Consumer>
                <a href="javascript:;" onClick={() => this.cancel(record)}>
                  Cancel
                </a>
              </span>
            ) : (
              <div>
                <a
                  style={{ marginRight: 10 }}
                  disabled={record.isEditing}
                  onClick={() => this.edit(record)}
                >
                  Edit
                </a>
                <Popconfirm
                  title="Sure to delete this variable?"
                  onConfirm={() => this.deleteField(record)}
                >
                  <a>Delete</a>
                </Popconfirm>
              </div>
            )}
          </div>
        );
      },
    },
  ];

  // set table data for rendering
  setTableData = data => {
    this.setState({ tableData: data });
    setGlobalVariablesList(data);
  };

  // Add new globale variable
  handleAdd = () => {
    const { tableData } = this.state;
    const newData = {
      processIdentifier: '',
      name: '',
      description: '',
      dataType: '',
      isCreditVariable: '',
      category: 'FORMDATA',
      stepIndex: 1,
      uuid: commonUtils.generateUUID(),
      isEditing: true,
    };
    this.setState({
      tableData: [...tableData, newData],
    });
  };

  checkDuplicatPIDs = (rule, value, callback, record) => {
    const samePIDObjects = this.state.tableData.filter(obj => {
      return (
        obj.processIdentifier.toLowerCase() === value.toLowerCase() &&
        record.uuid !== obj.uuid
      );
    });
    if (samePIDObjects.length !== 0) {
      callback(new Error('Duplicate Process Identifier found'));
    }
    callback();
  };

  // Below method is for the <AddGlobalVariableForm> component
  // Once the new variables are added they are added to the core table data of this page
  updateParentData = newAddedVariables => {
    let tableData = [...this.state.tableData];

    // Push all the newly added Variables into the tableData
    Array.prototype.push.apply(tableData, [...newAddedVariables]);
    this.setTableData(tableData);
  };

  render() {
    const tableData = this.state.tableData.map(data => {
      if (!data.dataType) {
        return {
          ...data,
          dataType: "STRING"
        };
      } else {
        return data;
      }
    });
    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    const columns = this.columns.map(col => {
      let inputType;
      if (col.key == "dataType") {
        inputType = "dropdown";
      } else if (col.key == "isCreditVariable") {
        inputType = "checkbox";
      } else {
        inputType = "text";
      }
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          editing: record.isEditing,
          record,
          inputType,
          dataIndex: col.dataIndex,
          title: col.title,
          required: col.required,
          checkDuplicatPIDs: this.checkDuplicatPIDs,
          edit: this.edit,
        }),
      };
    });

    return (
      <div className="">
        <div style={{ position: 'relative' }}>
          <AddVariableWrapper>
            <AddGlobalVariableForm
              postRequest={this.props.postRequest}
              updateParentData={this.updateParentData}
              currentGlobalVariables={[...this.state.tableData]}
              getVariables={this.getVariables}
              isImpExpVisible
            />
          </AddVariableWrapper>
          <EditableContext.Provider value={this.props.form}>
            <Table
              bordered
              components={components}
              dataSource={tableData}
              columns={columns}
              size="small"
              rowKey={record => record.uuid}
              rowClassName="global-variable-table-row"
              pagination={{
                position: 'both',
                pageSize: 20,
                showSizeChanger: true,
                showQuickJumper: true,
                pageSizeOptions: ['10', '25', '50', '100'],
              }}
            />
          </EditableContext.Provider>
        </div>
      </div>
    );
  }
}

GlobalVariables.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  globalVariables: makeSelectGlobalVariables(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: reqObject => dispatch(appActions.getRequest(reqObject)),
    postRequest: reqObject => dispatch(appActions.postRequest(reqObject)),
    putRequest: reqObject => dispatch(appActions.putRequest(reqObject)),
    deleteRequest: reqObject => dispatch(appActions.deleteRequest(reqObject)),
    setGlobalVariables: data => dispatch(appActions.setGlobalVariables(data)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'globalVariables', reducer });
const withSaga = injectSaga({ key: 'globalVariables', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(GlobalVariables);
