import React from 'react';
import PropTypes from 'prop-types';
import { Collapse, Radio } from 'antd';

const { Panel } = Collapse;

const customPanelStyle = {
  background: 'transparent',
  borderRadius: 0,
  marginBottom: 24,
  overflow: 'hidden',
};

const wrapperStyle = {
  background: 'transparent',
};

const RadioAccordionField = props => (
  <Collapse bordered={false} style={wrapperStyle}>
    <Panel header={<Radio>{props.label}</Radio>} style={customPanelStyle} />
  </Collapse>
);

RadioAccordionField.propTypes = {
  label: PropTypes.string,
};

export default RadioAccordionField;
