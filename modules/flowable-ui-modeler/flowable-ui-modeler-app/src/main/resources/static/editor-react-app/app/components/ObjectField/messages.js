/*
 * ObjectField Messages
 *
 * This contains all the text for the ObjectField component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.ObjectField.header',
    defaultMessage: 'This is the ObjectField component !',
  },
});
