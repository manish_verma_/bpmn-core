/*
This is the validation library
  Contains the conditions against a unique key for a validation condition
*/
import validationConstants from './validationConstants';
import { definedValueCheck } from '../commonUtils';

const validationResultObject = (resultBool, errorMessage) => ({
  isValid: resultBool,
  errorMessage: !resultBool ? errorMessage : '',
});

// A required validation check
// Use it wherever required condition is supposed to be met
const requiredValidation = value => {
  const error = validationConstants.errorMessages.isRequired;
  return validationResultObject(
    definedValueCheck(value) && value !== '',
    error,
  );
};

export default {
  isRequired: requiredValidation,
};
