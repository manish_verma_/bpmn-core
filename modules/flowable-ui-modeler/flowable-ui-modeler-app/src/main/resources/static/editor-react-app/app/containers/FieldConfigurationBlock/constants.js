/*
 *
 * FieldConfigurationBlock constants
 *
 */

export const DEFAULT_ACTION = 'app/FieldConfigurationBlock/DEFAULT_ACTION';
export const VALIDATE_AND_UPDATE_FIELD =
  'app/FieldConfigurationBlock/VALIDATE_AND_UPDATE_FIELD';
export const VALIDATE_ALL_FIELDS =
  'app/FieldConfigurationBlock/VALIDATE_ALL_FIELDS';
export const UPDATE_META = 'app/FieldConfigurationBlock/UPDATE_META';
export const UPDATE_FIELD_META = 'app/FieldConfigurationBlock/UPDATE_FIELD_META';
export const UPDATE_FIELD = 'app/FieldConfigurationBlock/UPDATE_FIELD';
export const UPDATE_SELECTED_INDEX =
  'app/FieldConfigurationBlock/UPDATE_SELECTED_INDEX';
  
export const REMOVE_SELECTED_INDEX =
  'app/FieldConfigurationBlock/REMOVE_SELECTED_INDEX';
export const SET_SELECTED_INDEX =
  'app/FieldConfigurationBlock/SET_SELECTED_INDEX';
export const SET_RENDER_DATA = 'app/FieldConfigurationBlock/SET_RENDER_DATA';
export const SET_META_VALID = 'app/FieldConfigurationBlock/SET_META_VALID';
export const TOGGLE_META_ERROR = 'app/FieldConfigurationBlock/TOGGLE_META_ERROR';
export const SET_FORM_META = 'app/FieldConfigurationBlock/SET_FORM_META';
export const UPDATE_RENDER_DATA =
  'app/FieldConfigurationBlock/UPDATE_RENDER_DATA';

// TODO: dummy data - remove after integrations
export const DUMMY_FORM_META = [
  {
    fieldType: 'FormField',
    id: '',
    name: 'first field',
    type: 'multi-line-text',
    value: null,
    required: false,
    readOnly: ['hello'],
    overrideId: false,
    placeholder: null,
    params: {
      meta: {
        type: 'string',
        form_type: 'elasticAutocomplete',
        default: '',
        order: 11,
        required: true,
        disabled: false,
        placeholder: false,
        api: {
          url: 'http://129.213.24.224:8001/api/masters/search-master/',
          method: 'POST',
          data: {
            index: 'company_master_updated',
            COM_NAME: 'companyName',
          },
          'prefix-id': ['POST'],
          dependentFields: {
            companyCategory: 2,
            companyAddress: 9,
            companyType: 6,
          },
          'options-mapping': {
            id: 'id',
            'name-index': 5,
          },
          'depends-on': '[]',
        },
        validator: '[]',
        allowKeyInput: true,
        lang: {
          'en-US': 'Your company',
          'vi-VN': 'Công ty hiện tại',
        },
      },
    },
    layout: null,
  },
  {
    fieldType: 'FormField',
    id: '',
    name: 'first field',
    type: 'multi-line-text',
    value: null,
    required: false,
    readOnly: ['hello'],
    overrideId: false,
    placeholder: null,
    params: {
      meta: {
        type: 'string',
        form_type: 'rest-dropdown',
        default: '',
        order: 11,
        required: true,
        disabled: false,
        placeholder: false,
        api: {
          GET: 'http://129.213.24.224:8001/api/masters/search-master/',
          POST: 'POST',
          // data: {
          //   index: 'company_master_updated',
          //   COM_NAME: 'companyName',
          // },
          // 'prefix-id': ['companyType'],
          // dependentFields: {
          //   companyCategory: 2,
          //   companyAddress: 9,
          //   companyType: 6,
          // },
          // 'options-mapping': {
          //   id: 'id',
          //   'name-index': 5,
          // },
          // 'depends-on': '[]',
        },
        validator: '[]',
        allowKeyInput: true,
        lang: {
          'en-US': 'Your company',
          'vi-VN': 'Công ty hiện tại',
        },
      },
    },
    layout: null,
  },
  {
    fieldType: 'OptionFormField',
    id: 'radioelement',
    name: 'radioElement',
    type: 'radio-buttons',
    value: 'male',
    required: false,
    readOnly: false,
    overrideId: true,
    placeholder: null,
    params: {
      meta: {
        type: 'string',
        form_type: 'radio',
        default: '',
        order: 6,
        required: true,
        disabled: false,
        placeholder: false,
        api: '',
        validator: '[]',
      },
    },
    layout: null,
    optionType: null,
    hasEmptyValue: null,
    options: [
      {
        id: 'male',
        name: 'Male',
      },
      {
        id: 'female',
        name: 'Female',
      },
    ],
    optionsExpression: null,
  },
];
