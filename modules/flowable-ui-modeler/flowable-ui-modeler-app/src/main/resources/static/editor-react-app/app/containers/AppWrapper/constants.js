/*
 *
 * AppWrapper constants
 *
 */

export const DEFAULT_ACTION = 'app/AppWrapper/DEFAULT_ACTION';

export const SET_CURRENT_FORM = 'app/AppWrapper/SET_CURRENT_FORM';
export const SET_GLOBAL_VARIABLES = 'app/AppWrapper/SET_GLOBAL_VARIABLES';


export const GET_REQUEST = 'app/AppWrapper/GET_REQUEST';
export const POST_REQUEST = 'app/AppWrapper/POST_REQUEST';
export const DELETE_REQUEST = 'app/AppWrapper/DELETE_REQUEST';
export const PUT_REQUEST = 'app/AppWrapper/PUT_REQUEST';
