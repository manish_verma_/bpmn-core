/*
This file contains the validation call for each element
called using one of the keys of available validators in Validators.js
*/

import dottie from 'dottie';
import validators from './validators';

const singleKeyValidation = (id, value, meta, formData) =>
  validators[id] && validators[id](value, meta, formData);

export const getValidationObj = (ids, value, meta, formData) => {
  // Call the validator with unique identification for a particular Validation
  // This will return the "first" invalid condition for provided identifiers
  let returnObj = {};
  if (Array.isArray(ids)) {
    ids.some(id => {
      returnObj = singleKeyValidation(id, value, meta, formData);
      if (returnObj) {
        return !returnObj.isValid;
      }
      window.logger.error(
        'META',
        `${id} is not a valid validator for ${meta.form_type}`,
        { formData, meta },
      );
      return false;
    });
  } else {
    returnObj = singleKeyValidation(ids, value, meta, formData);
  }
  return returnObj;
};

// Validate step element
export const validateElm = (elmData, value, formData) => {
  const isFieldRequired = dottie.get(elmData, 'required', false);
  let validObj = { isValid: true, errorMessage: '' };
  if (isFieldRequired) {
    // const isFieldRequired = meta.required || meta.required === 'true';
    // const validatorKeys = meta && meta.validator && JSON.parse(meta.validator);
    const validatorKeys = ['isRequired'];
    // if (Array.isArray(validatorKeys) && isFieldRequired) {
    //   validatorKeys.unshift('isRequired');
    // }

    if (validatorKeys && validatorKeys.length) {
      if (isFieldRequired) {
        validObj =
          getValidationObj(validatorKeys, value, elmData, formData) || validObj;
      } else if (!isFieldRequired && !!value) {
        validObj =
          getValidationObj(validatorKeys, value, elmData, formData) || validObj;
      }
    }
  }

  return validObj;
};
