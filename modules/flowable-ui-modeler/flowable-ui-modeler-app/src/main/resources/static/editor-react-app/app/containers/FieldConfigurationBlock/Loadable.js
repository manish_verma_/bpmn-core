/**
 *
 * Asynchronously loads the component for FieldConfigurationBlock
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
