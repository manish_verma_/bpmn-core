import styled from 'styled-components';

const colorMap = {
  darkTheme: '#1f3245',
  lightTheme: '#EFF4F8',
  editorLight: '#F9F9F9',
}

export const HeaderContainer = styled.div`
  background-color: #e8edf1;
  min-height: 54px;
  border-bottom: 1px solid #a4acb9;
  margin: 0px;
`;
export const FullHeightContainer = styled.div`
  height: 100%;
  overflow: auto;
  border-left: ${props => props.borderLeft ? '1px solid #CCC' : 'none'};
  border-right: ${props => props.borderRight ? '1px solid #CCC' : 'none'};
  margin: 0px;
  padding-top: ${props => props.paddingTop ? '15px' : '0px'};
  background: ${props => props.theme && colorMap[props.theme] || '#FFF'};
`;

export const PlayGroundContainer = styled.div`
  height: calc(100% - 54px);
  margin: 0px;
`;