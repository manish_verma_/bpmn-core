/**
 *
 * Divider
 *
 */

import React from 'react';
import { Tabs } from 'antd';

import GlobalVariables from '../GlobalVariables/Loadable';
import Masters from '../Masters/Loadable';

const { TabPane } = Tabs;
export class Divider extends React.PureComponent {
  componentDidMount() {}

  render() {
    return (
      <Tabs>
        <TabPane tab="GlobalVariables" key={1}>
          <GlobalVariables />
        </TabPane>
        <TabPane tab="Masters" key={2}>
          <Masters />
        </TabPane>
      </Tabs>
    );
  }
}

export default Divider;
