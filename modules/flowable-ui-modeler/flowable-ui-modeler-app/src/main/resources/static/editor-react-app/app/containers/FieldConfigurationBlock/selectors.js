import { createSelector } from 'reselect';
import { initialState } from './reducer';
import {
  getPrimaryRenderData,
  getMetaKeysRenderData,
  getFormDataObject,
} from './helper';

/**
 * Direct selector to the fieldConfigurationBlock state domain
 */

const selectFieldConfigurationBlockDomain = state =>
  state.get('fieldConfigurationBlock', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by FieldConfigurationBlock
 */

const makeSelectFieldConfigurationBlock = () =>
  createSelector(selectFieldConfigurationBlockDomain, substate =>
    substate.toJS(),
  );

const makeSelectFormData = () =>
  createSelector(selectFieldConfigurationBlockDomain, substate =>
    substate.get('formMeta'),
  );

const makeSelectSortableFormFields = () =>
  createSelector(selectFieldConfigurationBlockDomain, substate =>
    substate.get('sortableFormFields'),
  );

const makeSelectSelectedIndex = () =>
  createSelector(selectFieldConfigurationBlockDomain, substate =>
    substate.get('selectedIndex'),
  );

const makeSelectRenderData = () =>
  createSelector(selectFieldConfigurationBlockDomain, substate =>
    substate.get('renderData'),
  );

const makeSelectIsMetaValid = () =>
  createSelector(selectFieldConfigurationBlockDomain, substate =>
    substate.get('isMetaValid'),
  );

const makeSelectMetaErrorFlag = () =>
  createSelector(selectFieldConfigurationBlockDomain, substate =>
    substate.get('metaErrorFlag'),
  );

const makeSelectPrimaryData = () =>
  createSelector(
    makeSelectRenderData(),
    substate =>
      substate && Object.keys(substate) ? getPrimaryRenderData(substate) : [],
  );

const makeSelectMetaFieldsData = () =>
  createSelector(
    makeSelectRenderData(),
    substate =>
      substate && Object.keys(substate) ? getMetaKeysRenderData(substate) : [],
  );

const makeSelectFormFieldObject = () =>
  createSelector(makeSelectFormData(), substate => getFormDataObject(substate));

export default makeSelectFieldConfigurationBlock;
export {
  selectFieldConfigurationBlockDomain,
  makeSelectFormData,
  makeSelectSortableFormFields,
  makeSelectSelectedIndex,
  makeSelectRenderData,
  makeSelectIsMetaValid,
  makeSelectPrimaryData,
  makeSelectMetaFieldsData,
  makeSelectFormFieldObject,
  makeSelectMetaErrorFlag,
};
