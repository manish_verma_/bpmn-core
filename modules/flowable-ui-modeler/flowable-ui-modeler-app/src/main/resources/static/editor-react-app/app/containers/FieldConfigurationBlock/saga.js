import { takeEvery, take, put, select } from 'redux-saga/effects';
import dottie from 'dottie';
import {
  VALIDATE_AND_UPDATE_FIELD,
  VALIDATE_ALL_FIELDS,
  UPDATE_SELECTED_INDEX,
  REMOVE_SELECTED_INDEX,
  UPDATE_RENDER_DATA,
  SET_META_VALID,
  SET_RENDER_DATA,
} from 'containers/FieldConfigurationBlock/constants';
import * as validationFns from 'utilities/validations/validationFns';
import { definedValueCheck } from 'utilities/commonUtils';
import {
  makeSelectFormData,
  makeSelectRenderData,
  makeSelectSelectedIndex,
  makeSelectIsMetaValid,
  makeSelectMetaErrorFlag,
} from './selectors';
import {
  setRenderData,
  setSelectedIndex,
  setIsMetaValid,
  validateAllFields as validateAllFieldsAction,
  toggleMetaError,
} from './actions';
import { fieldMetaMap } from './config';
import { getValidatedRenderData, formatRenderData } from './helper';

export default function* fieldConfigurationBlockSaga() {
  yield takeEvery(VALIDATE_AND_UPDATE_FIELD, validateAndUpdateMeta);
  yield takeEvery(VALIDATE_ALL_FIELDS, validateAllField);
  yield takeEvery(UPDATE_SELECTED_INDEX, validateAndSetRenderData);
  yield takeEvery(REMOVE_SELECTED_INDEX, removeSelectedIndex);
  yield takeEvery(UPDATE_RENDER_DATA, updateRenderData);
}

export function* removeSelectedIndex() {
  yield put(setRenderData(null));
  yield put(setSelectedIndex(null));
}

export function* validateAndUpdateMeta(action) {
  const { value, key } = action;
  const renderData = yield select(makeSelectRenderData());
  const formData = yield select(makeSelectFormData());
  const validObj = validationFns.validateElm(renderData[key], value, formData);
  const currentRenderData = { ...renderData };
  // Set errorMessage and invalid flag
  if (currentRenderData[key] && typeof currentRenderData[key] === 'object') {
    currentRenderData[key].isValid = validObj.isValid;
    currentRenderData[key].errorMessage = validObj.errorMessage;
  }
  yield put(setRenderData(currentRenderData));
}

export function* validateAllField() {
  const formData = yield select(makeSelectFormData());
  const renderData = yield select(makeSelectRenderData());
  const selectedIndex = yield select(makeSelectSelectedIndex());
  const metaErrorFlag = yield select(makeSelectMetaErrorFlag());
  if (definedValueCheck(selectedIndex) && formData[selectedIndex] && renderData) {
    const validatedRenderData = getValidatedRenderData(
      formData[selectedIndex],
      renderData,
    );
    const isStepInvalid = Object.keys(renderData).some(
      elmId =>
        validatedRenderData[elmId] &&
        (!validatedRenderData[elmId].isValid ||
          validatedRenderData[elmId].isChildInValid),
    );
    yield put(setIsMetaValid(!isStepInvalid));
    yield put(setRenderData(validatedRenderData));
    yield put(toggleMetaError(!metaErrorFlag));
  } else {
    yield put(setIsMetaValid(true));
    yield put(setRenderData(null));
    yield put(toggleMetaError(!metaErrorFlag));
  }
}

export function* validateAndSetRenderData(action) {
  const selectedIndex = action.data;
  const formMeta = yield select(makeSelectFormData());
  const prevSelectedIndex = yield select(makeSelectSelectedIndex());
  let isValid = true;
  if (definedValueCheck(prevSelectedIndex) && definedValueCheck(formMeta[prevSelectedIndex])) {
    yield put(validateAllFieldsAction());
    yield take(SET_META_VALID); // Wait for validation/setIsMetaValid to complete
    yield take(SET_RENDER_DATA); // Wait for already set renderData (in *validateAllField) to complete
    isValid = yield select(makeSelectIsMetaValid());
  }
  if (isValid) {
    if (definedValueCheck(selectedIndex)) {
      const selectedFormMeta = formMeta[selectedIndex];
      let formType = dottie.get(selectedFormMeta, 'params.meta.form_type');
      const type = dottie.get(selectedFormMeta, 'params.meta.type');
      if (type === 'rest') {
        formType = type;
      }
      // if (formType && fieldMetaMap[formType]) {
        const renderData = formType && fieldMetaMap[formType] ? fieldMetaMap[formType] : fieldMetaMap['customComponent'];
      // } else {
      //   const renderData = fieldMetaMap['customComponent'];
      // }
      const formattedRenderData = formatRenderData(renderData);
      yield put(setRenderData(formattedRenderData));
      yield put(setSelectedIndex(selectedIndex));
    } else {
      yield put(setRenderData(null));
      yield put(setSelectedIndex(null));
    }
  }
}

export function* updateRenderData(action) {
  const { key, value } = action;
  const renderData = yield select(makeSelectRenderData());
  if (key) {
    yield put(
      setRenderData({
        ...renderData,
        [key]: value,
      }),
    );
  } else {
    yield put(setRenderData(value));
  }
}
