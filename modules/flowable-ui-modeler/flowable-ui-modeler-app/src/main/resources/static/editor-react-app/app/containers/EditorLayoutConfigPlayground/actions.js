/*
 *
 * EditorLayoutConfigPlayground actions
 *
 */

import * as C from './constants';

export function defaultAction() {
  return {
    type: C.DEFAULT_ACTION,
  };
}


export function setFormFields(data) {
  return {
    type: C.SET_FORM_FIELDS,
    data
  };
}
