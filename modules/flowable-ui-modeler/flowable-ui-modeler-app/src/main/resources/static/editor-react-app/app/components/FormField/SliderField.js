/**
 *
 * SliderField
 *
 */

import React from 'react';
import { Slider } from 'antd';
import Field from '../Field';

export default class SliderField extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    const {
      errorMessage,
      className,
      helperText,
      options,
      label,
      ...otherProps
    } = this.props;

    return (
      <Field label={label} errorMessage={errorMessage} helperText={helperText}>
        <Slider defaultValue={30} {...otherProps} />
      </Field>
    );
  }
}
