import { dropdownComponentStyle } from 'configs/commonStyles';
import styled from 'styled-components';


export const dropdownStyles = {
  ...dropdownComponentStyle,
};

export const FieldWrapper = styled.div`
  position: relative;
  span.tooltip-wrapper {
    position: absolute;
    right: 8px;
    top: 8px;
  }
`;
