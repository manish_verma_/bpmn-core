/**
 *
 * TextField
 *
 */

import React from 'react';
import { inputStyles, FieldWrapper } from './TextStyles';
import { TextField } from '../FormField';
import { IsJsonString } from '../../utilities/commonUtils';

export default class TextInputField extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  onChangeHandler = e => {
    const { metaId, compProps } = this.props;
    // In case of Input Number, the onChange has the value itself as the parameter
    const value = compProps.type === 'number' ? e : e.target.value;
    const valString = IsJsonString(value) ? JSON.parse(value) : value;
    this.props.onChangeHandler && this.props.onChangeHandler(valString, metaId);
  };

  onBlurHandler = e => {
    const { metaId, compProps } = this.props;
    // In case of Input Number, the onChange has the value itself as the parameter
    const value = compProps.type === 'number' ? e : e.target.value;
    this.props.onBlurHandler && this.props.onBlurHandler(value, metaId);
  };

  render() {
    const { errorMessage, compProps, className } = this.props;
    const { ...componentProps } = compProps;
    return (
      <FieldWrapper key={componentProps.id} className={className}>
        <div className={className}>
          <TextField
            
            errorMessage={errorMessage}
            {...componentProps}
            onChange={e => this.onChangeHandler(e)}
            onBlur={e => this.onBlurHandler(e)}
          />
          {/* {error} */}
          {/* {compProps.helperText ? (
            <span className="tooltip-wrapper">
              <InfoText helperText={compProps.helperText} />
            </span>
          ) : null} */}
        </div>
      </FieldWrapper>
    );
  }
}

TextInputField.propTypes = {};

TextInputField.defaultProps = {
  focusBoolean: true,
  inputStyles,
};
