/*

This file contains all the constants information related to Validations:

Type of constants (as of now):
  Regex
*/

const errorMessages = {
  isRequired: 'This field is required',
};

export default {
  errorMessages,
};
