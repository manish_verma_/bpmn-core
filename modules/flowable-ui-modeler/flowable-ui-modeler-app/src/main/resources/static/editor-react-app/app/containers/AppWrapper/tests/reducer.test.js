import { fromJS } from 'immutable';
import appWrapperReducer from '../reducer';

describe('appWrapperReducer', () => {
  it('returns the initial state', () => {
    expect(appWrapperReducer(undefined, {})).toEqual(fromJS({}));
  });
});
