import React from 'react';
import { Drawer, Input, Button, Icon, Form, Row, Col, Select, Checkbox, message } from 'antd';
import * as commonUtils from '../../utilities/commonUtils';
import * as notifUtils from '../../utilities/notificationUtils';
import { getUnaccountedFormIds } from './helper';
import ImportGlobalVariables from './ImportGlobalVariables';
import ExportGlobalVariables from './ExportGlobalVariables';
import { AntModal } from '../Masters/styles';

const { Option } = Select;
const FormItem = Form.Item;
const EditableContext = React.createContext();

/*
Props:
currentVariables: Array

*/

const SingleVariableBlock = props => {
  const { getFieldDecorator } = props.form;
  let { record, index, removeRecord, checkDuplicatPIDs } = props;
  return (
    <EditableContext.Consumer>
      {form => {
        const { getFieldDecorator } = form;
        return (
          <div>
            <Row gutter={6}>
              <Col span={5}>
                <FormItem>
                  {getFieldDecorator(
                    `data[${props.index}]['processIdentifier']`,
                    {
                      rules: [
                        {
                          required: true,
                          message: 'Please enter a Process Variable Identifier',
                        },
                        {
                          message: 'Duplicate Process Variable Identifier found',
                          validator: (rule, value, cb) =>
                            checkDuplicatPIDs(rule, value, cb, record),
                        },
                        {
                          message: 'Only alphanumeric and underscrore(_) characters are allowed',
                          validator: (rule, value, cb) => {
                            let regExp = new RegExp(/^[a-zA-Z0-9_]+$/);
                            if (!regExp.test(value)) {
                              cb(new Error('Only alphanumeric and underscrore(_) characters are allowed'));
                            }
                            // if (value.indexOf(' ') >= 0 || value.indexOf('-') >= 0) {
                            //   cb(new Error('No space or \'-\' is allowed'));
                            // }
                            cb();
                          }
                        }
                      ],
                      initialValue: record['processIdentifier'],
                    },
                  )(<Input placeholder="Process Variable Identifier" size="small" />)}
                </FormItem>
              </Col>
              <Col span={5}>
                <FormItem>
                  {getFieldDecorator(`data[${props.index}]['name']`, {
                    rules: [{ required: true, message: 'Please enter name' }],
                    initialValue: record['name'],
                  })(<Input placeholder="Name" size="small" />)}
                </FormItem>
              </Col>
              <Col span={4}>
                <FormItem>
                  {getFieldDecorator(`data[${props.index}]['description']`, {
                    initialValue: record['description'],
                  })(<Input placeholder="Description" size="small" />)}
                </FormItem>
              </Col>
              <Col span={3}>
                <FormItem>
                  {getFieldDecorator(`data[${props.index}]['dataType']`, {
                    initialValue: "STRING",
                  })(
                    <Select style={{ width: 140 }} size="small">
                      <Option value="STRING">string</Option>
                      <Option value="BOOLEAN">boolean</Option>
                      <Option value="DATE">date</Option>
                      <Option value="DOUBLE">double</Option>
                      <Option value="INTEGER">integer</Option>
                      <Option value="LONG">long</Option>
                      <Option value="ENCRYPTED">encrypted</Option>
                    </Select>
                  )}
                </FormItem>
              </Col>
              <Col span={4} style={{marginLeft: 25}}>
                <FormItem>
                  {getFieldDecorator(`data[${props.index}]['isCreditVariable']`, {
                    initialValue: 0,
                  })(
                    <Checkbox>isCreditVariable</Checkbox>
                  )}
                </FormItem>
              </Col>
              
              <Col>
                {index !== 0 ? (
                  <Button
                    type="primary"
                    size="small"
                    onClick={e => removeRecord(index, record)}
                    style={{marginTop: 8}}
                  >
                    <Icon type="minus" /> Remove
                  </Button>
                ) : null}
              </Col>
            </Row>
          </div>
        );
      }}
    </EditableContext.Consumer>
  );
};

class AddGlobalVariable extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      count: 1,
      showCount: true,
      showForm: false,
      visible: false,
      globalVariables: "[]",
      visibleExport: false,
      visibleImport: false,
    };
  }

  componentDidMount() {}

  addVariable = () => {
    // Add new globale variable
    let data = [...this.state.data];
    const newData = {
      processIdentifier: '',
      name: '',
      description: '',
      dataType: '',
      isCreditVariable: 0,
      category: 'FORMDATA',
      stepIndex: 1,
      uuid: commonUtils.generateUUID(),
    };
    this.setState({
      data: [...data, newData],
    });
  };

  showDrawer = () => {
    if (this.props.formFields && this.props.formFields.length) {
      // see if there is any unAccounted Data
      const unAccountedPIDs = getUnaccountedFormIds(
        this.props.formFields,
        this.props.currentGlobalVariables,
      );
      const unAccountedVarsToSave = unAccountedPIDs.map(pid => {
        return { processIdentifier: pid, name: pid };
      });
      // self.setState({unAccountedData: unAccountedVarsToSave});

      if (unAccountedVarsToSave && unAccountedVarsToSave.length) {
        const varData = [];
        unAccountedVarsToSave.forEach(data => {
          varData.push({
            processIdentifier: data.processIdentifier,
            name: data.name,
            description: '',
            dataType: data.dataType,
            isCreditVariable: data.isCreditVariable,
            category: 'FORMDATA',
            stepIndex: 1,
            uuid: commonUtils.generateUUID(),
          });
        });

        this.setState({ data: varData });
      } else {
        this.addVariable();
      }
    } else {
      this.addVariable();
    }
    this.setState({
      visible: true,
    });
  };

  toggleImportModal = () => {
    this.setState(prevState => ({
      visibleImport: !prevState.visibleImport,
    }));
  };

  toggleExportModal = () => {
    this.setState(prevState => ({
      visibleExport: !prevState.visibleExport,
    }));
  };

  setglobalVariablelist = (globalVariables) => {
    this.setState(() => ({ globalVariables }));
  }

  download = (filename, text) => {
    const element = document.createElement('a');
    element.setAttribute(
      'href',
      `data:application/json;charset=utf-8,${encodeURIComponent(text)}`,
    );
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  };

  onClose = () => {
    this.setState({
      visible: false,
      data: [],
    });
  };

  removeRecord = (index, record) => {
    // remove the record from the current added data
    const newData = [...this.state.data];
    if (index > -1) {
      newData.splice(index, 1);
      this.setState({ data: newData });
    } else {
      console.debug('not able to find the respective data item');
    }
  };

  triggerBulkSave = data => {
    const self = this;
    let url = FLOWABLE.APP_URL.getGlobalVariableBulkUrl();
    // Build the request data to PUT this variable
    data = data.map(singleData => {
      if (singleData.isCreditVariable === true) {
        return {
          ...singleData,
          isCreditVariable: 1
        }
      } else {
        return {
          ...singleData,
          isCreditVariable: 0
        }
      }
    });
    const reqData = {
      url: url,
      params: [...data],
      successCb: response => {
        if (response) {
          // Once its saved --> update the parent's data if there is any
          self.props.updateParentData(response);
          // Close the Drawer
          self.onClose();
        }
      },
      errorCb: error => {
        // Show the notification for error
        notifUtils.openNotificationWithIcon(errorNotifObj);
      },
    };

    this.props.postRequest({ data: reqData });
  };

  submitVariable = form => {
    form.validateFields((error, formData) => {
      console.debug('what is in submit', error, formData.data);
      let newData = formData.data;
      if (!error) {
        // merge the state.data with the form data
        let varData = [...this.state.data];
        varData.forEach((record, index) => {
          varData[index] = { ...record, ...newData[index] };
          varData[index].processIdentifier = varData[
            index
          ].processIdentifier.trim();
        });

        this.triggerBulkSave(varData);
      }
    });
  };

  checkDuplicatPIDs = (rule, value, callback, record) => {
    const samePIDObjects = this.props.currentGlobalVariables.filter(obj => {
      return obj.processIdentifier.toLowerCase() === value.toLowerCase();
    });
    if (samePIDObjects.length !== 0) {
      callback(new Error('Duplicate Process Variable Identifier found'));
    }
    callback();
  };

  render() {
    const { form } = this.props;
    const data = [...this.state.data];

    const renderingData = data.map((record, index) => {
      return (
        <SingleVariableBlock
          form={form}
          index={index}
          record={record}
          removeRecord={this.removeRecord}
          checkDuplicatPIDs={this.checkDuplicatPIDs}
          key={index}
        />
      );
    });

    return (
      <span>
        <Button type="primary" onClick={this.showDrawer}>
          <Icon type="plus" /> Add Global Variable
        </Button>
        {
          this.props.isImpExpVisible ?
            <>
              <Button type="primary" style={{ marginLeft: 10 }} onClick={this.toggleImportModal}>
                <Icon type="import" /> Import Global Variables
              </Button>
              <Button type="primary" style={{ marginLeft: 10 }} onClick={this.toggleExportModal}>
                <Icon type="export" /> Export Global Variables
              </Button>
            </> :
            null
        }
        <Drawer
          title="Create new Global Variables"
          width={1020}
          onClose={this.onClose}
          visible={this.state.visible}
        >
          <EditableContext.Provider value={form}>
            {renderingData}
            <Button type="primary" size="small" onClick={this.addVariable}>
              <Icon type="plus" /> Add More
            </Button>

            <div
              style={{
                position: 'absolute',
                left: 0,
                bottom: 0,
                width: '100%',
                borderTop: '1px solid #e9e9e9',
                padding: '10px 16px',
                background: '#fff',
                textAlign: 'right',
              }}
            >
              <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                Cancel
              </Button>
              <Button onClick={e => this.submitVariable(form)} type="primary">
                Submit
              </Button>
            </div>
          </EditableContext.Provider>
        </Drawer>
        <AntModal
          title="Import Global Variables"
          centered
          destroyOnClose
          visible={this.state.visibleImport}
          onCancel={this.toggleImportModal}
          footer={null}
        >
          <ImportGlobalVariables
            {...this.props}
            toggleImportModal={this.toggleImportModal}
          />
        </AntModal>
        <AntModal
          title="Export Global Variables"
          centered
          destroyOnClose
          visible={this.state.visibleExport}
          width={800}
          okText="Download File"
          onCancel={this.toggleExportModal}
          onOk={() => this.download(`globalVariables.json`, this.state.globalVariables)}
        >
          <ExportGlobalVariables
            {...this.props}
            toggleExportModal={this.toggleExportModal}
            setglobalVariablelist={this.setglobalVariablelist}
          />
        </AntModal>
      </span>
    );
  }
}

const AddGlobalVariableForm = Form.create()(AddGlobalVariable);

export default AddGlobalVariableForm;