import styled from 'styled-components';

export const Title = styled.h3`
  font-size: 18px;
  text-transform: capitalize;
  margin-bottom: 10px;
  padding-top: 10px;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 10px 0px;
`;

export const H4 = styled.h4`
  font-size: 16px;
`;

export const FormWrapper = styled.div`
  margin-bottom: 12px;
`;

export const ComponentWrapper = styled.div`
	position: relative;
`;

