import 'whatwg-fetch';

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  if (
    response.status === 204 ||
    response.status === 205 ||
    response.status === 401 ||
    response.status === 400
  ) {

    return response.json().then(function (obj) {
      return {status: response.status, response: obj};
    });
    // return { status: response.status, response: response.json() };
  }
  // return response.json();
  
  return response.text().then(function (text) {
    try {
      return JSON.parse(text);
    } catch (e) {
      return text;
    }
  });

  // return response.text().then(function(text) {
  //   return text ? JSON.parse(text) : {}
  // });
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  console.debug('checkStatus response', response);
  if (
    (response.status >= 200 && response.status < 300) ||
    response.status === 401 ||
    response.status === 400
  ) {
    return response;
  }
  console.debug('error in checkStatus', response);
  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {object}           The response data
 */
export default function request(url, options) {
  return fetch(url, options)
    .then(checkStatus)
    .then(parseJSON)
    .then(parsedJSON => {
      console.log(`Fetching ${url} with`, options, 'Response -->', parsedJSON);
      return parsedJSON;
    })
    .catch(error => {
      console.debug(`Fetching ${url} with`, options, 'API error --->', error);
    });
}
