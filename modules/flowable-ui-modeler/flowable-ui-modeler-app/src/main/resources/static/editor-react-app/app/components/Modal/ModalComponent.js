/**
*
* Modal
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

export default class ModalComponent extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

  }

  handleClose = () => {
    //this.setState({ open: false });
    this.props.triggerDialogClose && this.props.triggerDialogClose();   // to setState in parent function
  };

  actionHandler = (e, action) => {
    switch (action.action) {
      case 'hide':
        this.handleClose();

      case 'callback':
        this.props.triggerCallback && this.props.triggerCallback();

    }

    this.handleClose();
  };

  render() {
    const { headerText, actionButtons, preventOverlayClose, children, bodyText, open } = this.props;
    const actionBtns = actionButtons.map((action) => (
      <FlatButton
        label={action.label}
        primary={action.primary}
        onClick={(e) => this.actionHandler(e, action)}
      />
            ));

    return (
      <div>
        <Dialog
          title={headerText}
          actions={actionBtns}
          modal={false}
          open={open}
          autoScrollBodyContent
          contentStyle={{whiteSpace:'pre'}}
          onRequestClose={!preventOverlayClose && this.handleClose}
        >
          {children || bodyText}
        </Dialog>
      </div>
    );
  }
}
