/**
 *
 * RenderButton
 *
 */

import React from 'react';
import { Button } from 'antd';

import buttonStyle from './buttonStyle';

export default class RenderButtonComponent extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {}

  render() {
    const { label, type, onClick, disabled, size, icon } = this.props;
    const button = (
      <Button
        type={type}
        onClick={onClick}
        disabled={disabled}
        size={size}
        icon={icon}
      >
        {label}
      </Button>
    );

    return button;
  }
}

RenderButtonComponent.defautProps = {
  size: 'default',
};

/*
Allowed props
type: primary/secondary
onClick
label
disabled
*/
