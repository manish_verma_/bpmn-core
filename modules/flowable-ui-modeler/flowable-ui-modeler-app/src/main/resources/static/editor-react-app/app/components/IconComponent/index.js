import React from 'react';
import styleVars from 'configs/styleVars';
import { Icon } from 'antd';
const fonts = styleVars.fonts,
  colors = styleVars.colors;

export default class IconComponent extends React.PureComponent {

  onChangeHandler = (e) => {
    this.props.onChangeHandler && this.props.onChangeHandler(e.target.value, this.props.metaId);
  }

  render() {
    const { compProps, metaId } = { ...this.props };
    const color = !compProps.disabled ? '#a0a0a0' : colors.primaryDisableColor;
    const iconStyles = { pointerEvents: compProps.disabled ? 'none' : 'auto', cursor: compProps.disabled ? 'not-allowed' : 'default', cursor: 'pointer' };
    switch (metaId) {
      case 'delete':
        return (
          <Icon
            type="close-circle"
            fill={color}
            style={iconStyles}
            onClick={e => this.onChangeHandler(e)}
          />
        );
      default:
        return null;
    }
  }
}
