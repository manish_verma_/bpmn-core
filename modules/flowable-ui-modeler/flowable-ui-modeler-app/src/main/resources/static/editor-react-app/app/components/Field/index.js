/**
 *
 * Field
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import Label from '../Label';
import RenderError from '../RenderError';
import { FieldWrapper } from './styles';
// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

function Field(props) {
  const { label, errorMessage, helperText, children } = props;
  return (
    <FieldWrapper>
      <Label label={label} helperText={helperText} />
      {children}
      <RenderError errorMessage={errorMessage} />
    </FieldWrapper>
  );
}

Field.propTypes = {
  label: PropTypes.string,
  // errorMessage: PropTypes.oneOfType(PropTypes.string, PropTypes.array),
  helperText: PropTypes.string,
  children: PropTypes.element,
};

export default Field;
