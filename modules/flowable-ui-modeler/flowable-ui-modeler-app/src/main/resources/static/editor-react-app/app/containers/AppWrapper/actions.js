/*
 *
 * AppWrapper actions
 *
 */

import * as C from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function setCurrentForm (data) {
  return {
    type: C.SET_CURRENT_FORM,
    data
  }
}

export function setGlobalVariables (data) {
  return {
    type: C.SET_GLOBAL_VARIABLES,
    data
  }
}

export function getRequest(data) {
  return {
    type: C.GET_REQUEST,
    data,
  };
}

export function postRequest(data) {
  return {
    type: C.POST_REQUEST,
    data,
  };
}

export function putRequest(data) {
  return {
    type: C.PUT_REQUEST,
    data,
  };
}

export function deleteRequest(data) {
  return {
    type: C.DELETE_REQUEST,
    data,
  };
}
