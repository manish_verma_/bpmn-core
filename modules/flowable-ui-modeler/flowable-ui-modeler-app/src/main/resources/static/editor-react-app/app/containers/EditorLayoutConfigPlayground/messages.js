/*
 * EditorLayoutConfigPlayground Messages
 *
 * This contains all the text for the EditorLayoutConfigPlayground container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.EditorLayoutConfigPlayground';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the EditorLayoutConfigPlayground container!',
  },
});
