/*
 *
 * EditorLayoutConfigPlayground reducer
 *
 */

import { fromJS } from 'immutable';
import * as C from './constants';

export const initialState = fromJS({
	formFields: [],
});

function editorLayoutConfigPlaygroundReducer(state = initialState, action) {
  switch (action.type) {
    case C.DEFAULT_ACTION:
      return state;
    case C.SET_FORM_FIELDS:
      return state.
        set('formFields', action.data);
    default:
      return state;
  }
}

export default editorLayoutConfigPlaygroundReducer;
