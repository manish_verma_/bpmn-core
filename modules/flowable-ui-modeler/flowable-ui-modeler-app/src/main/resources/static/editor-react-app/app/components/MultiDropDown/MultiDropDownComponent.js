/**
 *
 * MultiDropDownComponent
 *
 */

import React from 'react';
import { getOptionList } from 'utilities/constants';
import { multiDropdownStyles, FieldWrapper } from './styles';
import { SelectField } from '../FormField';
import RenderError from '../RenderError';

export default class MultiDropDownComponent extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      values: props.compProps.value || [],
    };
  }

  onChangeHandler = option => {
    const { onChangeHandler, metaId } = this.props;
    let newValues = [...option]
    if (metaId === 'validator') {
      newValues = JSON.stringify(option);
    }
    this.setState({ values: option });
    onChangeHandler && onChangeHandler(newValues, metaId);
  };

  componentWillReceiveProps = newProps => {
    const value = newProps.compProps.value || [];
    // if (JSON.stringify(value) !== JSON.stringify(this.state.values)) {
      this.setState({
        values: value,
      });
    // }
  };

  render() {
    const { renderData, className, compProps, errorMessage } = this.props;
    const { values } = this.state;
    const { enums, enumList } = renderData;
    const { floatingLabelText, label, ...componentProps } = compProps;
    let error = null;

    const renderValues = Array.isArray(values) ? [...values] : JSON.parse(values);

    if (errorMessage) {
      error = <RenderError errorMessage={errorMessage} />;
    }
    let optionsList = enumList ? getOptionList(enumList) : enums

    return (
      <FieldWrapper key={componentProps.id} className={className}>
        <div className={className}>
          <SelectField
            {...multiDropdownStyles}
            {...componentProps}
            label={floatingLabelText || label}
            value={renderValues}
            mode="multiple"
            onChange={this.onChangeHandler}
            options={optionsList}
          />
          {error}
        </div>
      </FieldWrapper>
    );
  }
}
