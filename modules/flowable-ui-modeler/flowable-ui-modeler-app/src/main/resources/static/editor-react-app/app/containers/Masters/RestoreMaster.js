import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, message, Input } from 'antd';

import { RestoreContainer } from './styles';
import TrashCard from '../../components/TrashCard';

class RestoreMasters extends Component {
  static propTypes = {
    getRequest: PropTypes.func,
    fetchMastersData: PropTypes.func,
    toggleRestoreModal: PropTypes.func,
    companyName: PropTypes.string,
  };

  static defaultProps = {};

  state = {
    deletedMasters: [],
  };

  componentDidMount() {
    this.fetchDeletedMaster();
  }

  onSearch = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  fetchDeletedMaster = () => {
    // call REST api to fetch results
    const query = `companyName=${this.props.companyName}&isDeleted=true`;
    const url = MASTER.APP_URL.getMastersListUrl(query);
    // Build the request data to get masters data
    const reqData = {
      url,
      params: {},
      successCb: response => {
        if (response.data && Array.isArray(response.data)) {
          this.setState(() => ({ deletedMasters: response.data }));
        }
      },
      errorCb: () => {
        message.error('error occur in fetching deleted master');
      },
    };
    this.props.getRequest({ data: reqData });
  };

  restoreMaster = details => {
    // call REST api
    const query = `slug=${details.masterSlug}`;
    const url = MASTER.APP_URL.restoreMaster(query);
    // Build the request data to restore masters
    const reqData = {
      url,
      params: {},
      successCb: () => {
        message.success('Restore master successfully');
        this.props.toggleRestoreModal();
        this.props.fetchMastersData();
      },
      errorCb: () => {
        // Show the notification for error
        message.error('Master Not Deleted');
      },
    };
    this.props.getRequest({ data: reqData });
  };

  renderMasterCard = details => (
    <TrashCard
      restoreMaster={this.restoreMaster}
      key={details.masterSlug}
      details={details}
    />
  );

  render() {
    let { deletedMasters } = this.state;
    const { searchStr } = this.state;
    if (searchStr) {
      deletedMasters = deletedMasters.filter(Data => {
        const index = Data.masterName
          .toLowerCase()
          .indexOf(searchStr.toLowerCase());
        return !(index < 0);
      });
    }
    return (
      <div>
        <Input
          name="searchStr"
          onChange={this.onSearch}
          placeholder="search master"
          allowClear
          style={{ width: 300, margin: '5px 15px' }}
        />
        <RestoreContainer>
          {!!deletedMasters && deletedMasters.map(this.renderMasterCard)}
        </RestoreContainer>
      </div>
    );
  }
}

export default Form.create({ name: 'RestoreMasters' })(RestoreMasters);
