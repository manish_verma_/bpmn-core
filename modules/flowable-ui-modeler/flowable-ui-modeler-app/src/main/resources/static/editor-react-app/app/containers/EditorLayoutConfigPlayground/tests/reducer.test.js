import { fromJS } from 'immutable';
import editorLayoutConfigPlaygroundReducer from '../reducer';

describe('editorLayoutConfigPlaygroundReducer', () => {
  it('returns the initial state', () => {
    expect(editorLayoutConfigPlaygroundReducer(undefined, {})).toEqual(fromJS({}));
  });
});
