/**
 *
 * DropDownComponent
 *
 */

import React from 'react';
import { getOptionList } from 'utilities/constants';
import { SelectField } from '../FormField';
import { dropdownStyles, FieldWrapper } from './styles';
import RenderError from '../RenderError';

export default class DropDownComponent extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  onChangeHandler = value => {
    const { onChangeHandler, metaId } = this.props;
    onChangeHandler && onChangeHandler(value, metaId);
  };

  filterOptions = (input, option) => {
    const formattedOption =
      typeof option.props.children === 'string'
        ? option.props.children.toLowerCase()
        : option.props.children.toString().toLowerCase();
    return formattedOption.indexOf(input.toLowerCase()) >= 0;
  };

  render() {
    const { renderData, className, compProps, errorMessage } = this.props;
    const { enums, enumList } = renderData;
    const { floatingLabelText, label, ...componentProps } = compProps;
    let error = null;

    if (errorMessage) {
      error = <RenderError errorMessage={errorMessage} />;
    }

    let optionsList = enumList ? getOptionList(enumList) : enums

    return (
      <FieldWrapper key={componentProps.id} className={className}>
        <div className={className}>
          <SelectField
            showSearch
            dropdownStyle={{ minWidth: 150 }}
            {...componentProps}
            label={floatingLabelText || label}
            optionFilterProp="children"
            style={{ width: '100%' }}
            onChange={this.onChangeHandler}
            options={optionsList}
            filterOption={this.filterOptions}
          />
          {error}
        </div>
      </FieldWrapper>
    );
  }
}

DropDownComponent.propTypes = {};

DropDownComponent.defaultProps = {
  focusBoolean: true,
  dropdownStyles,
};
