/*
 *
 * DraggableComponentList reducer
 *
 */

import { fromJS } from 'immutable';
import * as C from './constants';

export const initialState = fromJS({
  componentList: [],
  
});

function draggableComponentListReducer(state = initialState, action) {
  switch (action.type) {
    case C.DEFAULT_ACTION:
      return state;
    case C.SET_COMPONENT_LIST:
      return state.
        set('componentList', action.data);
    default:
      return state;
  }
}

export default draggableComponentListReducer;
