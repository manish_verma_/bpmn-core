import { fromJS } from 'immutable';
import draggableComponentListReducer from '../reducer';

describe('draggableComponentListReducer', () => {
  it('returns the initial state', () => {
    expect(draggableComponentListReducer(undefined, {})).toEqual(fromJS({}));
  });
});
