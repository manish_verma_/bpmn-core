/**
 *
 * Asynchronously loads the component for ArrayField
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
