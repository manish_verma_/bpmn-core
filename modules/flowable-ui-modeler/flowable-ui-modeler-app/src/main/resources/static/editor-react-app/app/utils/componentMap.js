// This mapping of components based on their form type
// This does not include the prebuilt/static components at the moment
// Purely based on form type - unit components

import TextInputField from 'components/TextInputField';
import DropDown from 'components/DropDown';
import CheckboxField from 'components/Checkbox';
import MultiDropDown from 'components/MultiDropDown';
import ArrayField from 'components/ArrayField';
import ObjectField from 'components/ObjectField';
import EnumField from 'components/EnumField';
import IconComponent from 'components/IconComponent';
import EnumDependent from 'components/EnumDependent';
import EC from 'components/FormField';
import RenderButton from 'components/RenderButton';

export const componentMapByFormType = {
  string: TextInputField,
  number: TextInputField,
  boolean: CheckboxField,
  dropdown: DropDown,
  multiselect: MultiDropDown,
  array: ArrayField,
  object: ObjectField,
  enum: EnumField,
  icon: IconComponent,
  enumDependent: EnumDependent,
};

export const editorComponentMapByFormType = {
  text: EC.TextField,
  number: EC.TextField,
  rest: EC.TextField,
  hidden: EC.TextField,
  checkbox: EC.CheckboxField,
  datepicker: EC.TextField,
  radio: EC.TextField,
  dropdown: EC.SelectField,
  'multiselect-dropdown': EC.SelectField,
  slider: EC.SliderField,
  upload: EC.TextField,
  'rest-dropdown': EC.SelectField,
  autocomplete: EC.TextField,
  prebuilt: EC.TextField,
  popup: EC.TextField,
  elasticAutocomplete: EC.SelectField,
  heading: EC.HeadingField,
  download: RenderButton,
  accordion: EC.AccordionField,
  radioAccordion: EC.RadioAccordionField,
  // subJourney: RenderSubJourneyTrigger,
  editableTable: EC.TableField,
  // locator: RenderGeotracking,
  // icon: IconComponent,
  // expression: RenderExpression,
};

export const getEditorComponentMapByFormType = type =>
  editorComponentMapByFormType[type]
    ? editorComponentMapByFormType[type]
    : EC.TextField;
