/**
 *
 * EnumDependent
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import ObjectField from 'components/ObjectField';
import componentProps from 'utils/componentProps';

import { FormattedMessage } from 'react-intl';
import dottie from 'dottie';
import messages from './messages';

const checkboxOptions = [{id: true, name: 'true'}, {id: false, name: 'false'}];

/* eslint-disable react/prefer-stateless-function */
class EnumDependent extends React.Component {
  getRenderData = () => {
    const {
      selectedFieldData,
      compProps: { name },
      metaId,
    } = this.props;
    // Enums for would be 
    // ** either -> the options given to dropdown/radio/multiselect
    // ** or -> true/false (in case of checkbox)
    const enums = selectedFieldData.options || [...checkboxOptions];
    return {
      type: 'object',
      label: name,
      id: metaId,
      description: '',
      objectType: 'userDefined',
      dataObj: [
        {
          placeholder: 'Key',
          id: 'key',
          type: 'dropdown',
          description: '',
          required: true,
          enums: enums,
        },
        {
          placeholder: 'Value',
          id: 'value',
          type: 'multiselect',
          description: '',
          required: true,
          enumList: 'PV_LIST',
        },
      ],
    };
  };

  onBlurHandler = isValid => {
    const { updateRenderData, renderData, metaId } = this.props;
    const newRenderData = {
      ...renderData,
      isChildInValid: isValid,
    };
    updateRenderData(newRenderData, metaId);
  };

  onChangeHandler = (data) => {
    const { metaId } = this.props;
    this.props.onChangeHandler(data, metaId);
  }

  render() {
    const {
      metaId,
      compProps: { value },
      renderData: { type },
    } = this.props;
    const { updateRenderData, ...otherProps } = this.props;
    const getComponentProps = dottie.get(componentProps, type);
    const renderData = this.getRenderData();
    return (
      <div>
        <ObjectField
          {...otherProps}
          renderData={renderData}
          onBlurhandler={this.onBlurhandler}
          onChangeHandler={this.onChangeHandler}
          compProps={getComponentProps(renderData, metaId, value)}
        />
      </div>
    );
  }
}

EnumDependent.propTypes = {};

export default EnumDependent;
