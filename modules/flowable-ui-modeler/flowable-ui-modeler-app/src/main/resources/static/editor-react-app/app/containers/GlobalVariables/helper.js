import _ from 'lodash';

export const getUnaccountedFormIds = (formFields, currentGlobalVars) => {
	// get the ids present in the formFields
	const formIds = formFields.map((field) => field.id);
	// get the PIDs 
	const currentExisitingVars = currentGlobalVars.map((obj) => obj.processIdentifier);
	const unaccountedPIDs = _.difference(formIds, currentExisitingVars);
	return [...unaccountedPIDs];
}
