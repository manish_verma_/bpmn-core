/**
 *
 *EnumField
 *
 */

import React from 'react';

import componentProps from 'utils/componentProps';
import dottie from 'dottie';

import ArrayField from 'components/ArrayField';

const repeater = {
  type: 'object',
  // label: 'API Details',
  id: 'options',
  objectType: 'fixedKeys',
  viewType: 'inline',
  description: '',
  dataObj: [{
    placeholder: 'ID',
    id: 'id',
    type: 'string',
    description: 'Identifier for Option',
    required: true,
  },{
    placeholder: 'Name',
    id: 'name',
    type: 'string',
    description: 'Visible Name of the Option',
    required: true,
  }],
};

export default class EnumField extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {

  }

  onChangeHandler = (value) => {
    const { onChangeHandler, metaId } = this.props;
    onChangeHandler && onChangeHandler(value, metaId);
  }


  renderEnumRenderer = () => {
    const meta = {...this.props.renderData};
    meta.repeater = repeater;

    const defaultValue = this.props.compProps.value || '';
    const getComponentProps = dottie.get(componentProps, meta.type) || {};
    const unitComp = (
      <div>
        <ArrayField
          compProps={getComponentProps(meta, meta.id, defaultValue)}
          metaId={meta.id}
          key={`enum-${meta.id}`}
          renderData={meta}
          onChangeHandler={(val, id) =>
            this.onChangeHandler(val, id)
          }/>
      </div>
    );

    return unitComp;
  }



  render() {
    let renderEnumFields = this.renderEnumRenderer();
    return renderEnumFields;
  }
}

EnumField.propTypes = {};

EnumField.defaultProps = {};
