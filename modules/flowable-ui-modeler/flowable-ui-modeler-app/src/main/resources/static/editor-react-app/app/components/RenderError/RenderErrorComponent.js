/**
*
* RenderError
*
*/

import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import { ErrorMessage, ErrorMessageWrapper } from './ErrorMessage';

export default class RenderErrorComponent extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  render() {
    let { errorMessage } = {...this.props};
    if(!Array.isArray(errorMessage)){
      errorMessage = [errorMessage];
    }

    return (<ErrorMessageWrapper>
      {
        errorMessage.map((error, index) => {
          return (<ErrorMessage key={index}>{error} </ErrorMessage>)
        })
      }
      </ErrorMessageWrapper>
    )

  }
}
