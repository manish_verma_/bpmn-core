/**
 *
 * Asynchronously loads the component for TextField
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
