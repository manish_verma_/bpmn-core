import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

/* Form Editor App's building block Containers */
import DraggableComponentList from 'containers/DraggableComponentList/Loadable';
import EditorLayoutConfigPlayground from 'containers/EditorLayoutConfigPlayground/Loadable';
import FieldConfigurationBlock from 'containers/FieldConfigurationBlock/Loadable';
import EditorFormSave from 'containers/EditorFormSave/Loadable';

/* Global Variable App's building block containers */
import GlobalVariables from 'containers/GlobalVariables/Loadable';

// Divider
import Divider from '../../containers/Divider/Loadable';
import ConfigureHttpTask from '../../containers/ConfigureHttpTask/Loadable';

//
import {
  HeaderContainer,
  FullHeightContainer,
  PlayGroundContainer,
} from './wrappers';

export const buildFormEditorApp = props => {
  return (
    <FullHeightContainer className="">
      <HeaderContainer className="row">
        <div className="col-md-12 col-sm-12">
          <EditorFormSave
            closeEditor={props.closeEditor}
            toggleOldView={props.toggleOldView}
          />
        </div>
      </HeaderContainer>
      <PlayGroundContainer className="row">
        <FullHeightContainer
          theme="darkTheme"
          borderRight
          paddingTop
          className="col-md-2 col-sm-12"
        >
          <DraggableComponentList />
        </FullHeightContainer>
        <FullHeightContainer
          theme="editorLight"
          paddingTop
          className="col-md-7 col-sm-12"
        >
          <EditorLayoutConfigPlayground />
        </FullHeightContainer>
        <FullHeightContainer
          borderLeft
          theme="lightTheme"
          className="col-md-3 col-sm-12"
        >
          <FieldConfigurationBlock />
        </FullHeightContainer>
      </PlayGroundContainer>
    </FullHeightContainer>
  );
};

export const buildGlobalVariablesApp = props => {
  return (
    <FullHeightContainer className="" style={{ overflow: 'hidden' }}>
      <HeaderContainer className="row">
        <div className="col-md-12">
          <div>
            <h4 style={{ lineHeight: '54px' }}>Global Variables</h4>
          </div>
        </div>
      </HeaderContainer>
      <PlayGroundContainer className="row">
        <FullHeightContainer paddingTop className="col-md-12 col-sm-12">
          <Divider />
        </FullHeightContainer>
      </PlayGroundContainer>
    </FullHeightContainer>
  );
};

export const buildHTTPTaskApp = props => {
  return (
    <ConfigureHttpTask 
      updateIntegrationConfig={props.updateIntegrationConfig}
      getIntegrationObj={props.getIntegrationObj}/>
  );
};
