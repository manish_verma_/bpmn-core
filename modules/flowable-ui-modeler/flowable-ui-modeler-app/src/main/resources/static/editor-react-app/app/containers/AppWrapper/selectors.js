import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the appWrapper state domain
 */

const selectAppWrapperDomain = state => state.get('appWrapper', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by AppWrapper
 */

const makeSelectAppWrapper = () =>
  createSelector(selectAppWrapperDomain, substate => substate.toJS());

const makeSelectCurrentForm = () =>
  createSelector(selectAppWrapperDomain, substate => substate.get('currentForm'));

const makeSelectGlobalVariables = () =>
  createSelector(selectAppWrapperDomain, substate => substate.get('globalVariables'));

export default makeSelectAppWrapper;
export { 
	selectAppWrapperDomain, 
	makeSelectCurrentForm,
	makeSelectGlobalVariables,
};
