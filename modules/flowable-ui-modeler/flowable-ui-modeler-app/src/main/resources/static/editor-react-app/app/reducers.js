/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux-immutable';
import { connectRouter } from 'connected-react-router/immutable';

import history from 'utils/history';
import globalReducer from 'containers/App/reducer';
import EditorLayoutConfigPlayground from 'containers/EditorLayoutConfigPlayground/reducer';
import draggableComponentList from 'containers/DraggableComponentList/reducer';
import fieldConfigurationBlock from 'containers/FieldConfigurationBlock/reducer';
import languageProviderReducer from 'containers/LanguageProvider/reducer';

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({
    global: globalReducer,
    EditorLayoutConfigPlayground: EditorLayoutConfigPlayground,
    draggableComponentList: draggableComponentList,
    fieldConfigurationBlock: fieldConfigurationBlock,

    language: languageProviderReducer,
    ...injectedReducers,
  });

  // Wrap the root reducer and return a new root reducer with router state
  const mergeWithRouterState = connectRouter(history);
  return mergeWithRouterState(rootReducer);
}
