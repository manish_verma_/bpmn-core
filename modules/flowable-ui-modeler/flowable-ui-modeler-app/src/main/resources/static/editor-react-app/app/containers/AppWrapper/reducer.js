/*
 *
 * AppWrapper reducer
 *
 */

import { fromJS } from 'immutable';
import * as C from './constants';

export const initialState = fromJS({
	currentForm: {},
  globalVariables: [],
});

function appWrapperReducer(state = initialState, action) {
  switch (action.type) {
    case C.DEFAULT_ACTION:
      return state;
    case C.SET_CURRENT_FORM:
      return state
        .set('currentForm', action.data);
    case C.SET_GLOBAL_VARIABLES:
      return state
        .set('globalVariables', action.data);
    default:
      return state;
  }
}

export default appWrapperReducer;
