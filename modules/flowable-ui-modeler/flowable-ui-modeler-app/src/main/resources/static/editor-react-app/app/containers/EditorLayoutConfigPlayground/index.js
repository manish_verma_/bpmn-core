/**
 *
 * EditorLayoutConfigPlayground
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { Container } from 'react-smooth-dnd';
import { Icon, Badge } from 'antd';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { getEditorComponentMapByFormType } from 'utils/componentMap';
import makeSelectEditorLayoutConfigPlayground from './selectors';

import * as appActions from '../AppWrapper/actions';

import { makeSelectCurrentForm } from '../AppWrapper/selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import * as editorActions from './actions';
import * as editorConstants from './constants';
import { ComponentColumnWrapper, 
          ComponentWrapper, 
          Wrapper, 
          PlaceholderContainer, 
          FormDetailsSection,
          SecondaryWrapper } from './styles';
import './styles.css';

import * as componentListActions from '../DraggableComponentList/actions';
import * as fieldConfigBlockActions from '../FieldConfigurationBlock/actions';
import {
  makeSelectFormData,
  makeSelectSortableFormFields,
  makeSelectSelectedIndex,
  makeSelectFormFieldObject,
} from '../FieldConfigurationBlock/selectors';

import * as data from '../../utilities/dummyData';
import * as dndUtils from '../../utilities/dndUtils';
import * as commonUtils from '../../utilities/commonUtils';
import * as editorUtils from '../../utilities/editorUtils';
import { setGlobalVariablesList } from '../../utilities/constants';

/* eslint-disable react/prefer-stateless-function */
export class EditorLayoutConfigPlayground extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentForm: this.props.currentForm,
      sortableFormFields: [],
      hiddenFields: [],
      dummyFields: [],
    };
  }

  setRowClass = () => {
    const containerArr = document.getElementsByClassName(
      'smooth-dnd-container',
    );
    for (let i = 0; i < containerArr.length; i++) {
      const className = containerArr[i].classList.value;
      if (className.search('row') === -1) {
        containerArr[i].className += ' row';
      }
    }
  };

  setSortableFormFields = formFields => {
    this.setState({ sortableFormFields: formFields });
    // this.setRowClass();
  };

  setFieldUUID = field => {
    field.uuid = commonUtils.generateUUID();
  };

  buildFormItems = () => {
    const currentForm = this.props.currentForm;

    const fields = currentForm.formDefinition.fields;

    if (fields && fields.length) {
      // Set a unique dummmy unique identifier for each field
      fields.forEach(field => {
        if (!field.uuid) {
          this.setFieldUUID(field);
          if (!field.params || !field.params.meta) {
            field.params = {meta: {}};
          }
        }
      });
      // Set the form fields in store
      this.props.setFormFields([...fields]);
      // Set the sortable (drag n drop) list of formFields
      this.sortFormFields([...fields]);
    }
  };


  setFields = (sortableFormFields, hiddenFields, dummyFields) => {
    this.setState({ sortableFormFields, hiddenFields, dummyFields });
  }

  sortFormFields = formFields => {
    // Set hidden fields, dummy fields and sortable array
    let hiddenFields = [],
        dummyFields = [],
        sortableFields = [];

    formFields.forEach((item) => {
      if (item.params.meta.form_type === 'hidden') {
        hiddenFields.push(item);
      } else if (item.params.meta.dummyField) {
        dummyFields.push(item);
      } else {
        sortableFields.push(item);
      }
    })

    sortableFields.sort((a, b) => {
      if (a.params.meta.order < b.params.meta.order) {
        return -1;
      }
      if (a.params.meta.order > b.params.meta.order) {
        return 1;
      }
      return 0;
    });
    this.setFields([...sortableFields], [...hiddenFields], [...dummyFields]);
  };

  setFormFieldsFromSortables = (sortedFormFields, hiddenFields, dummyFields, formFields, isNewField) => {
    let completeFormFields = [...sortedFormFields, ...hiddenFields, ...dummyFields];
    completeFormFields.forEach((field, index) => {
      field.params.meta.order = index;
      // find the field in formFields and set the order there
      const dField = formFields.find(obj => obj.uuid === field.uuid);
      if (dField) {
        dField.params.meta.order = index;
      } 
      // else {
      //   formFields.push({...field});
      // }
    });
    this.props.setFormFields([...formFields]);

    //this.setSortableFormFields([...sortedFormFields]);
  };

  dropHandler = (dropResult, loopIndex, addInNewrow) => {
    const { removedIndex, addedIndex, payload } = dropResult;
    const { selectedIndex } = this.props;

    const sortableFormFields = [...this.state.sortableFormFields],
          hiddenFields = [...this.state.hiddenFields],
          dummyFields = [...this.state.dummyFields];


    const formFields = [...this.props.formFields];

    const isNewField = removedIndex === null && !payload.uuid;
    let itemToAdd = payload;
    if (removedIndex !== null || addedIndex !== null) {
      if (isNewField) {
        // Create a new entry into formFields Array (Insert a new item for the respective componentType)
        itemToAdd = editorUtils.createNewFormField(payload, addedIndex, addInNewrow);
        if (payload.form_type === 'hidden') {
          hiddenFields.push(itemToAdd);
        } else {
          sortableFormFields.splice(loopIndex + addedIndex, 0, itemToAdd);
        }
        formFields.push(itemToAdd);
      } else if (addedIndex !== null) {
        if (payload.form_type !== 'hidden') {
          const payLoadIndex = sortableFormFields.findIndex(
            obj => obj.uuid === payload.uuid,
          );
          // dragging from a different row
          itemToAdd = sortableFormFields.splice(payLoadIndex, 1)[0];
          // Alter the formFields sortable array so the items in the drop container gets rearranged
          sortableFormFields.splice(loopIndex + addedIndex, 0, itemToAdd);
        }
      }
      this.setFormFieldsFromSortables(
        sortableFormFields,
        hiddenFields,
        dummyFields,
        formFields,
        isNewField,
      );
      if (!commonUtils.definedValueCheck(selectedIndex) && isNewField) {
        this.setCurrentSelectedIndex(itemToAdd);
      }
    }
  };

  componentDidUpdate() {
    this.setRowClass();
  }

  componentDidMount() {
    // Set the form fields in the store from the currentForm
    this.buildFormItems();
  }

  componentWillReceiveProps(newProps) {
    const { formFields } = newProps;

    this.sortFormFields([...formFields]);
  }

  setCurrentSelectedIndex = (field, index) => {
    const sortableFormFields = [...this.state.sortableFormFields];

    const formFields = [...this.props.formFields];
    const dFieldIndex = formFields.findIndex(obj => obj.uuid === field.uuid);
    if (this.props.selectedIndex !== dFieldIndex) {
      this.props.updateSelectedIndex(dFieldIndex);
    }
  };

  removeFormField = (field, index, event) => {
    if (event) { 
      event.stopPropagation(); 
    }
    const sortableFormFields = [...this.state.sortableFormFields];

    const formFields = [...this.props.formFields];
    const dFieldIndex = formFields.findIndex(obj => obj.uuid === field.uuid);
    const isSelectedField = (dFieldIndex === this.props.selectedIndex);
    const selectedField = {...formFields[this.props.selectedIndex]};
    // sortableFormFields.splice(index, 1);
    formFields.splice(dFieldIndex, 1);
    // Remove the selected index completely if deleting the currently selected field
    if (isSelectedField) {
      this.props.removeSelectedIndex();
    }
    this.setFormFieldsFromSortables(
      [...sortableFormFields],
      [...this.state.hiddenFields],
      [...this.state.dummyFields],
      [...formFields]);
    if(!isSelectedField) {
      this.props.setSelectedIndex(formFields.findIndex(obj => obj.uuid === selectedField.uuid));
    }
  };

  renderComponent = (fields, formLayoutArea) => {
    const renderRows = [];

    const hiddenFields = [],
          rowClassName = 'row';
    const { formFields, selectedIndex, formFieldObject } = this.props;
    const { uuid: selectedUUID } =
      commonUtils.definedValueCheck(selectedIndex) &&
      commonUtils.definedValueCheck(formFields[selectedIndex]) &&
      formFields &&
      formFields.length
        ? formFields[selectedIndex]
        : {};
    let currentColWidth = 0;
    const renderElms = [];
    let rowIndex = 0;
    if (fields && fields.length) {
      fields.forEach((field, index) => {
        const meta = field.params && field.params.meta;
        const colWidth = meta.colWidth || 4;

        const offset = meta.offset || '';

        if (field) {
          const UnitComponent = getEditorComponentMapByFormType(meta.form_type);
          const isSelected = field.uuid === selectedUUID;
          const componentSection = (
            <ComponentColumnWrapper
              key={field.uuid}
              className={`col-md-${colWidth} offset-md-${meta.offset} ${
                isSelected ? 'selectedComponent' : ''
              }`}
              onClick={() => this.setCurrentSelectedIndex(field, index)}
            >
              <ComponentWrapper>

                <UnitComponent
                  disabled={true}
                  label={field.name}
                  style={{ width: '100%' }}
                  field={field}
                  formFieldObject={formFieldObject}
                />
                <Icon
                  type="close-circle"
                  className="removeBlock"
                  onClick={e => this.removeFormField(field, index, e)}
                />
              </ComponentWrapper>
            </ComponentColumnWrapper>
          );


          currentColWidth += colWidth;
          if (currentColWidth > 12 || meta.newRow) {
            const dIndex = rowIndex;
            if (renderElms.length) {
              renderRows.push(
                <Container
                  key={'row-' + dIndex}
                  className={rowClassName}
                  groupName="1"
                  orientation="horizontal"
                  getChildPayload={i => fields[i + dIndex]}
                  onDrop={e => {
                    this.dropHandler(e, dIndex);
                  }}
                  dropPlaceholder={{className: 'drop-placeholder', animationduration: 400}}
                  dragClass="dragging-element"
                  dropClass="dropping-element"
                >
                  {[...renderElms]}
                </Container>,
              );
              rowIndex = index;
            }
            renderElms.length = 0;
            currentColWidth = colWidth;
          }

          renderElms.push(componentSection);

          // If its the last elem
          if (index === fields.length - 1) {
            if (renderElms.length) {
              const dIndex = rowIndex;
              renderRows.push(
                <Container
                  key={'row-' + dIndex}
                  className={rowClassName}
                  groupName="1"
                  orientation="horizontal"
                  getChildPayload={i => fields[i + dIndex]}
                  onDrop={e => {
                    this.dropHandler(e, dIndex);
                  }}
                  dropPlaceholder={{className: 'drop-placeholder'}}
                >
                  {[...renderElms]}
                </Container>,
              );
            }
          }
        }
      });


      // Below is the drop area for creating extra row/dropping more elements into view
      
      // We don't need it for hidden/dummy fields as those blocks are just representation of those fields 
      // (they don't play a part in determining the layout for the form)
      if (formLayoutArea) {
        renderRows.push(
          <Container
            key={'row-' + fields.length + '-final'}
            className={rowClassName}
            groupName="1"
            orientation="horizontal"
            onDrop={e => {
              this.dropHandler(e, fields.length, true);
            }}
            dropPlaceholder={{className: 'drop-placeholder'}}
            render={setRef => (
              <PlaceholderContainer ref={setRef}>
                <Icon type="plus" size="large" shape="circle"/>
              </PlaceholderContainer>
            )}
          />,
        );
      }
    } else {
      // if there are no elements added yet then create a drop area for dropping/creating new elements
      if (formLayoutArea) {
        renderRows.push(
          <Container
            key={'row-drop-final'}
            groupName="1"
            orientation="horizontal"
            // getChildPayload={i => fields[0]}
            onDrop={e => {
              this.dropHandler(e, 0);
            }}
            dropPlaceholder={{className: 'drop-placeholder'}}
            render={setRef => (
              <PlaceholderContainer ref={setRef}>
                <Icon type="plus" size="large" shape="circle"/>
              </PlaceholderContainer>
            )}
          />,
        );
      }
    }

    return { renderRows };
  };

  render() {
    const {currentForm} = this.props;
    if (!currentForm.formDefinition) {
      return null;
    }

    const sortableFormFields = [...this.state.sortableFormFields];
    const hiddenFields = [...this.state.hiddenFields];
    const dummyFields = [...this.state.dummyFields];

    const renderSortableElms = this.renderComponent(sortableFormFields, true);
    const renderSortableRows = renderSortableElms && renderSortableElms.renderRows;

    const renderHiddenElms = this.renderComponent(hiddenFields);
    const renderHiddenRows = renderHiddenElms && renderHiddenElms.renderRows;

    const renderDummyElms = this.renderComponent(dummyFields);
    const renderDummyRows = renderDummyElms && renderDummyElms.renderRows;

    return (
      <div>
        <Wrapper id="canvas-form-wrapper">
          {[...renderSortableRows]}
        </Wrapper>
        <SecondaryWrapper>
          <h4> Hidden Form Fields </h4>
          <Wrapper id="canvas-hidden-form-wrapper">
            {[...renderHiddenRows]}
          </Wrapper>
        </SecondaryWrapper>
        <SecondaryWrapper>
          <h4> Dummy Form Fields </h4>
          <Wrapper id="canvas-dummy-wrapper">
            {[...renderDummyRows]}
          </Wrapper>
        </SecondaryWrapper>
      </div>
    );
  }
}

EditorLayoutConfigPlayground.propTypes = {
  dispatch: PropTypes.func.isRequired,
  formFieldObject: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  editorState: makeSelectEditorLayoutConfigPlayground(),
  formFields: makeSelectFormData(),
  sortableFormFields: makeSelectSortableFormFields(),
  selectedIndex: makeSelectSelectedIndex(),
  currentForm: makeSelectCurrentForm(),
  formFieldObject: makeSelectFormFieldObject(),
});

function mapDispatchToProps(dispatch) {
  return {
    setComponentList: componentList =>
      dispatch(componentListActions.setComponentList(componentList)),
    setFormFields: formFields =>
      dispatch(fieldConfigBlockActions.setFormMeta(formFields)),
    updateSelectedIndex: index =>
      dispatch(fieldConfigBlockActions.updateSelectedIndex(index)),
    setSelectedIndex: index =>
      dispatch(fieldConfigBlockActions.setSelectedIndex(index)),
    removeSelectedIndex: index =>
      dispatch(fieldConfigBlockActions.removeSelectedIndex()),

    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    setGlobalVariables: (data) => dispatch(appActions.setGlobalVariables(data)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({
  key: 'editorLayoutConfigPlayground',
  reducer,
});
const withSaga = injectSaga({ key: 'editorLayoutConfigPlayground', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(EditorLayoutConfigPlayground);
