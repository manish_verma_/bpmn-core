/**
 *
 * Label
 *
 */

import React from 'react';
import { Tooltip, Icon } from 'antd';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { LabelWrapper, LabelText, iconStyles } from './styles';
import InfoText from '../InfoText';
// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

function Label(props) {
  const { label, helperText } = props;
  if (!label) {
    return null;
  }
  return (
    <LabelWrapper>
      <LabelText>{label}</LabelText>
      <InfoText helperText={helperText} />
    </LabelWrapper>
  );
}

Label.propTypes = {
  label: PropTypes.string,
  helperText: PropTypes.string,
};

export default Label;
