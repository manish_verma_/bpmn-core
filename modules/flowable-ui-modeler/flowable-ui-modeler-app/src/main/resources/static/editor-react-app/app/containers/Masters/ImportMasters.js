import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Upload, Icon, message } from 'antd';

class ImportMasters extends Component {
  static propTypes = {
    postRequest: PropTypes.func,
    fetchMastersData: PropTypes.func,
    toggleImportModal: PropTypes.func,
  };

  static defaultProps = {};

  state = {
    fileList: [],
    error: '',
  };

  handleUpload = (uploadFile, fileList) => {
    // Limit the number of uploaded files
    // Only to show One recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);
    this.setState(() => ({ fileList }));
  };

  onRemoveFile = () => {
    this.setState(() => ({ fileList: [] }));
  };

  uploadFile = () => {
    const { fileList } = this.state;

    const file = fileList[0];
    const formData = new FormData();
    formData.append('file', file);

    const url = MASTER.APP_URL.importMasters();
    // Build the request data to import masters
    const reqData = {
      url,
      contentType: 'multipart/form-data',
      params: formData,
      successCb: () => {
        message.success('import new-masters successfully');
        this.props.toggleImportModal();
        this.props.fetchMastersData();
      },
      errorCb: () => {
        // Show the notification for error
        message.error('Error in import');
        this.setState(() => ({
          error: 'Error!!! may be some of masters allready exits',
        }));
      },
    };
    this.props.postRequest({ data: reqData });
  };

  render() {
    return (
      <div>
        <div>
          <Upload
            onRemove={removefile => this.onRemoveFile(removefile)}
            beforeUpload={(uploadFile, fileList) => {
              this.handleUpload(uploadFile, fileList);
              return false;
            }}
            accept=".json"
            fileList={this.state.fileList}
          >
            <Button style={{ marginTop: '10px' }}>
              <Icon type="upload" /> Select File
            </Button>
            <p style={{ color: 'darkgreen' }}>Select only JSON File...</p>
          </Upload>
        </div>
        <div style={{ marginTop: '10px' }}>
          <Button
            onClick={() => this.uploadFile({ saveToDB: true })}
            disabled={this.state.fileList.length === 0}
          >
            Import
          </Button>
          {this.state.error && (
            <p style={{ color: 'red' }}>{this.state.error}</p>
          )}
        </div>
      </div>
    );
  }
}

export default Form.create({ name: 'ImportMasters' })(ImportMasters);
