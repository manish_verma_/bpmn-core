export const textMeta = {
  type: 'string',
  form_type: 'text',
  default: {
    type: 'string',
    label: 'Default Value',
    id: 'default',
    description: '',
  },
  required: {
    type: 'boolean',
    label: 'Required',
    id: 'required',
    description: '',
    default: true,
  },
  disabled: {
    type: 'boolean',
    label: 'Disabled',
    id: 'disabled',
    description: '',
    default: false,
  },
  placeholder: {
    type: 'boolean',
    label: 'Placeholder',
    id: 'placeholder',
    description: '',
    default: false,
  },
  validator: {
    label: 'Validators',
    id: 'validators',
    type: 'multiselect',
    description: '',
    enums: [
      { id: 'required', name: 'Required' },
      { id: 'mobile', name: 'Mobile Number' },
    ],
  },
  multiline: {
    type: 'boolean',
    label: 'Multiline',
    id: 'multiline',
    description: '',
    default: false,
  },
  hintText: {
    type: 'string',
    label: 'Hint Text',
    id: 'hintText',
    description: '',
  },
};

const NAME_META = {
  metaKey: 'name',
  metaJson: {
    type: 'string',
    label: 'Name',
    id: 'name',
    description: 'Label for the field.',
    required: true,
  },
};

const ID_META = {
  metaKey: 'id',
  metaJson: {
    type: 'dropdown',
    label: 'ID',
    id: 'id',
    enumList: 'PV_LIST',
    required: true,
    description: 'Process Variable',
  },
};

const TYPE_META = {
  metaKey: 'type',
  metaJson: {
    type: 'dropdown',
    label: 'Type',
    id: 'type',
    default: 'string',
    enums: [
      {id: 'string', name: 'String'},
      {id: 'rest', name: 'Rest'},
      {id: 'radio-card', name: 'Radio Card'},
      {id: 'radio-block', name: 'Radio Block'},
      {id: 'radio-image', name: 'Radio Image'},
    ],
    required: false,
    description: 'Type to mention in certain ',
  },
};

const READ_ONLY_META = {
  metaKey: 'readOnly',
  metaJson: {
    type: 'boolean',
    label: 'Read Only',
    id: 'readOnly',
    description: '',
    default: false,
    description: 'If set true, the value of field will be Unwritable.',
  },
};

const OPTIONS_META = {
  metaKey: 'options',
  metaJson: {
    type: 'enum',
    label: 'Options',
    id: 'options',
    description: 'Enum/Options values for radio, multiselect etc',
  },
};

const VALIDATOR_META = {
  metaKey: 'validator',
  metaJson: {
    label: 'Validators',
    id: 'validator',
    type: 'multiselect',
    description: '',
    enumList: 'VALIDATORS',
    description: 'Validations on this field.',
    default: '[]',
  },
};

const CONDITIONAL_VALIDATOR_META = {
  metaKey: 'customValidators',
  metaJson: {
    type: 'array',
    label: 'Custom Validators',
    id: 'customValidators',
    description: 'Create your own multiple validations for this field.',
    default: [],
    repeater: {
      type: 'object',
      objectType: 'fixedKeys',
      dataObj: [
        {
          label: 'Validation Type',
          id: 'type',
          type: 'dropdown',
          enums:[{name: 'Expression', id: 'expression'}, {name: 'Regex', id: 'regex'}],
          description: 'Enter 1) Javascript expression using form keys or 2) A Regex Expression',
        },
        {
          label: 'Expression String',
          id: 'expression',
          type: 'string',
          description: 'Write expression for the field being valid',
        },
        {
          label: 'Error Message',
          id: 'errorMessage',
          type: 'string',
          description: 'Message to show when the condition is invalid',
        },
      ]
    }
  }
}

// CONDITIONAL_PROP_META is to initiate the conditionalMeta with an empty object
// This won't be shown in the field configurator block
// But operate in background whenever a metaId is switched to use a conditional expression
const CONDITIONAL_PROP_META = {
  metaKey: 'conditionalType',
  metaJson: {
    type: 'conditionalType',
    label: 'conditionalType',
    id: 'conditionalMeta',
    description: '',
    default: {},
  },
}

const COL_WIDTH_META = {
  metaKey: 'colWidth',
  metaJson: {
    type: 'dropdown',
    label: 'Column Width',
    id: 'colWidth',
    description: 'Width of this field in a 12 column layout. (12 column = 100% width)',
    enums: [
      {
        name: 1,
        id: 1,
      },
      {
        name: 2,
        id: 2,
      },
      {
        name: 3,
        id: 3,
      },
      {
        name: 4,
        id: 4,
      },
      {
        name: 5,
        id: 5,
      },
      {
        name: 6,
        id: 6,
      },
      {
        name: 7,
        id: 7,
      },
      {
        name: 8,
        id: 8,
      },
      {
        name: 9,
        id: 9,
      },
      {
        name: 10,
        id: 10,
      },
      {
        name: 11,
        id: 11,
      },
      {
        name: 12,
        id: 12,
      },
    ],
    default: 4,
  },
};

const REQUIRED_META = {
  metaKey: 'required',
  metaJson: {
    type: 'boolean',
    label: 'Required',
    id: 'required',
    description: '',
    default: true,
    description: 'if set true, field is required to be filled.'
  },
};

const DUMMY_FIELD_META = {
  metaKey: 'dummyField',
  metaJson: {
    type: 'boolean',
    label: 'Dummy Field',
    id: 'dummyField',
    description: 'If true, the field will be used as a reference rendering point, but won\'t render itself in the browser. (Mostly used in EditableTable or CloningComponent).',
    default: false,
  },
};

const HIDE_META = {
  metaKey: 'hide',
  metaJson: {
    type: 'boolean',
    label: 'Hide',
    id: 'hide',
    default: false,
    description: 'if true, field will not be shown on the page',
    allowConditions: true,
  },
}

const DISABLED_META = {
  metaKey: 'disabled',
  metaJson: {
    type: 'boolean',
    label: 'Disabled',
    id: 'disabled',
    description: '',
    default: false,
    description: 'if set true, field will disabled.',
    allowConditions: true,
  },
};

const MIN_LENGTH_META = {
  metaKey: 'minLength',
  metaJson: {
    type: 'number',
    label: 'Minimum Length Required',
    id: 'minLength',
    description: 'Minimum length of characters required. Should be used with "length" validator',
  },
}

const MAX_LENGTH_META = {
  metaKey: 'maxLength',
  metaJson: {
    type: 'number',
    label: 'Maximum Length Required',
    id: 'maxLength',
    description: 'Maximum length of characters required. Should be used with "length" validator',
  },
}

const PLACEHOLDER_META = {
  metaKey: 'placeholder',
  metaJson: {
    type: 'string',
    label: 'Placeholder',
    id: 'placeholder',
    description: 'Placeholder string',
    default: '',
  },
};

const DISPLAY_ON_SELECTION_META = {
  metaKey: 'display-on-selection',
  metaJson: {
    type: 'enumDependent',
    label: 'Display on selection',
    id: 'display-on-selection',
    description: 'Display fields based on this field\'s selected value. Else keep them hidden.',
  },
};

// discuss how to handle default value for different form type
const DEFAULT_META = {
  metaKey: 'default',
  metaJson: {
    type: 'string',
    label: 'Default Value',
    id: 'default',
    description: '',
    required: false,
    description: '',
    allowConditions: true,
  },
};

const API_META = {
  metaKey: 'api',
  metaJson: {
    // fixedKeys
    type: 'object',
    label: 'API Details',
    id: 'api',
    objectType: 'fixedKeys',
    description: 'API configuration for REST API',
    dataObj: [
      {
        label: 'API Source',
        id: 'apiSource',
        type: 'dropdown',
        description: 'In case of supporting New Masters Data Structure, please select newMasters, else let it be regular',
        default: 'regular',
        enums: [
          {
            name: 'New Masters',
            id: 'newMasters',
          },
          {
            name: 'Regular',
            id: 'regular',
          },
        ],
      },
      {
        label: 'URL',
        id: 'url',
        type: 'string',
        description: 'Enter an absolute URL (Avoid using it, try using sub URL instead)',
        required: false,
      },
      {
        label: 'Method',
        id: 'method',
        type: 'dropdown',
        description: 'API Request Method',
        required: true,
        enums: [
          {
            name: 'POST',
            id: 'POST',
          },
          {
            name: 'GET',
            id: 'GET',
          },
        ],
      },
      {
        // userDefined
        label: 'Data',
        id: 'data',
        type: 'object',
        objectType: 'userDefined',
        dataObj: [
          {
            placeholder: 'Key',
            id: 'key',
            type: 'string',
            description: '',
            required: true,
          },
          {
            placeholder: 'Value',
            id: 'value',
            type: 'string',
            description: '',
            required: true,
          },
        ],
        description: '',
      },
      {
        // userDefined
        label: 'Options Mapping',
        id: 'options-mapping',
        type: 'object',
        objectType: 'userDefined',
        dataObj: [
          {
            placeholder: 'Key',
            id: 'key',
            type: 'string',
            description: '',
          },
          {
            placeholder: 'Value',
            id: 'value',
            type: 'string',
            description: '',
          },
        ],
        description: 'mapping response array indexes to field values',
      },
      {
        // userDefined
        label: 'Mapping',
        id: 'mapping',
        type: 'object',
        objectType: 'userDefined',
        dataObj: [
          {
            placeholder: 'Field',
            id: 'key',
            type: 'dropdown',
            enumList: 'PV_LIST',
            description: '',
          },
          {
            placeholder: 'Value',
            id: 'value',
            type: 'string',
            description: '',
          },
        ],
        description: 'mapping response data to field values',
      },
      {
        label: 'Dependent Fields',
        id: 'dependentFields',
        type: 'object',
        objectType: 'userDefined',
        dataObj: [
          {
            placeholder: 'Field',
            id: 'key',
            type: 'dropdown',
            enumList: 'PV_LIST',
            description: '',
          },
          {
            placeholder: 'Value',
            id: 'value',
            type: 'string',
            description: '',
          },
        ],
      },
      {
        label: 'Prefix Ids',
        id: 'prefix-id',
        type: 'multiselect',
        enums: [
          {
            name: 'POST',
            id: 'POST',
          },
          {
            name: 'GET',
            id: 'GET',
          },
        ],
        enumList: 'PV_LIST', // get process variable list -> for constant defined enums using this enumList key
        description: 'Prefix value of fields in request body',
      },
    ],
  },
};

const API_META_WITH_FALLBACK = {
  ...API_META,
  metaJson: {
    ...API_META.metaJson,
    dataObj: [
      ...API_META.metaJson.dataObj,
      {
        type: 'object',
        label: 'Fallback Option',
        id: 'fallBackOption',
        description: 'A fallback option to be displayed in case of search result does not match with any of the response options',
        objectType: 'fixedKeys',
        dataObj: [{
          label: 'ID',
          id: 'id',
          type: 'string',
          description: '',
        },
        {
          label: 'Name',
          id: 'name',
          type: 'string',
          description: '',
        }]
      }
    ]
  }
}

const CURRENCY_META = {
  metaKey: 'isCurrency',
  metaJson: {
    type: 'boolean',
    label: 'Is Currency',
    id: 'isCurrency',
    description: 'Set true, if the field is representing currency.',
    default: true,
  },
};

const SUFFIX_META = {
  metaKey: 'suffix',
  metaJson: {
    type: 'string',
    label: 'Suffix',
    id: 'suffix',
    description: '',
  },
};

const MULTILINE_META = {
  metaKey: 'multiline',
  metaJson: {
    type: 'boolean',
    label: 'Multiline',
    id: 'multiline',
    description: 'Set true, if Input is should be multiline.',
    default: false,
  },
};

const HINT_TEXT_META = {
  metaKey: 'hintText',
  metaJson: {
    type: 'string',
    label: 'Hint Text',
    id: 'hintText',
    description: 'A suggestive description which comes just below the field',
  },
};

const NEW_ROW_META = {
  metaKey: 'newRow',
  metaJson: {
    type: 'boolean',
    label: 'Create Element in new Row',
    id: 'newRow',
    description: 'Set true, if field should always come in the next row.',
    default: false,
  },
};

// SLIDER META
const IRREGULAR_STEP_META = {
  metaKey: 'isStepIrregluar',
  metaJson: {
    type: 'boolean',
    label: 'Irregular Steps',
    id: 'isStepIrregluar',
    description: 'Set true, if slider does not have regular step values.',
    default: false,
  },
};

const STEP_META = {
  metaKey: 'step',
  metaJson: {
    type: 'number',
    label: 'Step',
    id: 'step',
    description: 'Step interval for Slider.',
    default: false,
  },
};

const MIN_VALUE_META = {
  metaKey: 'min',
  metaJson: {
    type: 'number',
    label: 'Min Value',
    id: 'min',
    description: '',
  },
};

const MAX_VALUE_META = {
  metaKey: 'max',
  metaJson: {
    type: 'number',
    label: 'Max Value',
    id: 'max',
    description: '',
  },
};

const VALUE_ARR_META = {
  metaKey: 'valArr',
  metaJson: {
    type: 'array',
    label: 'Possible Values',
    id: 'valArr',
    description: 'In case of Irregular Steps, enter possible values in sequence.',
    repeater: {
      type: 'number',
      label: 'Value',
      id: 'value',
      description: '',
    },
  },
};

// POPUP META
const BODY_TEXT_META = {
  metaKey: 'bodyText',
  metaJson: {
    type: 'string',
    label: 'Body Text',
    id: 'bodyText',
    description: '',
  },
};

const HEADER_TEXT_META = {
  metaKey: 'headerText',
  metaJson: {
    type: 'string',
    label: 'Header Text',
    id: 'headerText',
    description: '',
  },
};

const SHOW_TRIGGER_META = {
  metaKey: 'showtrigger',
  metaJson: {
    type: 'multiselect',
    label: 'Show trigger type',
    id: 'showtrigger',
    description: '',
    enums: [
      {
        name: 'Field Update',
        id: 'fieldUpdate',
      },
      {
        name: 'Click',
        id: 'click',
      },
    ],
  },
};

const POPUP_TYPE_META = {
  metaKey: 'popupType',
  metaJson: {
    type: 'dropdown',
    label: 'Popup Type',
    id: 'popupType',
    description: '',
    enums: [
      {
        name: 'Info',
        id: 'info',
      },
      {
        name: 'Success',
        id: 'success',
      },
      {
        name: 'Error',
        id: 'error',
      }
    ],
  },
};

const ACTION_BUTTON_META = {
  metaKey: 'actionButtons',
  metaJson: {
    type: 'array',
    label: 'Action Buttons',
    id: 'actionButtons',
    description: '',
    repeater: {
      type: 'object',
      objectType: 'fixedKeys',
      dataObj: [
        {
          label: 'Label',
          id: 'label',
          type: 'string',
          description: '',
        },
        {
          label: 'Action',
          id: 'action',
          type: 'dropdown',
          description: '',
          enums: [
            {
              name: 'Hide',
              id: 'hide',
            },
          ],
        },
      ],
    },
  },
};

const FIELD_UPDATE_META = {
  metaKey: 'triggerConfig',
  metaJson: {
    type: 'object',
    label: 'Popup Triggers Config',
    id: 'triggerConfig',
    description: 'Used to bind Popup to various trigger',
    objectType: 'userDefined',
    dataObj: [
      {
        placeholder: 'Key',
        id: 'key',
        type: 'dropdown',
        description: '',
        colWidth: 100,
        enums: [
          {
            name: 'On Field Update',
            id: 'fieldUpdate',
          },
        ],
      },
      {
        type: 'object',
        placeholder: 'fields',
        id: 'value',
        description: 'Used to bind Popup to various trigger',
        objectType: 'userDefined',
        dataObj: [
          {
            placeholder: 'Key',
            id: 'key',
            type: 'dropdown',
            description: '',
            enumList: 'PV_LIST',
          },
          {
            type: 'array',
            placeholder: 'Possible Values',
            id: 'value',
            description: '',
            repeater: {
              type: 'string',
              placeholder: 'Value',
              id: 'value',
              description: '',
            },
          },
        ],
      },
    ],
  },
};

// datepicker meta
const MAX_DATE_META = {
  metaKey: 'maxDate',
  metaJson: {
    type: 'string',
    label: 'Max Date (DD/MM/YYYY)',
    id: 'maxDate',
    description: '',
  },
};

const MIN_DATE_META = {
  metaKey: 'minDate',
  metaJson: {
    type: 'string',
    label: 'Min Date (DD/MM/YYYY)',
    id: 'minDate',
    description: '',
  },
};

const MIN_AGE_META = {
  metaKey: 'minAge',
  metaJson: {
    type: 'number',
    label: 'Minimum Age',
    id: 'minAge',
    description: 'Minimum Age allowed for this field (used for validation)',
  },
};

const MAX_AGE_META = {
  metaKey: 'maxAge',
  metaJson: {
    type: 'number',
    label: 'Maximum Age',
    id: 'maxAge',
    description: 'Maximum Age allowed for this field (used for validation)',
  },
};

// discuss
// "triggerConfig": { "fieldUpdate": { "includeLoanInsurance": [false] } }

// accordion meta
const IMG_URL_META = {
  metaKey: 'imageUrl',
  metaJson: {
    type: 'string',
    label: 'Icon URL',
    id: 'imageUrl',
    description: 'Icon to show in collapsible panel',
  },
};

const CHILDREN_META = {
  metaKey: 'children',
  metaJson: {
    label: 'Children Fields',
    id: 'children',
    type: 'multiselect',
    description: '',
    enumList: 'PV_LIST',
  },
};

const EXPRESSION_META = {
  metaKey: 'expression',
  metaJson: {
    type: 'string',
    label: 'Expression',
    id: 'expression',
    description: 'Expression for calculating this field\'s value in case this field is dependent on other fields.',
  },
};

const DEPENDENT_FIELDS_META = {
  metaKey: 'dependentFields',
  metaJson: {
    label: 'Dependent Fields',
    id: 'dependentFields',
    type: 'multiselect',
    description: 'Dependent fields on this field (mainly used in expression component).',
    enumList: 'PV_LIST',
  },
};

// radio accordion
const VALUE_HOLDER_META = {
  metaKey: 'valueHolder',
  metaJson: {
    label: 'Value Holder Field ID',
    id: 'valueHolder',
    type: 'dropdown',
    enumList: 'PV_LIST',
    required: true,
    description: 'Field which holds value of selected radio type',
  },
};

// Table meta
// columns: [
//   { fieldId: 'totalValueAnnual', viewOnly: false },
//   { fieldId: 'previousYearTotalAnnual', viewOnly: false },
//   { fieldId: 'currentYearTotalAnnual', viewOnly: false },
//   { fieldId: 'projectionsTotalyear1Annual', viewOnly: false },
//   { fieldId: 'projectionYear2Annual', viewOnly: false },
// ],
const CLONABLE_FIELDS_META = {
  metaKey: 'fields',
  metaJson: {
    type: 'array',
    label: 'Columns / Fields',
    id: 'fields',
    description: 'Columns/Fields for Table/Cloning component. These fields should be chosen from dummy fields',
    repeater: {
      type: 'object',
      objectType: 'fixedKeys',
      viewType: 'inline',
      dataObj: [
        {
          placeholder: 'Field Id',
          id: 'fieldId',
          type: 'dropdown',
          enumList: 'PV_LIST',
          description: '',
          colWidth: 55,
        },
        {
          type: 'boolean',
          label: 'View Only',
          id: 'viewOnly',
          description: '',
          default: false,
          colWidth: 45,
        }
      ],
    },
  },
};

const TABLE_FIELDS_META = {
  metaKey: 'fields',
  metaJson: {
    type: 'array',
    label: 'Columns / Fields',
    id: 'fields',
    description: 'Columns/Fields for Table/Cloning component. These fields should be chosen from dummy fields',
    repeater: {
      type: 'object',
      objectType: 'fixedKeys',
      viewType: 'inline',
      dataObj: [
        {
          placeholder: 'Field Id',
          id: 'fieldId',
          type: 'dropdown',
          enumList: 'PV_LIST',
          description: '',
          colWidth: 100,
        },
        {
          type: 'dropdown',
          label: 'Operation Type',
          id: 'operation',
          description: '',
          default: false,
          colWidth: 55,
          enums: [
            {
              name: 'None',
              id: '',
            },
            {
              name: 'Sum',
              id: 'sum',
            },
            {
              name: 'Mean/Average',
              id: 'mean',
            },
            {
              name: 'Minimum',
              id: 'min',
            },
            {
              name: 'Maximum',
              id: 'max',
            },
            {
              name: 'Count',
              id: 'count',
            }
          ],
        },
        {
          type: 'string',
          label: 'Operation Label',
          id: 'operationLabel',
          description: 'Label for operation to be displayed at the end of table',
          colWidth: 45,
        },
        {
          placeholder: 'Mapping to Global Variable',
          id: 'mapTo',
          type: 'dropdown',
          enumList: 'PV_LIST',
          description: 'Map the result of calculation to a process variable',
          colWidth: 55,
        },
        {
          type: 'boolean',
          label: 'View Only',
          id: 'viewOnly',
          description: '',
          default: false,
          colWidth: 45,
        }
      ],
    },
  },
};

const IS_CALCULATION_TABLE_META = {
  metaKey: 'isCalculationTable',
  metaJson: {
    type: 'boolean',
    label: 'Add calculations to table',
    id: 'isCalculationTable',
    description: 'This will allow to do calculations on Table columns',
    default: false,
  },
}

const SUBHEADING_RECORD_META = {
  metaKey: 'subheading',
  metaJson: {
    type: 'string',
    label: 'Sub Heading',
    id: 'subheading',
    description: 'Sub heading for each block added (in Cloning Component)',
  },
};

const ALLOW_ADD_RECORD_META = {
  metaKey: 'allowAdd',
  metaJson: {
    type: 'boolean',
    label: 'Allow adding more records',
    id: 'allowAdd',
    description: 'This will allow adding more records to table or cloning component',
    default: true,
  },
};

const ALLOW_DELETE_RECORD_META = {
  metaKey: 'allowDelete',
  metaJson: {
    type: 'boolean',
    label: 'Allow deleting records',
    id: 'allowDelete',
    description: 'This will allow deleting the records added to table or cloning component.',
    default: true,
  },
};

const MAX_COUNT_META = {
  metaKey: 'maxCount',
  metaJson: {
    type: 'number',
    label: 'Max Count',
    id: 'maxCount',
    description: 'Maximum number of records that can be added to table or cloning component',
  },
}

const FIXED_LAYOUT_META = {
  metaKey: 'fixedLayout',
  metaJson: {
    type: 'boolean',
    label: 'Fixed Column Layout',
    id: 'fixedLayout',
    description: 'This will enable all the columns having same width in the table',
    default: true,
  },
}

const SHOW_PREVIEW_META = {
  metaKey: 'showPreview',
  metaJson: {
    type: 'boolean',
    label: 'Show Preview',
    id: 'showPreview',
    description: 'This will allow the preview for uploaded file',
    default: true,
  },
};


// Renderers
const rendererMap = {
  string: 'StringRenderer',
  number: 'NumberRenderer',
  array: 'ArrayRenderer',
  dropdown: 'dropdownRenderer',
  boolean: 'BooleanRenderer',
  multiselect: 'MultiselectRenderer',
  object: 'ObjectRenderer',
  pvRenderer: 'PvRendererRenderer',
  valueFieldsRenderer: 'valueFieldsRenderer',
};

// Field Configs
const heading = {
  type: 'string',
  form_type: 'heading',
  default: '',
  order: 1,
  required: false,
  disabled: true,
  placeholder: false,
  api: '',
  validator: '[]',
  lang: { 'en-US': 'Personal Information', 'vi-VN': 'Thông tin cá nhân' },
};


const commonMetaKeys = [
  NAME_META,
  ID_META,
  READ_ONLY_META,
  NEW_ROW_META,
  DISABLED_META,
  DUMMY_FIELD_META,
  HIDE_META,
  TYPE_META,
  COL_WIDTH_META,
  DEFAULT_META,
  HINT_TEXT_META,
]

const commonBottomMetaKeys =[
  PLACEHOLDER_META,
  VALIDATOR_META,
  CONDITIONAL_VALIDATOR_META,
  CONDITIONAL_PROP_META,
]

const headingMeta = {
  type: 'string',
  form_type: 'heading',
  title: 'Heading',
  metaKeys: [
    ...commonMetaKeys,
    ...commonBottomMetaKeys,
  ],
};

// Text
const text = {
  type: 'string',
  form_type: 'text',
  default: '',
  order: 2,
  required: true,
  disabled: false,
  placeholder: false,
  api: '',
  validator: '[]',
};

const TextMeta = {
  type: 'string',
  form_type: 'text',
  title: 'Input Text',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    MIN_LENGTH_META,
    MAX_LENGTH_META,
    MULTILINE_META,
    EXPRESSION_META,
    DEPENDENT_FIELDS_META,
    ...commonBottomMetaKeys,
  ],
};

// Number
const number = {
  type: 'string',
  form_type: 'number',
  default: '',
  order: 2,
  required: true,
  disabled: false,
  placeholder: false,
  api: '',
  validator: '[]',
};

const NumberMeta = {
  type: 'string',
  form_type: 'number',
  title: 'Input Number',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    MIN_LENGTH_META,
    MAX_LENGTH_META,
    MULTILINE_META,
    ...commonBottomMetaKeys,
  ],
};

// REST input
const rest = {
  type: 'rest',
  form_type: 'text',
  default: '',
  order: 10,
  required: true,
  disabled: false,
  placeholder: false,
  api: {
    url: 'http://kuliza.mockable.io/pan',
    method: 'POST',
    data: { pan: 'panApplicant' },
    mapping: {
      firstNameApplicant: 'data.firstName',
      middleNameApplicant: 'data.middleName',
      lastNameApplicant: 'data.lastName',
      cibilScoreApplicant: 'data.cibilScore',
    },
  },
  validator: '[]',
  colWidth: 4,
};

const restMeta = {
  type: 'rest',
  form_type: 'text',
  title: 'Rest Input',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    MIN_LENGTH_META,
    MAX_LENGTH_META,
    API_META,
    ...commonBottomMetaKeys,
  ],
};

// IMP: need to discuss for maxDate
const datepicker = {
  type: 'string',
  form_type: 'datepicker',
  default: '',
  order: 5,
  required: true,
  disabled: false,
  placeholder: false,
  api: '',
  validator: '["dob"]',
  minAge: 0,
  maxDate: '',
  lang: {
    'en-US': 'Date of Birth',
    'vi-VN': 'Ngày tháng năm sinh',
  },
};

const datepickerMeta = {
  type: 'string',
  form_type: 'datepicker',
  title: 'Datepicker',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    // MAX_DATE_META,
    // MIN_DATE_META,
    MAX_AGE_META,
    MIN_AGE_META,
    ...commonBottomMetaKeys,
  ],
};

// Radio
const Radio = {
  type: 'string',
  form_type: 'radio',
  default: '',
  order: 6,
  required: true,
  disabled: false,
  placeholder: false,
  api: '',
  validator: '[]',
};

// for radio dropdown and other selectable field type enumRenderer would be added in block
const radioMeta = {
  type: 'string',
  form_type: 'radio',
  title: 'Radio Buttons',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    OPTIONS_META,
    DISPLAY_ON_SELECTION_META,
    ...commonBottomMetaKeys
  ],
};

// Dropdown
const dropdown = {
  type: 'string',
  form_type: 'dropdown',
  default: '',
  order: 6,
  required: true,
  disabled: false,
  placeholder: false,
  api: '',
  validator: '[]',
  lang: {
    'en-US': 'Marital Status',
    'vi-VN': 'Tình trạng hôn nhân',
  },
};

const dropdownMeta = {
  type: 'string',
  form_type: 'dropdown',
  title: 'Dropdown',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    OPTIONS_META,
    DISPLAY_ON_SELECTION_META,
    ...commonBottomMetaKeys,
  ],
};

const multiselectDropdownMeta = {
  type: 'string',
  form_type: 'multiselect-dropdown',
  title: 'Multiselect Dropdown',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    OPTIONS_META,
    DISPLAY_ON_SELECTION_META,
    ...commonBottomMetaKeys,
  ],
};

// Rest Dropdown
const restDropdown = {
  type: 'string',
  form_type: 'rest-dropdown',
  default: '',
  order: 10,
  required: true,
  disabled: false,
  placeholder: false,
  api: {
    url: 'http://35.200.188.121:8000/api/masters/kuliza-masters/master',
    method: 'GET',
    data: {
      id: '34',
    },
    'options-mapping': {
      id: 'id',
      'name-index': 0,
    },
  },
  validator: '[]',
  lang: {
    'en-US': 'Type of company',
    'vi-VN': 'Loại hình kinh doanh',
  },
};

const restDropDownMeta = {
  type: 'string',
  form_type: 'rest-dropdown',
  title: 'Rest Dropdown',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    // API_META,
    API_META_WITH_FALLBACK,
    ...commonBottomMetaKeys,
  ],
};

// Elastic Auto Complete
const elasticAutocomplete = {
  type: 'string',
  form_type: 'elasticAutocomplete',
  default: '',
  order: 11,
  required: true,
  disabled: false,
  placeholder: false,
  api: {
    url: 'http://129.213.24.224:8001/api/masters/search-master/',
    method: 'POST',
    data: {
      index: 'company_master_updated',
      COM_NAME: 'companyName',
    },
    'prefix-id': ['companyType'],
    dependentFields: {
      companyCategory: 2,
      companyAddress: 9,
      companyType: 6,
    },
    'options-mapping': {
      id: 'id',
      'name-index': 5,
    },
    'depends-on': '[]',
  },
  validator: '[]',
  allowKeyInput: true,
  lang: {
    'en-US': 'Your company',
    'vi-VN': 'Công ty hiện tại',
  },
};

const elasticAutocompleteMeta = {
  type: 'string',
  form_type: 'elasticAutocomplete',
  title: 'Elastic Autocomplete',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    // API_META,
    API_META_WITH_FALLBACK,
    ...commonBottomMetaKeys,
    // DISPLAY_ON_SELECTION_META,
  ],
};

// autocomplete
const autocomplete = {
  type: 'string',
  form_type: 'autocomplete',
  default: '',
  order: 11,
  required: true,
  disabled: false,
  placeholder: false,
  api: {
    url: 'http://35.200.188.121:8000/api/masters/kuliza-masters/master',
    method: 'GET',
    data: { id: '32' },
    mapping: {},
    'options-mapping': { id: 'id', 'name-index': 0 },
    'depends-on': '[]',
    dependentFields: { permanentProvinceCode: 1 },
  },
  validator: '[]',
  lang: { 'en-US': 'Province', 'vi-VN': 'Tỉnh' },
};

const autoCompleteMeta = {
  type: 'string',
  form_type: 'autocomplete',
  title: 'Autocomplete',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    // API_META,
    API_META_WITH_FALLBACK,
    ...commonBottomMetaKeys,
    // DISPLAY_ON_SELECTION_META,
  ],
};

// hidden
const hidden = {
  type: 'string',
  form_type: 'hidden',
  default: '',
  order: 13,
  required: false,
  disabled: false,
  placeholder: false,
  api: {},
  validator: '[]',
};

const hiddenMeta = {
  type: 'string',
  form_type: 'hidden',
  title: 'Hidden field',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    ...commonBottomMetaKeys,
  ],
};

// Slider
const slider = {
  type: 'long',
  form_type: 'slider',
  default: '',
  order: 1,
  step: 1000000,
  min: 5,
  max: 50,
  required: true,
  disabled: false,
  placeholder: false,
  api: {},
  validator: '[]',
  isCurrency: true,
  lang: {
    'en-US': 'Loan Amount',
    'vi-VN': 'Số tiền vay (triệu)',
  },
};

const sliderIrregular = {
  type: 'long',
  form_type: 'slider',
  isStepIrregluar: true,
  valArr: [6, 9, 12, 18, 24, 36],
  required: true,
  step: 1,
  disabled: false,
  placeholder: false,
  api: {},
  validator: '[]',
  default: 6,
  suffix: 'months',
  lang: {
    'en-US': 'Tenure',
    'vi-VN': 'Thời gian vay (tháng)',
  },
};

const sliderMeta = {
  type: 'long',
  form_type: 'slider',
  title: 'Slider',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    IRREGULAR_STEP_META,
    STEP_META,
    MIN_VALUE_META,
    MAX_VALUE_META,
    VALUE_ARR_META,
    CURRENCY_META,
    SUFFIX_META,
    ...commonBottomMetaKeys,
  ],
};

// Checkbox
const checkbox = {
  type: 'string',
  form_type: 'checkbox',
  default: true,
  order: 5,
  required: false,
  disabled: false,
  placeholder: false,
  api: '',
  validator: '[]',
  lang: {
    'en-US': 'I’d like to add insurance with my loan.',
    'vi-VN': 'Tôi muốn đăng ký bảo hiểm cho khoản vay.',
  },
};

const checkboxMeta = {
  type: 'string',
  form_type: 'checkbox',
  title: 'Checkbox',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    DISPLAY_ON_SELECTION_META,
    ...commonBottomMetaKeys,
  ],
};

const Popup = {
  type: 'string',
  form_type: 'popup',
  default: '',
  order: 8,
  required: false,
  disabled: true,
  placeholder: false,
  api: '',
  validator: '[]',
  headerText: {
    lang: {
      'en-US': 'Loan Insurance',
      'vi-VN': 'Bảo hiểm khoản vay',
    },
  },
  bodyText: {
    lang: {
      'en-US':
        'Opting to add Insurance may enable you to receive a higher loan amount',
      'vi-VN':
        'Đăng ký bảo hiểm khoản vay có thể giúp bạn tăng khả năng được duyệt.',
    },
  },
  actionButtons: [
    {
      label: {
        lang: {
          'en-US': 'OK,I got it!',
          'vi-VN': 'Tôi đã hiểu',
        },
      },
      action: 'hide',
    },
  ],
  showtrigger: ['fieldUpdate'],
  popupType: 'info',
  triggerConfig: { fieldUpdate: { includeLoanInsurance: [false] } },
};

const popupMeta = {
  type: 'string',
  form_type: 'popup',
  title: 'Popup',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    // API_META,
    HEADER_TEXT_META,
    BODY_TEXT_META,
    ACTION_BUTTON_META,
    SHOW_TRIGGER_META,
    POPUP_TYPE_META,
    FIELD_UPDATE_META,
    ...commonBottomMetaKeys,
    // { fieldUpdate: { includeLoanInsurance: [false] } },
  ],
};

const upload = {
  type: 'string',
  form_type: 'upload',
  default: '',
  order: 2,
  required: false,
  disabled: false,
  placeholder: false,
  api: {
    url: 'http://{{DMS}}/api/dms/kdms/documents/',
    method: 'POST',
    data: { document_type: 1 },
    mapping: {},
    'depends-on': '[]',
  },
  validator: '[]',
  colWidth: 4,
};

const uploadMeta = {
  type: 'string',
  form_type: 'upload',
  title: 'Upload',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    API_META,
    SHOW_PREVIEW_META,
    ...commonBottomMetaKeys,
  ],
};

const download = {
  type: 'string',
  form_type: 'download',
  default: '',
  order: 1,
  required: false,
  disabled: false,
  placeholder: false,
  api: {},
  validator: '[]',
  docType: 'pdf',
  colWidth: 4,
};

const downloadMeta = {
  type: 'string',
  form_type: 'download',
  title: 'Download',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    ...commonBottomMetaKeys,
  ],
};

const accordion = {
  type: 'string',
  form_type: 'accordion',
  default: '',
  imageUrl: 'http://129.213.24.224:8002/images/personalInfo@3x.png',
  order: 1,
  required: false,
  disabled: true,
  placeholder: false,
  api: '',
  validator: '[]',
  lang: { 'en-US': 'Personal Information', 'vi-VN': 'Thông tin cá nhân' },
  children: [
    'borrowerFullNameFromHyperverge',
    'dateOfBirthFromHyperverge',
    'gender',
  ],
};

const accordionMeta = {
  type: 'string',
  form_type: 'accordion',
  title: 'Accordian',
  metaKeys: [
    ...commonMetaKeys,
    CHILDREN_META,
    IMG_URL_META,
    ...commonBottomMetaKeys,
  ],
};

const radioAccordion = {
  type: 'string',
  form_type: 'radioAccordion',
  default: '',
  order: 1,
  required: false,
  disabled: false,
  placeholder: false,
  api: '',
  validator: '[]',
  imageUrl: 'http://129.213.24.224:8002/images/cash@3x.png',
  children: ['collectCashEnum'],
  valueHolder: 'methodOfDisbursal',
  lang: { 'en-US': 'Collect Cash', 'vi-VN': 'Nhận tiền mặt' },
};

const radioAccordionMeta = {
  type: 'string',
  form_type: 'radioAccordion',
  title: 'Radio Accordian',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    CHILDREN_META,
    VALUE_HOLDER_META,
    IMG_URL_META,
    ...commonBottomMetaKeys,
  ],
};

const editableTable = {
  type: 'string',
  form_type: 'editableTable',
  columns: [
    { fieldId: 'totalValueAnnual', viewOnly: false },
    { fieldId: 'previousYearTotalAnnual', viewOnly: false },
    { fieldId: 'currentYearTotalAnnual', viewOnly: false },
    { fieldId: 'projectionsTotalyear1Annual', viewOnly: false },
    { fieldId: 'projectionYear2Annual', viewOnly: false },
  ],
  default: [
    {
      totalValueAnnual: 'Total Revenue',
      previousYearTotalAnnual: '',
      currentYearTotalAnnual: '',
      projectionsTotalyear1Annual: '',
      projectionYear2Annual: '',
    },
    {
      totalValueAnnual: 'Total Raw Material',
      previousYearTotalAnnual: '',
      currentYearTotalAnnual: '',
      projectionsTotalyear1Annual: '',
      projectionYear2Annual: '',
    },
    {
      totalValueAnnual: 'Other expenses Total',
      previousYearTotalAnnual: '',
      currentYearTotalAnnual: '',
      projectionsTotalyear1Annual: '',
      projectionYear2Annual: '',
    },
    {
      totalValueAnnual: 'PBDIT total',
      previousYearTotalAnnual: '',
      currentYearTotalAnnual: '',
      projectionsTotalyear1Annual: '',
      projectionYear2Annual: '',
    },
    {
      totalValueAnnual: 'PBDIT %',
      previousYearTotalAnnual: '',
      currentYearTotalAnnual: '',
      projectionsTotalyear1Annual: '',
      projectionYear2Annual: '',
    },
    {
      totalValueAnnual: 'PAT Total',
      previousYearTotalAnnual: '',
      currentYearTotalAnnual: '',
      projectionsTotalyear1Annual: '',
      projectionYear2Annual: '',
    },
    {
      totalValueAnnual: 'Total Income',
      previousYearTotalAnnual: '',
      currentYearTotalAnnual: '',
      projectionsTotalyear1Annual: '',
      projectionYear2Annual: '',
    },
  ],
  order: 5,
  required: false,
  disabled: true,
  placeholder: false,
  api: '',
  validator: '[]',
  maxRowCount: 15,
  colWidth: 12,
  newRow: true,
};

const editableTableMeta = {
  type: 'string',
  form_type: 'editableTable',
  title: 'Editable Table',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    IS_CALCULATION_TABLE_META,
    TABLE_FIELDS_META,
    ALLOW_ADD_RECORD_META,
    ALLOW_DELETE_RECORD_META,
    FIXED_LAYOUT_META,
    MAX_COUNT_META,
    //VALUE_HOLDER_META,
    ...commonBottomMetaKeys,
  ],
};

const clonableBlockMeta = {
  type: 'string',
  form_type: 'cloning',
  title: 'Clonable Block',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    CLONABLE_FIELDS_META,
    SUBHEADING_RECORD_META,
    ALLOW_ADD_RECORD_META,
    ALLOW_DELETE_RECORD_META,
    MAX_COUNT_META,

    // VALUE_HOLDER_META,
    ...commonBottomMetaKeys,
  ],
};

const expression = {
  "type": "string",
  "form_type": "text",
  "default": "",
  "order": 6,
  "required": true,
  "disabled": false,
  "placeholder": false,
  "api": "",
  "expression": "((amount * rate) / (1 - (1 / (1 + rate)^(tenure))))",
  "dependentFields": ["amount", "rate", "tenure"],
  "validator": "[]",
  "colWidth": 6
}


const customComponentMeta = {
  type: 'string',
  form_type: 'customComponent',
  title: 'Custom Component',
  metaKeys: [
    REQUIRED_META,
    ...commonMetaKeys,
    ...commonBottomMetaKeys,
  ],
};

// in fieldMetaMap all the keys are the form_type for each of the component
export const fieldMetaMap = {
  text: TextMeta,
  number: NumberMeta,
  heading: headingMeta,
  rest: restMeta,
  popup: popupMeta,
  checkbox: checkboxMeta,
  hidden: hiddenMeta,
  slider: sliderMeta,
  autocomplete: autoCompleteMeta,
  elasticAutocomplete: elasticAutocompleteMeta,
  'rest-dropdown': restDropDownMeta,
  radio: radioMeta,
  dropdown: dropdownMeta,
  upload: uploadMeta,
  download: downloadMeta,
  datepicker: datepickerMeta,
  'multiselect-dropdown': multiselectDropdownMeta,
  accordion: accordionMeta,
  radioAccordion: radioAccordionMeta,
  editableTable: editableTableMeta,
  cloning: clonableBlockMeta,
  customComponent: customComponentMeta,
};

export const componentConfigList = [
  TextMeta,
  NumberMeta,
  restMeta,
  checkboxMeta,
  headingMeta,
  popupMeta,
  hiddenMeta,
  sliderMeta,
  autoCompleteMeta,
  elasticAutocompleteMeta,
  restDropDownMeta,
  radioMeta,
  dropdownMeta,
  multiselectDropdownMeta,
  uploadMeta,
  downloadMeta,
  datepickerMeta,
  accordionMeta,
  radioAccordionMeta,
  editableTableMeta,
  clonableBlockMeta,
  customComponentMeta,
];
