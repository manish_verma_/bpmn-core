import styleVars from 'configs/styleVars';

const { colors, fonts } = styleVars;

const commonButtonStyleObj = {
  style: {
    margin: '15px',
  },
  labelStyle: {
    fontSize: fonts.buttonFontSize,
    lineHeight: fonts.buttonLineHeight,
  },
  buttonStyle: {
    height: '50px',
    width: '150px',
    backgroundColor: colors.secondaryBGColor,
  },
  labelColor: colors.secondaryFontColor,
};

const primaryStyle = {
  style: { ...commonButtonStyleObj.style },
  labelColor: colors.primaryFontColor,
  buttonStyle: {
    ...commonButtonStyleObj.buttonStyle,
    backgroundColor: colors.primaryBGColor,
  },
  labelStyle: { ...commonButtonStyleObj.labelStyle },
};

const secondaryStyle = {
  style: { ...commonButtonStyleObj.style },
  labelColor: colors.secondaryFontColor,
  buttonStyle: {
    ...commonButtonStyleObj.buttonStyle,
    backgroundColor: colors.secondaryBGColor,
  },
  labelStyle: { ...commonButtonStyleObj.labelStyle },
};

export default function buttonStyle(type) {
  if (type === 'primary') {
    return { ...primaryStyle };
  }
  if (type === 'secondary') {
    return { ...secondaryStyle };
  }
  return { ...secondaryStyle };
}
