import { inputComponentStyle } from 'configs/commonStyles';
import styled from 'styled-components';
export const inputStyles = {
  ...inputComponentStyle,
};

export const FieldWrapper = styled.div`
  position: relative;
  span.tooltip-wrapper {
    position: absolute;
    right: 8px;
    top: 8px;
  }
`;

export const ButtonWrapper = styled.div`
  text-align: right;
  margin: 0px 15px 5px 0px;
`;

export const FieldRow = styled.div`
  display: ${props =>
    props.type === 'userDefined' || props.viewType === 'inline'
      ? 'flex'
      : 'block'};
  flex-wrap: wrap;
  position: relative;

  .rowCell.icon-row {
    flex: 0;
    padding-top: 0px;
    opacity: 0;
    position: absolute;
    right: -8px;
    top: 3px;
  }
  &:hover {
    & > .rowCell.icon-row {
      opacity: 1;
    }
  }
`;

export const CellRow = styled.div`
  width: ${props =>
    props.type === 'userDefined' || props.viewType === 'inline'
      ? `${100 / props.columnCount}%`
      : 'auto'};
  padding-right: ${props =>
    props.type === 'userDefined' || props.viewType === 'inline' ? '10px' : '0'};
  flex: ${props => (props.colWidth ? `1 0 ${props.colWidth}%` : 1)} !important;
`;

export const HeadingWrapper = styled.h5`
  font-size: 14px;
  padding: 10px 0px;
  margin-bottom: 0px;
`;

export const DataWrapper = styled.div`
  background-color: #f9f9f9;
  padding: ${props => (props.level === 0 ? '10px 13px' : '0px')};
`;

export const HeadingText = styled.span`
  display: inline-block;
  margin-right: 5px;
`;

export const iconStyles = {
  verticalAlign: 'middle',
};

export const LinkText = styled.span`
  vertical-align: middle;
  display: inline-block;
  margin-left: 5px;
`;
