/**
 *
 * RenderConditionInput
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { Input } from 'antd';
import Field from 'components/Field';

/* eslint-disable react/prefer-stateless-function */
class RenderConditionInput extends React.PureComponent {

	onChangeHandler = (e) => {
		this.props.onChangeHandler(e.target.value)
	}

  render() {
  	const {label, helperText, value, placeHolder, errorMessage} = this.props;

    return (
      <Field label={label} errorMessage={errorMessage} helperText={helperText}>
	        <Input.TextArea 
	          value={value}
	          onChange={this.onChangeHandler}
	          placeHolder={placeHolder || 'Write a conditional statement based upon form keys..'}
	          autosize={{minRows: 3}}
	        />
      </Field>
    );
  }
}

RenderConditionInput.propTypes = {};

export default RenderConditionInput;
