import styled from 'styled-components';
import { Button as StyledButton } from 'antd';

export const Wrapper = styled.div`
  display: flex;
  margin: 5px;
  width: 18%;
`;

export const Card = styled.div`
  background-color: gainsboro;
  padding: 10px;
  width: 100%;
  border-radius: 10px;
`;

export const Name = styled.p`
  font-size: 18px;
  width: 100%;
  font-family: OpenSans-Semibold;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  cursor: pointer;
`;

export const Button = styled(StyledButton)`
  font-size: 11px;
  font-weight: 600;
`;

export const Seprator = styled.div`
  height: 15px;
  margin: 0 8px;
  width: 1px;
  background-color: 'white;
`;

export const NameTag = styled.span`
  color: peru;
`;
