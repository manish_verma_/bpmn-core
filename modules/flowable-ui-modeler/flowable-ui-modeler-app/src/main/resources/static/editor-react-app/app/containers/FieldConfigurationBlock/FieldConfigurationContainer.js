/**
 *
 * FieldConfigurationContainer
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { componentMapByFormType } from 'utils/componentMap';
import componentProps from 'utils/componentProps';
import dottie from 'dottie';
import brace from 'brace';
import AceEditor from 'react-ace';

import 'brace/mode/json';
import 'brace/theme/github';
import 'brace/theme/monokai';

import ModalComponent from 'components/Modal';
import RenderButton from 'components/RenderButton';
import RenderError from 'components/RenderError';
import { DUMMY_FORM_META } from './constants';
import { ButtonWrapper, H4, FormWrapper, FieldWrapper, Title, ComponentWrapper } from './styles';
import * as commonUtils from 'utilities/commonUtils';
import { fieldMetaMap } from './config';

import RenderConditionInput from 'components/RenderConditionInput';

import { Tabs, Input, message, Modal, Button, Icon, Tooltip } from 'antd';
const TabPane = Tabs.TabPane;

const inValidJsonError = 'INVALID JSON, please input a valid JSON string';
/* eslint-disable react/prefer-stateless-function */
export class FieldConfigurationContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      openMetaStringModal: false,
      metaString: '',
      metaStringError: '',
      showConditionalMetaFlag: {},
      conditionalMetaData: {},
      toggleTab: false,
      aceInstace: null,
    };
    this.toggleDialog = this.toggleDialog.bind(this);
  }

  setConditionals = (formData, selectedFieldData, metaFieldData) => {
    // set the showConditionalMetaFlag s for eligible metaIds
    const showConditionalMetaFlag = {},
          stateConditionalMetaData = {};

    if (selectedFieldData) {
      // check for what all metaIds conditionalMeta exists
      metaFieldData.forEach((meta) => {
        const conditionalMeta = selectedFieldData.params.meta.conditionalMeta;
        if (conditionalMeta && commonUtils.definedValueCheck(conditionalMeta[meta.id])) {
          showConditionalMetaFlag[meta.id] = true;
          stateConditionalMetaData[meta.id] = conditionalMeta[meta.id]
        }
      });
    }
    this.setState({ showConditionalMetaFlag, conditionalMetaData: stateConditionalMetaData });
  }

  componentWillReceiveProps(newProps) {
    const { formData, selectedIndex, metaFieldData } = newProps;
    if (!(typeof selectedIndex === 'undefined' || selectedIndex === null) && formData[selectedIndex]) {
      // set the metaString from the selected field's meta 
      const selectedFieldData = formData[selectedIndex];
      this.setState({
        metaString: JSON.stringify(selectedFieldData.params.meta),
        editorMetaString: JSON.stringify(selectedFieldData.params.meta, null, 4)
      });
      if (!newProps.isMetaValid && (this.props.metaErrorFlag !== newProps.metaErrorFlag)) {
        this.showMetaError();
      }
      if (selectedIndex !== this.props.selectedIndex && metaFieldData ) {
        this.setConditionals(formData, selectedFieldData, metaFieldData);
      }
    }
  }

  showMetaError = () => {
    message.error('Please fill all the properties for the selected field properly');
  }

  // On blur handler for elements
  onBlurHandler = (value, elmId) => {
    this.props.validateAndUpdateField(value, elmId);
  };

  onChangeHandler = (value, elmId) => {
    // updateFormData
    this.props.updateMeta(value, elmId);
    this.props.validateAndUpdateField(value, elmId);
  };

  closeFieldConfiguration = () => {
    // set the selected index to null
    // Below call itself will handle all the validation for opened item (if any)
    this.props.updateSelectedIndex(null);
  }

  toggleConditionForMetaId = (metaId) => {
    // Check if the conditionalMeta Object contains 
    if (!this.state.showConditionalMetaFlag[metaId]) {
      this.setState({showConditionalMetaFlag: {...this.state.showConditionalMetaFlag, [metaId]: true}})
    } else {
      this.setState({showConditionalMetaFlag: {...this.state.showConditionalMetaFlag, [metaId]: false}})
      // if showConditionalMetaFlag is false then delete that key from the conditionalMeta 
      this.deleteConditionalMetaId(metaId);
    }
  }

  deleteConditionalMetaId = (metaId) => {
    const conditionalMetaData = {...this.state.conditionalMetaData};
    if (commonUtils.definedValueCheck(conditionalMetaData[metaId])) {
      delete conditionalMetaData[metaId];
    }
    this.onChangeHandler({...conditionalMetaData}, 'conditionalMeta');
  }

  updateConditionalMetaId = (metaId, value) => {
    const conditionalMetaData = {...this.state.conditionalMetaData, [metaId]: value};
    this.setState({conditionalMetaData: conditionalMetaData});
    this.onChangeHandler({...conditionalMetaData}, 'conditionalMeta');
  };

  showConditionalConfigurator = (metaId) => {
    return this.state.showConditionalMetaFlag[metaId] && metaId !== 'validator';
  }

  renderComponents = (metaList, type) => {
    const { formData, selectedIndex } = this.props;
    const selectedFieldData = formData[selectedIndex];
    if (!selectedFieldData) { 
      return null;
    }
    return metaList.map(meta => {
      const UnitComponent = componentMapByFormType[meta.type];
      if (UnitComponent) {
        const getComponentProps = dottie.get(componentProps, meta.type);
        const defaultValue =
          type === 'meta'
            ? dottie.get(selectedFieldData, `params.meta.${meta.id}`)
            : dottie.get(selectedFieldData, meta.id);
        return (
          <ComponentWrapper key={'wrapper-' + meta.id}>
            {
              !this.showConditionalConfigurator(meta.id) ?
                (<UnitComponent
                          key={`ku-${meta.type}-${meta.id}-${selectedFieldData.uuid}`}
                          compProps={
                            getComponentProps
                              ? getComponentProps(meta, meta.id, defaultValue)
                              : {}
                          }
                          toggleTab={this.state.toggleTab}
                          metaId={meta.id}
                          onBlurHandler={this.onBlurHandler}
                          onChangeHandler={this.onChangeHandler}
                          renderData={meta}
                          errorMessage={meta.errorMessage}
                          formData={formData}
                          selectedFieldData={formData[selectedIndex]}
                          updateRenderData={this.props.updateRenderData}
                          globalVariables={this.props.globalVariables}
                        />)
                :


                (<RenderConditionInput
                  label={meta.label}
                  helperText={meta.description}
                  value={this.state.conditionalMetaData[meta.id]}
                  onChangeHandler={(val) => this.updateConditionalMetaId(meta.id, val)}
                />)

            }
            {meta.allowConditions ? 
              (<Tooltip placement="left" title={'Toggle conditional statements for this meta field'}>
                <Icon type="clock-circle" style={{position: 'absolute', top: 16, right: 0}} onClick={(e) => this.toggleConditionForMetaId(meta.id)}/>
              </Tooltip>) 
              : null}
          </ComponentWrapper>
        );
      }
      return null;
    });
  };

  toggleDialog = () => {
    this.setState({
      open: !this.state.open,
    });
  };

  toggleMetaStringDialog = () => {
    this.setState({
      openMetaStringModal: !this.state.openMetaStringModal,
      editorMetaString: JSON.stringify(JSON.parse(this.state.metaString), null, 4)
    });
  };

  renderJSON = () => {
    const { formData, selectedIndex } = this.props;
    return JSON.stringify(formData[selectedIndex], null, 4);
  };

  onEditorMetaStringChange = (value) => {
    let metaString = value;
    this.updateEditorMetaString(metaString);
  }

  updateEditorMetaString = (value) => {
    let editorMetaString = value;
    // Check if the string is a valid JSON string
    let isJson = commonUtils.IsJsonString(editorMetaString);
    if (isJson) {
      // update the parsed json to this field's meta 
      this.props.updateFieldMeta(JSON.parse(editorMetaString));
    } else {
      // set the errorMessage for invalid JSON
      // this.setState({metaStringError : inValidJsonError});
    }
    this.setState({ editorMetaString });
  }

  beautifyMeta = (e) => {
    if (commonUtils.IsJsonString(this.state.editorMetaString)) {
      this.setState({editorMetaString: JSON.stringify(JSON.parse(this.state.editorMetaString), null, 4)});
    }
  }

  updateMetaString = (value) => {
    let metaString = value;
    // Check if the string is a valid JSON string
    let isJson = commonUtils.IsJsonString(metaString);
    if (isJson) {
      // update the parsed json to this field's meta 
      this.props.updateFieldMeta(JSON.parse(metaString));
      this.setState({metaStringError : ''});
    } else {
      // set the errorMessage for invalid JSON
      this.setState({metaStringError : inValidJsonError});
    }
    this.setState({ metaString });
  }

  triggerTabChange = () => {
    const toggleTab = !this.state.toggleTab;
    this.setState({ toggleTab });
  }

 

  render() {
    if (!commonUtils.definedValueCheck(this.props.selectedIndex)) {
      return null;
    }
    
    const {
      primaryData,
      metaFieldData,
      validateAllFields,
      selectedIndex,
      formData,
    } = this.props;

    const selectedField = formData[selectedIndex];
    if (!selectedField) {
      return null;
    }
    const formType = dottie.get(selectedField, `params.meta.form_type`);
    const type = dottie.get(selectedField, `params.meta.type`);

    const metaFieldConfigurator = (
        metaFieldData && metaFieldData.length ? (
          <FormWrapper>
            {this.renderComponents(metaFieldData, 'meta')}
          </FormWrapper>
        ) : null
    );

    const AceEditorTitle = (
      <div>
        <span>Meta Editor</span>
        <Button type="primary" onClick={this.beautifyMeta} style={{float: 'right', marginRight: 25}}>
          Beautify
        </Button>
      </div>
    );

    const metaStringConfigurator = (
      <div>
        <Input.TextArea 
          value={this.state.metaString}
          onChange={(e) => this.updateMetaString(e.target.value)}
          autosize={{minRows: 5}}
          onClick={this.toggleMetaStringDialog}
          />
        <RenderError errorMessage={this.state.metaStringError} />

        <Modal
          width={'70%'}
          style={{top:3}}
          title={AceEditorTitle}
          visible={this.state.openMetaStringModal}
          onOk={this.toggleMetaStringDialog}
          onCancel={this.toggleMetaStringDialog}
          footer={[
            <Button key="close" onClick={this.toggleMetaStringDialog}>Close</Button>,
          ]}
        >
          <div>
            <AceEditor
              width='100%'
              height='400px'
              fontSize={14}
              placeholder="Placeholder Text"
              mode="json"
              theme="monokai"
              name="metaStringEeditor"
              onChange={this.onEditorMetaStringChange}
              showPrintMargin={true}
              showGutter={true}
              highlightActiveLine={true}
              wrapEnabled={true}
              value={this.state.editorMetaString}
              setOptions={{
                enableBasicAutocompletion: false,
                enableLiveAutocompletion: false,
                enableSnippets: false,
                showLineNumbers: true,
                tabSize: 4,
              }}
            />
          </div>
        </Modal>
      </div>
    )

    const regularTabBlock = (
      <Tabs defaultActiveKey="1" onChange={this.triggerTabChange}>
        <TabPane tab='Meta Fields' key="1">
          {metaFieldConfigurator}
        </TabPane>
        <TabPane tab='Meta String' key="2">
          {metaStringConfigurator}
        </TabPane>
      </Tabs>
    );

    const customComponentTabBlock = (
      <Tabs defaultActiveKey="1">
        <TabPane tab='Meta String' key="1">
          {metaStringConfigurator}
        </TabPane>
      </Tabs>
    );

    const tabBlock = (formType !== 'customComponent' && fieldMetaMap[formType]) ? regularTabBlock : customComponentTabBlock;
    return (
      <div style={{paddingBottom: 40}}>
        <ButtonWrapper>
          <RenderButton
            label="Close"
            type="primary"
            onClick={(e) => this.closeFieldConfiguration()}
          />
          <RenderButton
            label="Preview Config"
            type="primary"
            onClick={this.toggleDialog}
          />
        </ButtonWrapper>

        <Modal
          style={{top:30}}
          title="Preview"
          visible={this.state.open}
          onOk={this.toggleDialog}
          onCancel={this.toggleDialog}
          footer={[
            <Button key="close" onClick={this.toggleDialog}>Close</Button>,
          ]}
        >
          <div><pre>{this.renderJSON()}</pre></div>
        </Modal>

        <Title>{type === 'rest' ? type : formType} Field</Title>
        
        {primaryData && primaryData.length ? (
          <FormWrapper>
            {this.renderComponents(primaryData)}
          </FormWrapper>
        ) : null}

        {tabBlock}
        
      </div>


    );
  }
}

FieldConfigurationContainer.propTypes = {
  selectedIndex: PropTypes.number,
  validateAndUpdateField: PropTypes.func,
  updateMeta: PropTypes.func,
  setFormMeta: PropTypes.func,
  updateSelectedIndex: PropTypes.func,
  primaryData: PropTypes.array,
  metaFieldData: PropTypes.array,
  formData: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  updateField: PropTypes.func,
  validateAllFields: PropTypes.func,
  updateRenderData: PropTypes.func,
};

export default FieldConfigurationContainer;
