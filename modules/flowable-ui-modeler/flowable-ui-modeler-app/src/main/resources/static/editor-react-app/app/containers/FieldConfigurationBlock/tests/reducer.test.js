import { fromJS } from 'immutable';
import fieldConfigurationBlockReducer from '../reducer';

describe('fieldConfigurationBlockReducer', () => {
  it('returns the initial state', () => {
    expect(fieldConfigurationBlockReducer(undefined, {})).toEqual(fromJS({}));
  });
});
