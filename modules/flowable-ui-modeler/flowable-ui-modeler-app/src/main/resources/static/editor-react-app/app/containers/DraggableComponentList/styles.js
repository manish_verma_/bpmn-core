import styled from 'styled-components';

export const SectionHeading = styled.h5`
  font-size: 20px;
  line-height: 21px;
  padding-top: 0px;
  color: #fba515;
  margin-left: -5px;
`;
