/*
 *
 * DraggableComponentList actions
 *
 */

import * as C from './constants';

export function defaultAction() {
  return {
    type: C.DEFAULT_ACTION,
  };
}

export function setComponentList(data) {
  return {
    type: C.SET_COMPONENT_LIST,
    data
  };
}
