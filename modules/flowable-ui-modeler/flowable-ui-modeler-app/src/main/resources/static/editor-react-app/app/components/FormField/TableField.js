import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import dottie from 'dottie';
import { Table } from 'antd';
import { IsJsonString } from 'utilities/commonUtils';
import { getEditorComponentMapByFormType } from 'utils/componentMap';
import Heading from './HeadingField';

const ColumnPlaceholder = () => 'Add Column Component';

const CellComponent = props => {
  const field = props.fieldData ? props.fieldData : {};
  const meta = dottie.get(field, 'params.meta', {});
  const UnitComponent = props.fieldData
    ? getEditorComponentMapByFormType(meta.form_type)
    : ColumnPlaceholder;
  const value = props.data[props.dataIndex]
    ? props.data[props.dataIndex]
    : '';
  return (
    <td>
      <UnitComponent
        value={value}
        // label={field.name}
        style={{ width: '100%' }}
      />
    </td>
  );
};

const TableField = props => {
  const {
    label,
    formFieldObject,
    field: {
      params: {
        meta: { fields, default: defaultValue },
      },
    },
  } = props;
  const tableColumns =
    fields && fields.length
      ? fields.map((column, index) => ({
        title: formFieldObject[column.fieldId]
          ? formFieldObject[column.fieldId].name
            : column.fieldId,
        dataIndex: column.fieldId,
        key: column.fieldId,
        onCell: data => ({
          data,
          title: formFieldObject[column.fieldId]
              ? formFieldObject[column.fieldId].name
              : column.fieldId,
          dataIndex: column.fieldId,
          key: column.fieldId,
          fieldData: formFieldObject[column.fieldId],
        }),
      }))
      : [];
  const dummyDataObj = {};
  if (fields && fields.length)
    fields.forEach(column => {
      dummyDataObj[column.fieldId] = '';
    });
  const dataSource = (defaultValue &&
    IsJsonString(defaultValue) &&
    JSON.parse(defaultValue)) || [dummyDataObj];
  return (
    <Fragment>
      <Heading label={label} />
      <Table
        components={{
          body: {
            cell: CellComponent,
          },
        }}
        bordered
        pagination={false}
        locale={{ emptyText: 'Empty Table' }}
        dataSource={Array.isArray(dataSource) ? dataSource : []}
        columns={tableColumns}
      />
    </Fragment>
  );
};

TableField.propTypes = {
  label: PropTypes.string,
  field: PropTypes.object,
  formFieldObject: PropTypes.object,
  // field: PropTypes.shape({
  //   params: PropTypes.shape({
  //     meta: PropTypes.shape({
  //       columns: PropTypes.array,
  //     }),
  //   }),
  // }),
};

export default TableField;
