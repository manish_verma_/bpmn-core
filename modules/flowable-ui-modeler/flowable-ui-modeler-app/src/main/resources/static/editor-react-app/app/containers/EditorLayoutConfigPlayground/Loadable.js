/**
 *
 * Asynchronously loads the component for EditorLayoutConfigPlayground
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
