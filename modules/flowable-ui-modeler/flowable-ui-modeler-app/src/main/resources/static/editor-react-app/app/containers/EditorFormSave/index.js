/**
 *
 * EditorFormSave
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectEditorFormSave from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { hasWhiteSpace, definedValueCheck } from '../../utilities/commonUtils';
import * as notifUtils from '../../utilities/notificationUtils';
import * as appActions from '../AppWrapper/actions';
import {
  makeSelectFormData,
  makeSelectSelectedIndex,
  makeSelectIsMetaValid,
} from '../FieldConfigurationBlock/selectors';
import { validateAllFields } from '../FieldConfigurationBlock/actions';
import {
  makeSelectCurrentForm,
  makeSelectGlobalVariables,
} from '../AppWrapper/selectors';

import * as editorUtils from '../../utilities/editorUtils';
import {
  Modal,
  Button,
  Input,
  Checkbox,
  notification,
  Tooltip,
  Badge,
} from 'antd';
// import moment from 'moment';
import Label from '../../components/Label';
import RenderError from '../../components/RenderError';

import AddGlobalVariableForm from '../GlobalVariables/AddGlobalVariables';
import { setGlobalVariablesList } from '../../utilities/constants';

import { buildRenderingJSON, getUnaccountedFormIds } from './helper';

import styled from 'styled-components';

const FieldWrapper = styled.div`
  margin-bottom: 10px;
`;

const emptyErrorMessage = 'Please fill this property.';
const noSpaceErrorMessage = 'Please remove any spaces from the form key.';

const successNotifObj = {
  type: 'success',
  messages: {
    title: 'Success',
    description: 'The form has been saved successfully.',
  },
};

const formSavingError = 'There is some error in saving the form. Please check the data before submitting';

// const errorNotifObj = {
//   type: 'error',
//   messages: {
//     title: 'Error!',
//     description:
//       'There is some error in saving the form. Please check the data before submitting',
//   },
// };



const getFormInvalidObj = invalidIds => {
  // let invalidIdsString = invalidIds.join(', <br/>' )
  let invalidIdsString = (
    <div>
      <span>
        Please fill the mandatory properties of <br />{' '}
      </span>
      {invalidIds.map(id => (
        <strong>
          {' '}
          {id} <br />
        </strong>
      ))}
    </div>
  );
  return {
    type: 'error',
    messages: {
      title: 'Error!',
      description: invalidIdsString,
    },
  };
};

/* eslint-disable react/prefer-stateless-function */
export class EditorFormSave extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentForm: this.props.currentForm,
      formName: this.props.currentForm.name,
      formKey: this.props.currentForm.key,
      openModal: false,
      openPreviewModal: false,
      newVersion: false,
      reusable: false,
      description: '',
      comment: '',
      formKeyError: '',
      formNameError: '',
      unAccountedData: [],
    };
  }

  componentDidMount() {
    // set the variable in the constants file
    this.getVariables();
  }

  componentWillReceiveProps = newProps => {
    // Autosave Auto Save form whenever the selected index is changed
    // ** To avoid the lost of changes if something goes wrong
    if (newProps.selectedIndex !== this.props.selectedIndex) {
      this.saveForm();
    }
  };

  getVariables = () => {
    const self = this;
    // call REST api to fetch results
    let url = FLOWABLE.APP_URL.getGlobalVariableUrl();
    // Build the request data to get saved variables
    const reqData = {
      url: url,
      params: {},
      successCb: response => {
        if (response && Array.isArray(response)) {
          setGlobalVariablesList(response);
        }
        // set in app's Global Variables
        self.props.setGlobalVariables(response);
      },
      errorCb: error => {
        // Show the notification for error
        //notifUtils.openNotificationWithIcon(errorNotifObj);
      },
    };
    this.props.getRequest({ data: reqData });
  };

  isFormValid = () => {
    let isFormValid = true,
      invalidIds = [];
    this.props.formFields.forEach(field => {
      if (!field.id || !field.name) {
        invalidIds.push(field.name || field.id || 'New ' + field.form_type);
        isFormValid = false;
        return;
      }
    });

    return { isFormValid, invalidIds };
  };

  openSaveModal = () => {
    // Check if the form is valid and all the fields have the Id and Name present
    // then Open the dialog
    let { isFormValid, invalidIds } = this.isFormValid();
    if (isFormValid) {
      this.setState({ openModal: true });
    } else {
      notifUtils.openNotificationWithIcon(getFormInvalidObj(invalidIds));
    }
  };

  showMetaError = () => {
    message.error(
      'Please fill all the properties for the selected field properly',
    );
  };

  closeModal = () => {
    this.setState({ openModal: false });
  };

  updateFormVersion = e => {
    this.setState({ newVersion: e.target.checked });
    if (!e.target.checked) {
      this.setState({ comment: '' });
    }
  };

  updateDescription = e => {
    this.setState({ description: e.target.value });
  };

  updateComment = e => {
    this.setState({ comment: e.target.value });
  };

  updateFormKey = e => {
    let value = e.target.value.trim();
    this.setState({ formKey: value });
    if (!value) {
      this.setState({ formKeyError: emptyErrorMessage });
    } else if (hasWhiteSpace(value)) {
      this.setState({ formKeyError: noSpaceErrorMessage });
    } else {
      this.setState({ formKeyError: '' });
    }
  };

  updateFormName = e => {
    let value = e.target.value;
    this.setState({ formName: value });
    if (!value) {
      this.setState({ formNameError: emptyErrorMessage });
    } else {
      this.setState({ formNameError: '' });
    }
  };

  // Removes all properties starting with '_', used before json serialization
  removePrivateFields = fields => {
    fields.forEach((field, i) => {
      for (let attr in field) {
        if (attr.indexOf('_') === 0) {
          delete field[attr];
        }
      }
      if (field.fields) {
        for (let fieldsName in field.fields) {
          this.removePrivateFields(field.fields[fieldsName]);
        }
      }
    });
    return fields;
  };

  buildFormPayload = () => {
    let {
      description,
      newVersion,
      reusable,
      comment,
      formName,
      formKey,
    } = this.state;
    let currentForm = { ...this.props.currentForm };

    let fields = this.removePrivateFields([...this.props.formFields]);
    let formRepresentation = {
      ...currentForm,
      name: formName,
      key: formKey,
      description: description,
      formDefinition: {
        ...currentForm.formDefinition,
        name: formName,
        key: formKey,
        fields: fields,
      },
    };

    let formObj = {
      newVersion,
      reusable,
      comment,
      formRepresentation: formRepresentation,
    };

    // If form is being saved from the autoSave functionality (everytime the selectedIndex is changed)
    // then newVersion should always be false
    if (!this.state.openModal) {
      formObj.newVersion = false;
    }

    return formObj;
  };

  previewForm = e => {
    const { isFormValid, invalidIds } = { ...this.isFormValid() };
    if (isFormValid) {
      this.setState({ openPreviewModal: true });
      const formPreviewRenderingJSON = buildRenderingJSON([
        ...this.props.formFields,
      ]);
      import ('../../rendering-engine').then((REngine) => {
          window.renderJourneyIntoComponent ? 
              window.renderJourneyIntoComponent('preview-form-wrapper', {
                userId: 'dummy-userId',
                pid: 'dummy-pid',
                token: 'dummy-token',
                formRenderingJson: {...formPreviewRenderingJSON}

              })
            : '';
        });
    } else {
      notifUtils.openNotificationWithIcon(getFormInvalidObj(invalidIds));
    }
  };

  closePreviewModal = () => {
    window.cancelJourneyInContainer
      ? window.cancelJourneyInContainer('preview-form-wrapper')
      : '';
    this.setState({ openPreviewModal: false });
  };

  saveForm = (e, closeEditor) => {
    e && e.preventDefault ? e.preventDefault() : '';

    const self = this;
    // Run validation for formKey and formName (can't be empty)
    if (this.state.formKeyError || this.state.formNameError) {
      return;
    }

    // Structure the current form to submit through an API
    // insert formRepresentation from store's formItems
    let formToSubmit = this.buildFormPayload();
    // Attach a screenshot for form's thumbnail (formImageBase64)
    editorUtils.generateFormImage('canvas-form-wrapper').then(formImage => {
      formToSubmit.formImageBase64 = formImage; // This is needed in terms of keeping the API working for legacy flowable
      let url = FLOWABLE.APP_URL.getFormModelUrl(this.props.currentForm.id);

      // Build the request data to save form request
      const reqData = {
        url: url,
        params: formToSubmit,
        successCb: response => {
          // Setup the render Data in the app
          // Call the render saga service
          if (response) {
            if (this.state.openModal) {
              // Show the notification for success
              notifUtils.openNotificationWithIcon(successNotifObj);
            }
            // update the form state
            self.props.setCurrentForm({ ...formToSubmit.formRepresentation });
            // close the dialog
            self.closeModal();
            if (closeEditor) {
              self.triggerCloseEditor();
            }
          }
        },
        errorCb: error => {
          console.debug('error in saving form', error);
          // Show the notification for error
          const errorNotifObj = {
            type: 'error',
            messages: {
              title: 'Error!',
              description:
                error.message || formSavingError,
            },
          }
          notifUtils.openNotificationWithIcon(errorNotifObj);
        },
      };

      // Issue form save request
      this.props.putRequest({ data: reqData });
    });
  };

  getCommentComp = () => {
    return (
      <FieldWrapper>
        <Label label="Comment" />
        <Input.TextArea
          value={this.state.comment}
          type="textarea"
          value={this.state.comment}
          onChange={this.updateComment}
        />
      </FieldWrapper>
    );
  };

  triggerCloseEditor = () => {
    this.props.closeEditor();
  };

  toggleOldView = () => {
    this.props.toggleOldView();
  };

  // Below method is for the <AddGlobalVariableForm> component
  // Once the new variables are added they are added to the core table data of this page
  updateParentData = newAddedVariables => {
    let variableData = [...this.props.globalVariables];
    // Push all the newly added Variables into the tableData
    Array.prototype.push.apply(variableData, [...newAddedVariables]);
    setGlobalVariablesList(variableData);
    this.props.setGlobalVariables(variableData);
  };

  render() {
    const currentForm = { ...this.props.currentForm };

    let renderComment = this.state.newVersion ? this.getCommentComp() : null;
    const updateDate = new Date(currentForm.lastUpdated);
    // const updateString = moment(updateDate).format('Do MMMM YY, h:mm a');

    return (
      <div style={{ marginTop: 10 }}>
        <div style={{ float: 'left' }}>
          <h4 style={{ marginBottom: 0 }}>
            {' '}
            {currentForm.name}{' '}
            <span style={{ fontSize: 12 }}>
              (v
              {currentForm.version})
            </span>
          </h4>
          <div>
            <div style={{ fontSize: 10 }}>
              {' '}
              Last updated by {currentForm.lastUpdatedBy}{' '}
            </div>
          </div>
        </div>
        <div style={{ float: 'right' }}>
          <Tooltip placement="topLeft" title="Preview form">
            <Button
              type="primary"
              icon="layout"
              style={{ marginRight: '10px' }}
              onClick={this.previewForm}
            />
          </Tooltip>
          <Tooltip placement="topLeft" title="Save form">
            <Button
              type="primary"
              icon="save"
              onClick={this.openSaveModal}
              style={{ marginRight: '10px' }}
            />
          </Tooltip>
          <AddGlobalVariableForm
            postRequest={this.props.postRequest}
            updateParentData={this.updateParentData}
            formFields={this.props.formFields}
            currentGlobalVariables={[...this.props.globalVariables]}
          />

          <Button
            type="primary"
            onClick={e => this.toggleOldView()}
            style={{ marginRight: '10px', marginLeft: '10px' }}
          >
            Switch to Old View
          </Button>
          <Tooltip placement="topLeft" title="Close form">
            <Button
              type=""
              icon="close-circle"
              onClick={e => this.triggerCloseEditor()}
              style={{ marginRight: '10px' }}
            />
          </Tooltip>

          <Modal
            style={{ top: 30 }}
            title="Save Form"
            visible={this.state.openModal}
            onOk={this.saveForm}
            onCancel={this.closeModal}
            footer={[
              <Button key="back" onClick={this.closeModal}>
                Close
              </Button>,
              <Button
                key="submit"
                type="primary"
                onClick={e => this.saveForm(e)}
              >
                Save Form
              </Button>,
              <Button
                key="submitAndClose"
                type="primary"
                onClick={e => this.saveForm(e, true)}
              >
                Save Form & Close Editor
              </Button>,
            ]}
          >
            <FieldWrapper>
              <Label label="Form Key" />
              <Input
                value={this.state.formKey}
                label="Form Key"
                value={this.state.formKey}
                onChange={this.updateFormKey}
              />
              <RenderError errorMessage={this.state.formKeyError} />
            </FieldWrapper>
            <FieldWrapper>
              <Label label="Form Name" />
              <Input
                value={this.state.formName}
                label="Form Name"
                value={this.state.formName}
                onChange={this.updateFormName}
              />
              <RenderError errorMessage={this.state.formNameError} />
            </FieldWrapper>
            <FieldWrapper>
              <Label label="Description" />
              <Input.TextArea
                value={this.state.description}
                label="Description"
                type="textarea"
                value={this.state.description}
                onChange={this.updateDescription}
              />
            </FieldWrapper>
            <FieldWrapper>
              <Checkbox
                checked={this.state.newVersion}
                onChange={this.updateFormVersion}
              >
                Save this as a new version? This means you can always go back to
                a previous version
              </Checkbox>
            </FieldWrapper>

            {renderComment}
          </Modal>

          <Modal
            width={'99%'}
            style={{ top: 5 }}
            title="Preview Form"
            visible={this.state.openPreviewModal}
            onOk={this.closePreviewModal}
            onCancel={this.closePreviewModal}
          >
            <div id="preview-form-wrapper" />
          </Modal>
        </div>
        <div style={{ clear: 'both' }} />
      </div>
    );
  }
}

EditorFormSave.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  editorFormSave: makeSelectEditorFormSave(),
  formFields: makeSelectFormData(),
  selectedIndex: makeSelectSelectedIndex(),
  currentForm: makeSelectCurrentForm(),
  globalVariables: makeSelectGlobalVariables(),
  isMetaValid: makeSelectIsMetaValid(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: reqObject => dispatch(appActions.getRequest(reqObject)),
    postRequest: reqObject => dispatch(appActions.postRequest(reqObject)),
    putRequest: reqObject => dispatch(appActions.putRequest(reqObject)),
    setCurrentForm: data => dispatch(appActions.setCurrentForm(data)),
    setGlobalVariables: data => dispatch(appActions.setGlobalVariables(data)),
    validateAllFields: data => dispatch(validateAllFields(data)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'editorFormSave', reducer });
const withSaga = injectSaga({ key: 'editorFormSave', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(EditorFormSave);
