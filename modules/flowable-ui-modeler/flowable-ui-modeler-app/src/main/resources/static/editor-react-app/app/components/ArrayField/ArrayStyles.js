import { inputComponentStyle } from 'configs/commonStyles';
import styled from 'styled-components';
export const inputStyles = {
  ...inputComponentStyle,
};

export const FieldWrapper = styled.div`
  position: relative;
  span.tooltip-wrapper {
    position: absolute;
    right: 8px;
    top: 8px;
  }
`;

export const ButtonWrapper = styled.div`
  text-align: right;
  margin: 0px 15px 5px 0px;
`;

export const RowWrapper = styled.div`
  display: flex;
  position: relative;
  margin-bottom: 10px;

  .rowCell {
    flex: 1;
  }
  .rowCell.icon-row {
    flex: 0;
    padding-top: 3px;
    position: absolute;
    right: -8px;
    opacity: 0;
  }
  &:hover {
    & > .rowCell.icon-row {
      opacity: 1;
    }
  }
`;

export const DataWrapper = styled.div`
  background-color: #f9f9f9;
  padding: 10px 13px;
`;

export const Heading = styled.h5`
  font-size: 14px;
  padding: 10px 0px;
`;

export const iconStyles = {
  verticalAlign: 'middle',
};

export const LinkText = styled.span`
  vertical-align: middle;
  display: inline-block;
  margin-left: 5px;
`;
