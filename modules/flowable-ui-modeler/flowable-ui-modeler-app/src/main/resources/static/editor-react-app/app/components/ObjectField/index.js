/**
 *
 ObjectField
 *
 */

import React from 'react';
import Proptypes from 'prop-types';
import { componentMapByFormType } from 'utils/componentMap';
import componentProps from 'utils/componentProps';
import dottie from 'dottie';
import { typeCheck } from 'utilities/commonUtils';
import { validateElm } from 'utilities/validations/validationFns';
import { Icon } from 'antd';
import InfoText from '../InfoText';
import RenderError from '../RenderError';
import RenderButton from '../RenderButton';
import A from '../A';
import {
  ButtonWrapper,
  FieldRow,
  CellRow,
  HeadingWrapper,
  HeadingText,
  DataWrapper,
  iconStyles,
  LinkText,
} from './ObjectStyles';
export default class ObjectInputField extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      rowData: [],
      rowDataError: [],
      rowConfig: [],
    };
    this.rowDataChangeHandler = this.rowDataChangeHandler.bind(this);
  }

  componentDidMount() {
    const { value } = this.props.compProps;
    this.convertValueToData(value);
  }

  componentWillReceiveProps(newProps) {
    const { value } = newProps.compProps;
    if (newProps.toggleTab !== this.props.toggleTab) {
      this.convertValueToData(value);
    }
  }

  parseUserDefinedValue = obj => {
    const arr = [];
    Object.keys(obj).forEach((key, index) => {
      arr.push({ key, value: obj[key] });
    });
    return arr;
  };

  parseBack = arr => {
    const obj = {};
    arr.forEach(entry => {
      obj[entry.key] = entry.value;
    });
    return obj;
  };

  convertValueToData = (val) => {
    const self = this;
    // value is recieved in a json string
    const value = val;
    const { objectType } = this.props.renderData;
    let arrayData;
    if (value) {
      arrayData =
        objectType === 'fixedKeys'
          ? [value]
          : this.parseUserDefinedValue(value);
    } else {
      arrayData = [];
    }
    const rowDataError = arrayData.map(data =>
      Array(Object.keys(data).length).fill({ key: '', value: '' }),
    );
    this.setState({ rowData: arrayData, rowDataError }, () => {
      // this.initialiseErrorData();
      if (!self.state.rowData.length) {
        self.addRow();
      }
    });
  };

  validateRowEl = (value, fieldId, index) => {
    const {
      renderData: { dataObj },
    } = this.props;
    const repeater = dataObj.find(data => data.id === fieldId);
    // const arrayData = [...this.state.rowData];
    const { errorMessage } = validateElm(repeater, value);
    this.setState(({ rowDataError }) => {
      const newRowDataError = [...rowDataError];
      newRowDataError[index][fieldId] = errorMessage;
      return {
        rowDataError: newRowDataError,
      };
    });
  };

  validateAllFields = (value, key, type) => {
    // for array object and other nested object need to get the child validation status
    const isChildInValid = type === 'validationStatus' ? value : false;
    const { rowData } = this.state;
    const { updateRenderData, renderData, metaId, onBlurHandler } = this.props;
    const { dataObj } = renderData;
    let isInvalid = false;
    // rowValue =>
    // { key: 'This field is required', label: 'This field is required'}
    // { url: 'This field is required', method: 'This field is required'}
    const rowDataError = rowData.map(rowValue => {
      const errorMap = {};
      Object.keys(rowValue).forEach(key => {
        const repeater = dataObj.find(data => data.id === key);
        const { errorMessage, isValid } = validateElm(repeater, rowValue[key]);
        errorMap[key] = isValid ? '' : errorMessage;
        isInvalid = !isInvalid && !isValid ? true : isInvalid;
      });
      return errorMap;
    });
    if (typeCheck(updateRenderData, 'function')) {
      const newRenderData = {
        ...renderData,
        isChildInValid: isChildInValid || isInvalid,
      };
      updateRenderData(newRenderData, metaId);
    } else if (typeCheck(onBlurHandler, 'function')) {
      onBlurHandler(isChildInValid || isInvalid, metaId, 'validationStatus');
    }
    this.setState({ rowDataError });
  };

  rowDataChangeHandler = (value, fieldId, index) => {
    const arrayData = [...this.state.rowData];
    const { onChangeHandler, metaId, renderData } = this.props;

    if (fieldId === 'delete') {
      this.deleteRow(index);
    } else {
      arrayData[index][fieldId] = value;
      this.setState({ rowData: arrayData }, () => {
        this.validateRowEl(value, fieldId, index);
      });
    }
    if (renderData.objectType === 'userDefined') {
      onChangeHandler && onChangeHandler(this.parseBack(arrayData), metaId);
    } else {
      onChangeHandler && onChangeHandler(arrayData[0], metaId);
    }
  };

  getErrorMessage = () => this.props.renderData.errorMessage || '';

  getEmptyRowData = () => {
    const data = {};
    const { dataObj } = this.props.renderData;
    dataObj.forEach(conf => {
      if (conf.id === 'delete') return;
      data[conf.id] = '';
    });
    return data;
  };

  addRow = () => {
    const arrayData = [...this.state.rowData];
    const arrayDataError = [...this.state.rowDataError];
    arrayData.push(this.getEmptyRowData());
    arrayDataError.push(this.getEmptyRowData());
    this.setState(
      {
        rowData: arrayData,
        rowDataError: arrayDataError,
      }
      // this.validateAllFields,
    );
  };

  deleteRow = deleteIndex => {
    this.setState(
      ({ rowData, rowDataError }) => ({
        rowData: rowData.filter((element, index) => deleteIndex !== index),
        rowDataError: rowDataError.filter(
          (element, index) => deleteIndex !== index,
        ),
      }),
      () => {
        this.updateFormData();
        this.validateAllFields();
      },
    );
  };

  updateFormData = () => {
    const value = this.convertTableRowsIntoValue();
    this.props.onChangeHandler(value, this.props.metaId);
  };

  convertTableRowsIntoValue = () => this.state.rowData;

  renderArrayRows = () => {
    const arrayRowData = this.state.rowData;
    const { renderData } = { ...this.props };
    return arrayRowData.map((rowData, index) => (
      <FieldRow
        key={`arrayRow-${index}`}
        type={renderData.objectType}
        viewType={renderData.viewType}
      >
        {this.renderSingleRow(rowData, index)}
      </FieldRow>
    ));
  };

  renderSingleRow = (data, index) => {
    const self = this;
    const { rowDataError, rowData } = this.state;
    const { renderData, selectedFieldData, level } = { ...this.props };
    const renderCells = [];

    let { dataObj } = { ...renderData };
    if (renderData.objectType === 'userDefined') {
      dataObj = [
        ...dataObj,
        {
          label: 'Delete',
          type: 'icon',
          id: 'delete',
          description: 'Delete this row',
        },
      ];
    }

    dataObj.forEach((data, idx) => {
      const meta = data;

      const UnitComponent = componentMapByFormType[meta.type];
      const defaultValue = rowData[index][meta.id] || '';
      const getComponentProps = dottie.get(componentProps, meta.type) || {};
      const errorMessage = rowDataError[index][meta.id] || '';
      const unitComp = (
        <div>
          <UnitComponent
            compProps={getComponentProps(meta, meta.id, defaultValue)}
            toggleTab={this.props.toggleTab}
            metaId={meta.id}
            key={`abc${index}-${idx}`}
            renderData={meta}
            level={level + 1}
            updateFormData={this.props.updateFormData}
            onChangeHandler={(val, id) =>
              this.rowDataChangeHandler(val, id, index)
            }
            onBlurHandler={this.validateAllFields}
            errorMessage={errorMessage}
          />
        </div>
      );

      renderCells.push(
        <CellRow
          key={`cell-${index}-${idx}`}
          className={`rowCell ${meta.type}-row`}
          colWidth={meta.colWidth}
          type={meta.type !== 'icon' ? renderData.objectType : 'icon'}
          viewType={renderData.viewType}
          columnCount={dataObj.length}
        >
          {unitComp}
        </CellRow>,
      );
    });
    return renderCells;
  };

  render() {
    const { renderData, className, level } = this.props;
    const arrayRows = this.renderArrayRows();
    const errorMessage = this.getErrorMessage();
    let error = null;
    if (errorMessage) {
      error = <RenderError errorMessage={errorMessage} />;
    }
    return (
      <div className={className}>
        {renderData.label ? (
          <HeadingWrapper>
            <HeadingText>{renderData.label}</HeadingText>
            <InfoText helperText={renderData.description} />
          </HeadingWrapper>
        ) : null}
        <DataWrapper level={level}>{arrayRows}</DataWrapper>
        {error}
        {renderData.objectType === 'userDefined' ? (
          <ButtonWrapper>
            {/* <RenderButton
              label="Add"
              type="primary"
              onClick={this.addRow}
              size="small"
              // icon="plus-circle"
            /> */}
            <A onClick={this.addRow}>
              <Icon type="plus-circle" style={iconStyles} />
              <LinkText>Add</LinkText>
            </A>
          </ButtonWrapper>
        ) : null}
      </div>
    );
  }
}

ObjectInputField.propTypes = {
  updateRenderData: Proptypes.func,
  metaId: Proptypes.string,
  onBlurHandler: Proptypes.func,
  onChangeHandler: Proptypes.func,
  className: Proptypes.string,
  level: Proptypes.number,
};

ObjectInputField.defaultProps = {
  level: 0,
};
