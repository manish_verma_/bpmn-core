/**
 *
 * Masters
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import {
  Tooltip,
  Icon,
  Select,
  Button,
  Popconfirm,
  message,
  Input,
} from 'antd';

import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import * as appActions from '../AppWrapper/actions';
import EditMaster from './EditMaster';
import ImportMasters from './ImportMasters';
import RestoreMaster from './RestoreMaster';
import { AntTable, AntModal, Wrapper, Header } from './styles';
import CodeEditor from '../../components/CodeEditor';

const { Option } = Select;

export class Masters extends React.PureComponent {
  static propTypes = {
    getRequest: PropTypes.func,
  };

  static defaultProps = {};

  state = {
    mastersData: [],
    company: [],
    companyName: '',
    visible: false,
    visibleImport: false,
    visibleRestore: false,
    loading: false,
  };

  componentDidMount() {
    this.fetchCompanyList();
  }

  fetchCompanyList = () => {
    const url = MASTER.APP_URL.getCompanyList();
    // Build the request data to get company list
    const reqData = {
      url,
      params: {},
      successCb: response => {
        if (response.data && response.data.companyList) {
          const { companyList } = response.data;
          this.setState(
            () => ({
              company: companyList,
              companyName: companyList[0],
            }),
            () => this.fetchMastersData(),
          );
        }
      },
      errorCb: () => {
        message.error('error occur in fetching company list');
      },
    };
    this.props.getRequest({ data: reqData });
  };

  fetchMastersData = () => {
    // call REST api to fetch results
    const query = `companyName=${this.state.companyName}`;
    const url = MASTER.APP_URL.getMastersListUrl(query);
    // Build the request data to get masters data
    const reqData = {
      url,
      params: {},
      successCb: response => {
        if (response.data && Array.isArray(response.data)) {
          const data = response.data.map((item, index) => {
            const newIndex = index + 1;
            return {
              ...item,
              index: newIndex,
            };
          });
          // set the table data
          this.setState(() => ({ mastersData: [...data] }));
        }
      },
      errorCb: () => {
        message.error('error occur in fetching master data');
      },
    };
    this.props.getRequest({ data: reqData });
  };

  editMaster = (record, isNew) => {
    this.setState(() => ({ visible: true, record, isNewMaster: isNew }));
  };

  deleteMaster = record => {
    // call REST api
    const query = `${record.masterSlug}`;
    const url = MASTER.APP_URL.deleteMaster(query);
    // Build the request data to delete masters
    const reqData = {
      url,
      params: {},
      successCb: () => {
        message.success('Delete master successfully');
        this.fetchMastersData();
      },
      errorCb: () => {
        // Show the notification for error
        message.error('Master Not Deleted');
      },
    };
    this.props.deleteRequest({ data: reqData });
  };

  // application/json text/plain;charset=utf-8
  download = (filename, text) => {
    const element = document.createElement('a');
    element.setAttribute(
      'href',
      `data:application/json;charset=utf-8,${encodeURIComponent(text)}`,
    );
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  };

  exportMaster = record => {
    // call REST api
    const { companyName } = this.state;
    const query = record
      ? `slug=${record.masterSlug}`
      : `companyName=${companyName}`;
    const url = MASTER.APP_URL.exportMaster(query);
    // Build the request data to export masters
    const reqData = {
      url,
      params: {},
      successCb: response => {
        if (record) {
          this.setState(() => ({ masterSlug: record.masterSlug }));
        } else {
          this.toggleLoading();
        }
        this.setState(() => ({
          visibleExport: true,
          exportMasterData: JSON.stringify(response, null, 2),
        }));
      },
      errorCb: () => {
        // Show the notification for errort
        message.error('Error occur in export master');
      },
    };
    this.props.getRequest({ data: reqData });
  };

  columns = [
    {
      title: 'Index',
      dataIndex: 'index',
      width: '5%',
    },
    {
      title: 'Master Slug',
      dataIndex: 'masterSlug',
      width: '15%',
    },
    {
      title: 'Master Name',
      dataIndex: 'masterName',
      width: '15%',
    },
    {
      title: 'URL',
      dataIndex: 'masterURL',
      width: '55%',
      render: (text, record) => (
        <a href={text} target="_blank" style={{ textDecoration: 'none' }}>
          {text}
        </a>
      ),
    },

    {
      title: 'Actions',
      dataIndex: '_source.content',
      width: '10%',
      render: (text, record) => (
        <div>
          <Tooltip title="Edit Master">
            <Icon
              type="edit"
              style={{ color: 'green' }}
              onClick={() => this.editMaster(record, false)}
            />
          </Tooltip>
          <Popconfirm
            title="Sure to delete?"
            onConfirm={() => this.deleteMaster(record)}
          >
            <Icon type="delete" style={{ color: 'red', marginLeft: 15 }} />
          </Popconfirm>
          <Tooltip title="Export Master">
            <Icon
              type="export"
              style={{ color: 'blue', marginLeft: 15 }}
              onClick={() => this.exportMaster(record)}
            />
          </Tooltip>
        </div>
      ),
    },
  ];

  onSearch = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  renderCompany = companyName => (
    <Option key={companyName} value={companyName}>
      {companyName}
    </Option>
  );

  onChangeCompany = companyName => {
    this.setState(() => ({ companyName }), () => this.fetchMastersData());
  };

  toggleLoading = () => {
    this.setState(prevState => ({ loading: !prevState.loading }));
  };

  toggleEditModal = () => {
    this.setState(prevState => ({
      visible: !prevState.visible,
    }));
  };

  showImportModal = () => {
    this.setState(() => ({ visibleImport: true }));
  };

  toggleImportModal = () => {
    this.setState(prevState => ({
      visibleImport: !prevState.visibleImport,
    }));
  };

  toggleExportModal = () => {
    this.setState(prevState => ({
      visibleExport: !prevState.visibleExport,
    }));
  };

  toggleRestoreModal = () => {
    this.setState(prevState => ({
      visibleRestore: !prevState.visibleRestore,
    }));
  };

  render() {
    let { mastersData } = this.state;
    const { masterSlug, companyName, exportMasterData, searchStr } = this.state;
    const fileName = masterSlug || companyName;
    const modalHeader = this.state.isNewMaster ? 'Add Master' : 'Edit Master';
    if (searchStr) {
      mastersData = mastersData.filter(Data => {
        const index = Data.masterName
          .toLowerCase()
          .indexOf(searchStr.toLowerCase());
        return !(index < 0);
      });
    }

    return (
      <Wrapper>
        <Header>
          <Select
            value={this.state.companyName}
            name="companyName"
            onChange={this.onChangeCompany}
            style={{ width: 250 }}
          >
            {!!this.state.company.length &&
              this.state.company.map(this.renderCompany)}
          </Select>
          <div>
            <Button type="primary" onClick={() => this.showImportModal()}>
              <Icon type="import" /> Import
            </Button>
            <Button
              type="primary"
              icon="export"
              onClick={() => {
                this.toggleLoading();
                this.exportMaster();
              }}
              loading={this.state.loading}
              style={{ marginLeft: 10 }}
            >
              Export All
            </Button>
            <Button
              type="primary"
              onClick={() => this.toggleRestoreModal()}
              style={{ marginLeft: 10 }}
            >
              <Icon type="undo" /> Restore
            </Button>
            <Button
              type="primary"
              onClick={() => this.editMaster({}, true)}
              style={{ marginLeft: 10 }}
            >
              <Icon type="plus" /> Add New Master
            </Button>
          </div>
        </Header>
        <Input
          name="searchStr"
          onChange={this.onSearch}
          placeholder="search for master"
          allowClear
          style={{ width: 250, margin: '5px 0' }}
        />
        <AntTable
          dataSource={mastersData}
          columns={this.columns}
          size="small"
          rowKey={record => record.index}
          pagination={{
            position: 'bottom',
            pageSize: 20,
            showSizeChanger: true,
            showQuickJumper: true,
            pageSizeOptions: ['10', '20', '50', '100'],
          }}
          style={{ wordBreak: 'break-all' }}
        />
        <AntModal
          title={modalHeader}
          centered
          visible={this.state.visible}
          onCancel={this.toggleEditModal}
          destroyOnClose
          footer={null}
          width="60%"
        >
          <EditMaster
            record={this.state.record}
            {...this.props}
            isNewMaster={this.state.isNewMaster}
            companyName={this.state.companyName}
            toggleEditModal={this.toggleEditModal}
            fetchMastersData={this.fetchMastersData}
          />
        </AntModal>
        <AntModal
          title="Import Masters"
          centered
          destroyOnClose
          visible={this.state.visibleImport}
          onCancel={this.toggleImportModal}
          footer={null}
        >
          <ImportMasters
            {...this.props}
            toggleImportModal={this.toggleImportModal}
            fetchMastersData={this.fetchMastersData}
          />
        </AntModal>
        <AntModal
          title="Export Masters"
          centered
          destroyOnClose
          visible={this.state.visibleExport}
          width={800}
          okText="Download File"
          onCancel={this.toggleExportModal}
          onOk={() => this.download(`${fileName}.json`, exportMasterData)}
        >
          <CodeEditor mode="json" code={this.state.exportMasterData} readOnly />
        </AntModal>
        <AntModal
          title="Restore Masters"
          centered
          destroyOnClose
          visible={this.state.visibleRestore}
          onCancel={this.toggleRestoreModal}
          width={1000}
          footer={null}
        >
          <RestoreMaster
            {...this.props}
            toggleRestoreModal={this.toggleRestoreModal}
            fetchMastersData={this.fetchMastersData}
            companyName={this.state.companyName}
          />
        </AntModal>
      </Wrapper>
    );
  }
}

const mapStateToProps = createStructuredSelector({});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: reqObject => dispatch(appActions.getRequest(reqObject)),
    postRequest: reqObject => dispatch(appActions.postRequest(reqObject)),
    putRequest: reqObject => dispatch(appActions.putRequest(reqObject)),
    deleteRequest: reqObject => dispatch(appActions.deleteRequest(reqObject)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'masters', reducer });

export default compose(
  withReducer,
  withConnect,
)(Masters);
