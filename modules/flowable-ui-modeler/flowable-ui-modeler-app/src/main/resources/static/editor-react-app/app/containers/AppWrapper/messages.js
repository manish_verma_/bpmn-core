/*
 * AppWrapper Messages
 *
 * This contains all the text for the AppWrapper container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.AppWrapper';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the AppWrapper container!',
  },
});
