/**
 *
 * App Container
 *
 * This contains the serving app's root containers and the root building block of an app

 Type of APPS:
  formEditor
  globalVariablesApp
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import AppWrapper from '../AppWrapper';

import GlobalStyle from '../../global-styles';
import './global-styles.css';

import * as RootContainers from './RootContainerService';

export default function App(compProps) {
  let props = { ...compProps.appProps },
    Component = null;

  const buildAppContainer = appType => {
    let container = null;
    switch (appType) {
      case 'formEditor':
        container = RootContainers.buildFormEditorApp(props);
        break;

      case 'globalVariablesApp':
        container = RootContainers.buildGlobalVariablesApp(props);
        break;

      case 'httpTaskBlock':
        container = RootContainers.buildHTTPTaskApp(props);
        break;
    }
    return container;
  };

  const AppContainer = buildAppContainer(compProps.appType);

  return (
    <AppWrapper {...props} className="">
      {AppContainer}
    </AppWrapper>
  );
}
