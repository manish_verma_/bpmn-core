/*
 * ArrayField Messages
 *
 * This contains all the text for the ArrayField component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.ArrayField.header',
    defaultMessage: 'This is the ArrayField component !',
  },
});
