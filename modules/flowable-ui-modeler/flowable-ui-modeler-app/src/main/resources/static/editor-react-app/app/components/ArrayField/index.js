/**
 *
 * ArrayField
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { componentMapByFormType } from 'utils/componentMap';
import componentProps from 'utils/componentProps';
import dottie from 'dottie';
import { typeCheck, generateUUID } from 'utilities/commonUtils';
import { validateElm } from 'utilities/validations/validationFns';
import { SelectField } from 'components/FormField';
import { Icon } from 'antd';
import {
  RowWrapper,
  DataWrapper,
  Heading,
  LinkText,
  iconStyles,
  ButtonWrapper,
} from './ArrayStyles';
import RenderError from '../RenderError';
import A from '../A';

export default class ArrayInputField extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      rowData: [],
      rowDataError: [],
      rowConfig: [],
    };
    this.rowDataChangeHandler = this.rowDataChangeHandler.bind(this);
  }

  componentDidMount() {
    this.buildArrayRenderConfig();
    const { value } = this.props.compProps;
    this.convertValueToData(value);
  }

  componentWillReceiveProps(newProps) {
    const { value } = newProps.compProps;
    if (newProps.toggleTab !== this.props.toggleTab) {
      this.convertValueToData(value);
    }
  }

  buildArrayRenderConfig = () => {
    const { repeater } = this.props.renderData;

    const columns = [
      {
        fieldId: repeater.type,
        viewOnly: false,
      },
      { fieldId: 'delete', viewOnly: false },
    ];
    this.setState({ rowConfig: columns });
  };

  convertValueToData = (val) => {
    const self = this;
    // value is recieved in a json string
    let value = val;
    if (!value || value === '') {
      value = '[]';
    }
    let arrayData;
    if (typeof value === 'string') {
      arrayData = JSON.parse(value);
    } else {
      arrayData = value;
    }

    this.setState({ rowData: arrayData }, () => {
      // this.initialiseErrorData();
      if (!self.state.rowData.length) {
        self.addRow();
      }
    });
  };

  onChangeHandler = e => {
    const { metaId } = this.props;
    this.props.onChangeHandler &&
      this.props.onChangeHandler(e.target.value, metaId);
  };

  validateArrayEl = (value, fieldId, index) => {
    const {
      renderData: { repeater },
    } = this.props;
    // const arrayData = [...this.state.rowData];
    const { errorMessage } = validateElm(repeater, value);
    this.setState(({ rowDataError }) => {
      const newRowDataError = [...rowDataError];
      newRowDataError[index] = errorMessage;
      return {
        rowDataError: newRowDataError,
      };
    });
  };

  validateAllFields = (value, key, type) => {
    // for array object and other nested object need to get the child validation status
    const isChildInValid = type === 'validationStatus' ? value : false;
    const { rowData } = this.state;
    const { updateRenderData, renderData, metaId, onBlurHandler } = this.props;
    const { repeater } = renderData;
    const rowDataError = rowData.map(rowValue => {
      const { errorMessage, isValid } = validateElm(repeater, rowValue);
      return isValid ? '' : errorMessage;
    });
    const isInvalid = rowDataError.some(message => !!message);
    if (typeCheck(updateRenderData, 'function')) {
      const newRenderData = {
        ...renderData,
        isChildInValid: isChildInValid || isInvalid,
      };
      updateRenderData(newRenderData, metaId);
    } else if (typeCheck(onBlurHandler, 'function')) {
      onBlurHandler(isChildInValid || isInvalid, metaId, 'validationStatus');
    }
    this.setState({ rowDataError });
  };

  rowDataChangeHandler = (value, fieldId, index) => {
    const arrayData = [...this.state.rowData];
    const { onChangeHandler, metaId } = this.props;

    if (fieldId === 'delete') {
      this.deleteRow(index);
    } else {
      arrayData[index] = value;
      this.setState({ rowData: arrayData }, () => {
        this.validateArrayEl(value, fieldId, index);
      });
    }
    onChangeHandler && onChangeHandler(arrayData, metaId);
  };

  getErrorMessage = () => this.props.renderData.errorMessage || '';

  getEmptyRowData = () => {
    const data = '';
    this.state.rowConfig.forEach(conf => {
      if (conf.fieldId === 'delete') return;
    });
    return data;
  };

  addRow = () => {
    const arrayData = [...this.state.rowData];
    const arrayDataError = [...this.state.rowDataError];
    arrayData.push(this.getEmptyRowData());
    arrayDataError.push(this.getEmptyRowData());
    this.setState(
      {
        rowData: arrayData,
        rowDataError: arrayDataError,
      },
      this.validateAllFields,
    );
  };

  deleteRow = deleteIndex => {
    const arrayData = [...this.state.rowData].filter(
      (element, index) => deleteIndex !== index,
    );
    const arrayDataError = [...this.state.rowDataError].filter(
      (element, index) => deleteIndex !== index,
    );
    this.setState(
      {
        rowData: arrayData,
        rowDataError: arrayDataError,
      },
      () => {
        this.updateFormData();
        this.validateAllFields();
      },
    );
  };

  updateFormData = () => {
    const value = this.convertTableRowsIntoValue();
    this.props.onChangeHandler(value, this.props.metaId);
  };

  convertTableRowsIntoValue = () => this.state.rowData;

  renderArrayRows = () => {
    const arrayRowData = this.state.rowData;
    return arrayRowData.map((rowData, index) => {
      const uuid = generateUUID();
      return (
        <RowWrapper key={uuid}>
          {this.renderSingleRow(rowData, index)}
        </RowWrapper>
      )
    });
  };

  renderSingleRow = (data, index) => {
    const self = this;
    const { rowConfig, rowDataError } = this.state;
    const { renderData, level } = this.props;
    const renderCells = [];

    rowConfig.forEach((fieldConfig, idx) => {
      const { fieldId } = fieldConfig;
      const meta =
        fieldId !== 'delete'
          ? renderData.repeater
          : {
              label: 'Delete',
              type: 'icon',
              id: 'delete',
              description: 'Delete this row',
            };

      // const meta = fieldRenderData;

      const UnitComponent = componentMapByFormType[meta.type];
      // const defaultValue = dottie.get(selectedFieldData, meta.id);
      const defaultValue = this.state.rowData[index] || '';
      const getComponentProps = dottie.get(componentProps, meta.type) || {};

      const unitComp = (
        <div>
          <UnitComponent
            compProps={getComponentProps(meta, meta.id, defaultValue)}
            toggleTab={this.props.toggleTab}
            metaId={meta.id}
            key={`${fieldId + index}-${idx}`}
            renderData={meta}
            level={level + 1}
            updateFormData={this.props.updateFormData}
            onBlurHandler={this.validateAllFields}
            onChangeHandler={(val, id) =>
              this.rowDataChangeHandler(val, id, index)
            }
            errorMessage={rowDataError[index]}
          />
        </div>
      );

      renderCells.push(
        <div key={`cell${index}-${idx}`} className={`rowCell ${meta.type}-row`}>
          {unitComp}
        </div>,
      );
    });
    return renderCells;
  };

  onStringChangeHandler = value => {
    const { onChangeHandler, metaId, renderData } = this.props;

    let arrayData = [];
    if (renderData.repeater.type === 'number') {
      value.forEach((item, index) => {
        if (!Number.isNaN(item)) {
          arrayData[index] = Number(item);
        }
      });
      this.setState({ rowData: arrayData });
      onChangeHandler && onChangeHandler(arrayData, metaId);
    } else {
      this.setState({ rowData: [...value] });
      onChangeHandler && onChangeHandler(arrayData, [...value]);
    }
  };

  render() {
    const { renderData, className } = this.props;
    let error = null;
    const errorMessage = this.getErrorMessage();
    if (errorMessage) {
      error = <RenderError errorMessage={errorMessage} />;
    }
    if (renderData.repeater.type === 'string' || renderData.repeater.type === 'number') {
      return (
        <div className={className}>
          <SelectField
            mode="tags"
            label={renderData.label}
            style={{ width: '100%' }}
            onChange={this.onStringChangeHandler}
            options={[]}
            value={[...this.state.rowData]}
          />
        </div>
      );
    }

    const arrayRows = this.renderArrayRows();
    return (
      <div className={className}>
        <Heading>{renderData.label}</Heading>
        <DataWrapper>
          <div>{arrayRows}</div>
          {error}
          <ButtonWrapper>
            <A onClick={this.addRow}>
              <Icon type="plus-circle" style={iconStyles} />
              <LinkText>Add</LinkText>
            </A>
          </ButtonWrapper>
        </DataWrapper>
      </div>
    );
  }
}

ArrayInputField.propTypes = {
  className: PropTypes.string,
  level: PropTypes.number,
};

ArrayInputField.defaultProps = {
  level: 0,
};
