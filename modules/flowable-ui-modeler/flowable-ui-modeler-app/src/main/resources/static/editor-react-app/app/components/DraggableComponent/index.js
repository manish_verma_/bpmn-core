/**
 *
 * DraggableComponent
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { Icon } from 'antd';
import { Wrapper, Title } from './styles';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

function DraggableComponent(props) {
  const { formType, type, title } = props;

  return (
  	<Wrapper>
  		<Icon type="border" style={{
  			marginTop: '-2px',
  			verticalAlign: 'middle',
  			marginLeft: '9px', fontSize: '6px',}} />
  		<Title> {title} </Title>
  	</Wrapper>
   
  );
}

DraggableComponent.propTypes = {
  formType: PropTypes.string,
  type: PropTypes.string,
};

export default DraggableComponent;
