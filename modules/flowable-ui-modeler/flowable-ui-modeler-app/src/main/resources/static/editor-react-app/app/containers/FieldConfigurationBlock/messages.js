/*
 * FieldConfigurationBlock Messages
 *
 * This contains all the text for the FieldConfigurationBlock container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.FieldConfigurationBlock';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the FieldConfigurationBlock container!',
  },
});
