/*
 * RenderButton Messages
 *
 * This contains all the text for the RenderButton component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderButton.header',
    defaultMessage: 'This is the RenderButton component !',
  },
});
