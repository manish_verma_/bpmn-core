import styled from 'styled-components';
import { Button, Table, Modal } from 'antd';
import { Search } from 'brace';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const UpdateButton = styled(Button)`
  margin-top: 10px;
  float: right;
`;

export const AntTable = styled(Table)`
  margin-top: '20px';
  word-break: 'break-all';
  background-color: white;
  thead[class*='ant-table-thead'] tr {
    background-color: lightgrey !important;
  }
  div[class*='ant-table-body'] {
    margin: 0 !important;
  }
`;

export const AntModal = styled(Modal)`
  div[class*='ant-modal-body'] {
    padding-top: 0;
  }
`;

export const P = styled.p`
  font-size: x-large;
  font-family: 'OpenSans';
  margin: 0;
`;

export const RestoreContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 0px 10px;
`;
