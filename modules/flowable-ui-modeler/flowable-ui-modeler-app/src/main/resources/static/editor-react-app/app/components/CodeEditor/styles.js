import styled from 'styled-components';
import { Icon } from 'antd';

export const Wrapper = styled.div`
  &.ace_editor,
  .ace_editor div {
    font-family: monospace;
    padding: 2px;
  }
  p,
  div,
  label {
    font-family: monospace;
  }
`;

export const CodeWarning = styled.div`
  display: flex;
  padding: 10px;
  align-items: center;
  line-height: 1;
`;

export const EditorTitle = styled.p`
  margin-bottom: 5px;
  font-size: 20px;
`;

export const Success = styled.p`
  color: green;
`;

export const Error = styled.p`
  color: red;
`;

export const Hint = styled.p`
  color: 'blue;
`;

export const ExpandIcon = styled(Icon)`
  float: right;
  z-index: 100;
  top: 35px;
  position: relative;
  right: 25px;
  font-size: 20px;
  &.anticon {
    color: green;
  }
`;
