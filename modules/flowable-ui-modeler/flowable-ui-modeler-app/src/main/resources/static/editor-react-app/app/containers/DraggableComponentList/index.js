/**
 *
 * DraggableComponentList
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Container, Draggable } from 'react-smooth-dnd';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import DraggableComponent from 'components/DraggableComponent';
import makeSelectDraggableComponentList from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import * as componentListActions from './actions';

import * as data from '../../utilities/dummyData';
import * as dndUtils from '../../utilities/dndUtils';

import { componentConfigList } from '../FieldConfigurationBlock/config';
import { SectionHeading } from './styles';

/* eslint-disable react/prefer-stateless-function */
export class DraggableComponentList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    // call to get all the available component list and set in the store

    // dummy call to service to get the static components for now
    const componentList = [...componentConfigList];
    componentList.sort((a, b) => {
      if (a.title < b.title) {
        return -1;
      }
      if (a.title > b.title) {
        return 1;
      }
      return 0;
    })
    this.props.setComponentList(componentList);
  }

  render() {
    const props = this.props;

    const componentList = [...this.props.draggableComponentList.componentList];

    return (
      <div style={{color: '#a8bac1'}}>
        <SectionHeading>Component List</SectionHeading>
        <Container
          behaviour="copy"
          groupName="1"
          getChildPayload={i => componentList[i]}
          onDrop={e =>
            props.setComponentList(dndUtils.applyDrag(componentList, e))
          }
          dragClass="dragging-element"
          dropClass="dropping-element"
        >
          {componentList.map((p, i) => (
            <Draggable key={i}>
              <DraggableComponent title={p.title} formType={p.form_type} type={p.type} />
            </Draggable>
          ))}
        </Container>
      </div>
    );
  }
}

DraggableComponentList.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  draggableComponentList: makeSelectDraggableComponentList(),
});

function mapDispatchToProps(dispatch) {
  return {
    setComponentList: componentList =>
      dispatch(componentListActions.setComponentList(componentList)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'draggableComponentList', reducer });
const withSaga = injectSaga({ key: 'draggableComponentList', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(DraggableComponentList);
