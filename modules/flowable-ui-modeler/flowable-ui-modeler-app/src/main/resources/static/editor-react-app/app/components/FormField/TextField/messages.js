/*
 * TextField Messages
 *
 * This contains all the text for the TextField component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.TextField';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the TextField component!',
  },
});
