/*
 *
 * EditorLayoutConfigPlayground constants
 *
 */

export const DEFAULT_ACTION = 'app/EditorLayoutConfigPlayground/DEFAULT_ACTION';
export const SET_FORM_FIELDS = 'app/EditorLayoutConfigPlayground/SET_FORM_FIELDS';


export const enumFormTypes = ['radio', 'dropdown'];
