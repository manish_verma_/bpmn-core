import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the draggableComponentList state domain
 */

const selectDraggableComponentListDomain = state =>
  state.get('draggableComponentList', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by DraggableComponentList
 */

const makeSelectDraggableComponentList = () =>
  createSelector(selectDraggableComponentListDomain, substate =>
    substate.toJS(),
  );

export default makeSelectDraggableComponentList;
export { selectDraggableComponentListDomain };
