/*
 *
 * DraggableComponentList constants
 *
 */

export const DEFAULT_ACTION = 'app/DraggableComponentList/DEFAULT_ACTION';
export const SET_COMPONENT_LIST = 'app/DraggableComponentList/SET_COMPONENT_LIST';
