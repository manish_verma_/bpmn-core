import styled from 'styled-components';

export const AddVariableWrapper = styled.div`
  margin-bottom: 16px;
  float: left;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 10;
`;

