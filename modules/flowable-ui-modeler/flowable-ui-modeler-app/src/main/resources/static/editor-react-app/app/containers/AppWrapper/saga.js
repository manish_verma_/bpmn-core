import {
  call,
  put,
  select,
  take,
  takeLatest,
  takeEvery,
} from 'redux-saga/effects';

import qs from 'query-string';
import * as C from './constants';
import request from '../../utils/request';

const formGetRequestObject = data => {
  const paramString =
    data.params && qs.stringify(data.params)
      ? `?${qs.stringify(data.params)}`
      : '';

  const obj = {
    url: data.url + paramString,
    options: {
      method: 'GET',
      successCb: data.successCb,
      errorCb: data.errorCb,
      // headers: {},
    },
  };

  return obj;
};

const formPostRequestObject = (data, reqType) => {
  const isUploadMultipart = data.contentType === 'multipart/form-data';
  const contentType = isUploadMultipart
    ? 'multipart/form-data'
    : 'application/json;charset=UTF-8';
  let type = '';
  switch (reqType) {
    case 'post':
      type = 'POST';
      break;
    case 'put':
      type = 'PUT';
      break;
    case 'delete':
      type = 'DELETE';
      break;
    default:
      type = 'POST';
      break;
  }

  const headers = {};
  if (!isUploadMultipart) {
    headers['Content-Type'] = contentType;
  }

  const options = {
    method: type,
    body: isUploadMultipart ? data.params : JSON.stringify(data.params),
    headers,
    successCb: data.successCb,
    errorCb: data.errorCb,
  };

  return {
    url: data.url,
    options,
  };
};

// Individual exports for testing
export default function* appWrapperSaga() {
  // See example in containers/HomePage/saga.js

  const watcherGetNetworkSaga = yield takeEvery(C.GET_REQUEST, networkSaga);
  const watcherPostNetworkSaga = yield takeEvery(C.POST_REQUEST, networkSaga);
  const watcherPutNetworkSaga = yield takeEvery(C.PUT_REQUEST, networkSaga);
  const watcherDeleteNetworkSaga = yield takeEvery(
    C.DELETE_REQUEST,
    networkSaga,
  );
}

export function* networkSaga(action) {
  let response = {};
  const actionData = action.data.data;
  let requestData; // = requestUtils.formGetRequestObject
  if (action.type === C.GET_REQUEST) {
    requestData = formGetRequestObject(actionData);
  }
  if (action.type === C.POST_REQUEST) {
    requestData = formPostRequestObject(actionData, 'post');
  }
  if (action.type === C.PUT_REQUEST) {
    requestData = formPostRequestObject(actionData, 'put');
  }
  if (action.type === C.DELETE_REQUEST) {
    requestData = formPostRequestObject(actionData, 'delete');
  }
  /* FORMAT of requestData
    {
      type: SOMETHING_SOMETHING,
      data: {
        url: ,
        options: {
          method: 'POST/GET',
          params: `PAYLOAD`,
          successCb: `Fn`,
          headers: `HEADERS`
        }
      }
    }
  */

  response = yield call(request, requestData.url, requestData.options);
  if (response) {
    if (response.status && response.status === 401) {
    } else if (
      response.status &&
      response.status !== 200 &&
      response.status !== 'success' &&
      response.status !== 'OK' &&
      response.status !== true &&
      response.status !== 204
    ) {
      debugger;
      if (requestData.options.errorCb) {
        yield call(requestData.options.errorCb, response.response);
      }
    } else {
      debugger;
      yield call(requestData.options.successCb, response);
    }
  } else if (requestData.options.errorCb) {
    yield call(requestData.options.errorCb);
  }
}
