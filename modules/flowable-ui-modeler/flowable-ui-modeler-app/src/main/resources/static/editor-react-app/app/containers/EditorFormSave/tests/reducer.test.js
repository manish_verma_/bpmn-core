import { fromJS } from 'immutable';
import editorFormSaveReducer from '../reducer';

describe('editorFormSaveReducer', () => {
  it('returns the initial state', () => {
    expect(editorFormSaveReducer(undefined, {})).toEqual(fromJS({}));
  });
});
