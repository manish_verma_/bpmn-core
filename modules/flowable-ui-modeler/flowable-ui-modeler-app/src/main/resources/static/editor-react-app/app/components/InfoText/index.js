import React, { Component } from 'react';
import { Tooltip, Icon } from 'antd';
import { iconStyles } from './styles';

export default class InfoText extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { helperText, position } = { ...this.props };
    if (!helperText) {
      return null;
    }
    return (
      <Tooltip title={helperText}>
        <Icon type="question-circle-o" style={iconStyles} />
      </Tooltip>
    );
  }
}

InfoText.defaultProps = {};
