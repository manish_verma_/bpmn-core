import dottie from 'dottie';
import * as validationFns from '../../utilities/validations/validationFns';

export const primaryKeys = ['name', 'id', 'readOnly', 'options'];

export function getValidatedObj(value, elmId, renderData, formData) {
  const validObj = validationFns.validateElm(
    renderData[elmId],
    value,
    formData,
  );
  const currentRenderData = { ...renderData };
  // Set errorMessage and invalid flag
  currentRenderData[elmId].isValid = validObj.isValid;
  currentRenderData[elmId].errorMessage = validObj.errorMessage;
  return currentRenderData[elmId];
}

export function getValidatedRenderData(formData, renderData) {
  const currentRenderData = { ...renderData };
  Object.keys(renderData).forEach(key => {
    if (renderData[key]) {
      if (renderData[key].isHidden) {
        // If a field is hidden - skip the validation
        currentRenderData[key].isValid = true;
      } else if (key !== 'form_type') {
        const value =
          primaryKeys.indexOf(key) !== -1
            ? formData[key]
            : formData.params.meta[key];
        currentRenderData[key] = getValidatedObj(
          value,
          key,
          renderData,
          formData,
        );
      }
    } else {
      window.logger.error('META', `Couldn't find ${key} in render data`, {
        renderData,
      });
    }
  });
  return currentRenderData;
}

export const getPrimaryRenderData = renderData =>
  primaryKeys.filter(key => renderData[key]).map(key => renderData[key]);

export const getMetaKeysRenderData = renderData => {
  const metaFieldsData = [];
  Object.keys(renderData).forEach(key => {
    if (
      primaryKeys.indexOf(key) === -1 &&
      key !== 'form_type'
    ) {
      metaFieldsData.push(renderData[key]);
    }
  });
  return metaFieldsData;
};

export const formatRenderData = renderData => {
  const newRenderObj = {};
  renderData.metaKeys.forEach(metaObj => {
    const { metaKey, metaJson } = metaObj;
    newRenderObj[metaKey] = { ...metaJson };
  });
  return newRenderObj;
};

export const getFormDataObject = formData => {
  const obj = {};
  formData.forEach(field => {
    const id = dottie.get(field, 'id');
    if (id) {
      obj[id] = field;
    }
  });
  return obj;
};
