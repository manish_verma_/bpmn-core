/**
 *
 * Asynchronously loads the component for EnumDependent
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
