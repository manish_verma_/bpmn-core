import dottie from 'dottie';
import * as commonUtils from './commonUtils';

// Special component type for specific properties to be inserted into field JSON
const radioVar = 'radio';
const dropdownVar = 'dropdown';
const multiSelectVar = 'multiselect-dropdown';

const enumFormTypes = [radioVar, dropdownVar, multiSelectVar];

const OPTION_FIELD_TYPE = 'OptionFormField';
const TYPE_VALS = {
  [radioVar]: 'radio-buttons',
  [dropdownVar]: 'dropdown',
  [multiSelectVar]: 'dropdown',
};

const extObjForOptionsField = type => ({
  type: TYPE_VALS[type],
  fieldType: OPTION_FIELD_TYPE,
  options: [],
  optionsExpression: null,
  hasEmptyValue: null,
});

const setFieldUUID = field => {
  field.uuid = commonUtils.generateUUID();
};

export const createNewFormField = (componentConfig, addedIndex, addInNewrow) => {
  let dummyNewField = {
    fieldType: 'FormField',
    id: '',
    name: `New ${componentConfig.form_type}`,
    type: 'text',
    value: null,
    required: false,
    readOnly: false,
    overrideId: true,
    placeholder: null,
    params: {
      meta: {},
    },
    layout: null,
  };
  if (enumFormTypes.indexOf(componentConfig.form_type) !== -1) {
    dummyNewField = {
      ...dummyNewField,
      ...extObjForOptionsField(componentConfig.form_type),
    };
  }
  setFieldUUID(dummyNewField);
  const metaKeys = componentConfig.metaKeys;
  if (metaKeys && metaKeys.length) {
    const fieldMeta = dummyNewField.params.meta;
    metaKeys.forEach((obj, i) => {
      const objMeta = dottie.get(obj, 'metaJson');
      fieldMeta[objMeta.id] = dottie.get(objMeta, 'default');
    });
    // Add the order where the item is being dropped
    fieldMeta.order = addedIndex;
    fieldMeta.form_type = componentConfig.form_type || 'text';
    fieldMeta.type = componentConfig.type || 'string';
    fieldMeta.newRow = addInNewrow || false;
  }

  // Push the newly created field into sortableFormFields
  // modify the
  return dummyNewField;
};


// Generate a data URL image for the form snapshot
export const generateFormImage = async (containerId) => {
  let imageString = '';

  let deferred = new Promise ((resolve, reject) => {
    // As Flowable is using ahtml2canvas library for the div to image conversion, we are using it as is
    // flowable is using v0.4.1 of html2canvas library
    if (window.html2canvas) {
      window.html2canvas(document.querySelector('#' + containerId), {
        onrendered: (canvas) => {
          var scale = canvas.width / 300.0;
          var extra_canvas = document.createElement("canvas");
          extra_canvas.setAttribute('width', 300);
          extra_canvas.setAttribute('height', canvas.height / scale);
          var ctx = extra_canvas.getContext('2d');
          ctx.drawImage(canvas, 0, 0, canvas.width, canvas.height, 0, 0, 300, canvas.height / scale);
          imageString = extra_canvas.toDataURL("image/png");
          resolve(imageString);
        }
      });
    } else {
      resolve(imageString);
    }
  });

  return deferred;
}
