/**
 *
 * FieldConfigurationBlock
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import * as appActions from '../AppWrapper/actions';
import makeSelectFieldConfigurationBlock, {
  makeSelectFormData,
  makeSelectSelectedIndex,
  makeSelectRenderData,
  makeSelectPrimaryData,
  makeSelectMetaFieldsData,
  makeSelectIsMetaValid,
  makeSelectMetaErrorFlag,
} from './selectors';
import { makeSelectGlobalVariables } from '../AppWrapper/selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import {
  validateAndUpdateField,
  validateAllFields,
  updateMeta,
  updateFieldMeta,
  updateSelectedIndex,
  setFormMeta,
  updateField,
  updateRenderData,
} from './actions';
import FieldConfigurationContainer from './FieldConfigurationContainer';

/* eslint-disable react/prefer-stateless-function */
export class FieldConfigurationBlock extends React.PureComponent {
  render() {
    return (<FieldConfigurationContainer {...this.props} />);
  }
}

FieldConfigurationBlock.propTypes = {
  dispatch: PropTypes.func.isRequired,
  validateAllFields: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  fieldConfigurationBlock: makeSelectFieldConfigurationBlock(),
  formData: makeSelectFormData(),
  selectedIndex: makeSelectSelectedIndex(),
  renderData: makeSelectRenderData(),
  primaryData: makeSelectPrimaryData(),
  metaFieldData: makeSelectMetaFieldsData(),
  isMetaValid: makeSelectIsMetaValid(),
  metaErrorFlag: makeSelectMetaErrorFlag(),

  globalVariables: makeSelectGlobalVariables(),
});

function mapDispatchToProps(dispatch) {
  return {
    updateMeta: (value, key) => dispatch(updateMeta(value, key)),
    updateFieldMeta: (value) => dispatch(updateFieldMeta(value)),
    validateAndUpdateField: (value, key) =>
      dispatch(validateAndUpdateField(value, key)),
    validateAllFields: data => dispatch(validateAllFields(data)),
    updateSelectedIndex: data => dispatch(updateSelectedIndex(data)),
    removeSelectedIndex: () => dispatch(removeSelectedIndex()),
    setFormMeta: data => dispatch(setFormMeta(data)),
    updateField: data => dispatch(updateField(data)),
    updateRenderData: (value, key) => dispatch(updateRenderData(value, key)),
    
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'fieldConfigurationBlock', reducer });
const withSaga = injectSaga({ key: 'fieldConfigurationBlock', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(FieldConfigurationBlock);
