import { colors, fonts } from './styleVars';

export default {
  fontFamily: fonts.primaryFontFamily,
  palette: {
    textColor: colors.black,
    // primaryTextColor: '#10A1A1',
    primary1Color: colors.primaryBGColor,

  },

  slider: {
    trackSize: 4,
    trackColor: colors.basicLightFontColor,
    trackColorSelected: colors.basicLightFontColor,
    handleFillColor: colors.basicLightFontColor,
    handleSize: 15,
    handleSizeActive: 18,
    handleSizeDisabled: 15,
    selectionColor: colors.primaryBGColor,
  },
  stepper: {
    iconColor: colors.primaryBGColor,
  },
  datePicker: {
    // color: '#FFF',
     // textColor: '#FFF',
     // calendarTextColor: '#FFF',
    selectColor: colors.primaryBGColor,
     // selectTextColor: colors.primaryBGColor,
     // calendarYearBackgroundColor: colors.primaryBGColor,
    headerColor: colors.primaryBGColor,
  },
  select: {
    padding: undefined,
    // paddingRight: theme.spacing.unit * 4,
    height: undefined,
  },
  selectMenu: {
    lineHeight: undefined,
  },
};

