/**
 *
 * Asynchronously loads the component for Masters
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
