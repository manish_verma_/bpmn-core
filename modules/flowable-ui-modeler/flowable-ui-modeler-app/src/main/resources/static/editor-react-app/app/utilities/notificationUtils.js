import { notification } from 'antd';

const config = {
	placement: 'topRight',
	top: 50,
}
notification.config(config);

export const openNotificationWithIcon = (optionObj) => {
	const type = optionObj.type; // type could be success/error/warning/info

    notification[type]({
      message: optionObj.messages.title,
      description: optionObj.messages.description,
    });
 };