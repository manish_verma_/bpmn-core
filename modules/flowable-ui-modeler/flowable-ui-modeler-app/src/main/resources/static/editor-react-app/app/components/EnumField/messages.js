/*
 * EnumField Messages
 *
 * This contains all the text for the EnumField component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.EnumField.header',
    defaultMessage: 'This is the EnumField component !',
  },
});
