/*
 * RenderConditionInput Messages
 *
 * This contains all the text for the RenderConditionInput component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.RenderConditionInput';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RenderConditionInput component!',
  },
});
