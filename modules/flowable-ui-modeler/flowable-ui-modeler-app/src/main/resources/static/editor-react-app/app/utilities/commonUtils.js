export const generateUUID = () => {
  const crypto = window.crypto || window.msCrypto;
  const randomDigit = () => {
    if (crypto && crypto.getRandomValues) {
      const rands = new Uint8Array(1);
      crypto.getRandomValues(rands);
      return (rands[0] % 16).toString(16);
    }
    return ((Math.random() * 16) | 0).toString(16);
  };

  return 'xxxxxxxx-xxxx-4xxx-8xxx-xxxxxxxxxxxx'.replace(/x/g, randomDigit);
};

export const definedValueCheck = value =>
  typeof value !== 'undefined' && value !== null;

export const typeCheck = (value, type) => value && typeof value === type;


export const hasWhiteSpace = (string) => {
  return string.search(' ') !== -1;
}

export const IsJsonString = str => {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};
