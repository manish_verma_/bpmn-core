import React from 'react';
import { message } from 'antd';
import CodeEditor from '../../components/CodeEditor';

class ExportGlobalVariables extends React.Component {

  state = {
    globalVariables: "[]"
  };

  componentDidMount() {
    this.exportGlobalVariables();
  }

  exportGlobalVariables = () => {
    const url = FLOWABLE.APP_URL.exportGlobalVariableUrl();
    const reqData = {
      url,
      params: {},
      successCb: (response) => {
        const globalVariables = JSON.stringify(response, null, 2);
        this.setState(() => ({ globalVariables }));
        this.props.setglobalVariablelist(globalVariables);
      },
      errorCb: () => {
        // Show the notification for error
        message.error('Error in import');
      },
    };
    this.props.postRequest({ data: reqData });
  }

  render() {
    return (
      <CodeEditor mode="json" code={this.state.globalVariables} readOnly />
    );
  }
}

export default ExportGlobalVariables;