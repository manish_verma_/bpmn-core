import cloneDeep from 'lodash/cloneDeep';


const buildDummyRenderingJSON = () => {
	return {
		"data": {
	        "processInstanceId": "dummy-pid",
	        "data": {
	            "completedProcessTasks": [],
	            "applicationId": "DummAppId"
	        },
	        "formPreview": {

	        },
	        "taskId": "dummy-taskId"
	    },
		"errorMessage": "",
	    "message": "SUCCESS!",
	    "screenInfo": "formPreview",
	    "screenName": "Form Preview",
	    "status": 200
	}
}


export const buildRenderingJSON = (formFields) => {
	// build a new dummy preview rendering object (response like)
	let previewJson = buildDummyRenderingJSON();

	const formPreviewObj = previewJson.data.formPreview;
	// set the field arrangements in previewJson
	formFields.forEach((field, index) => {
		formPreviewObj[field.id] = cloneDeep(formFields[index]);
		formPreviewObj[field.id].isWritable = !formPreviewObj[field.id].readOnly;
		if (formPreviewObj[field.id] && formPreviewObj[field.id].options && Array.isArray(formPreviewObj[field.id].options)) {
			formPreviewObj[field.id].enumValues = [...formPreviewObj[field.id].options];
		}
	});

	return previewJson;
}


export const getUnaccountedFormIds = (formFields, currentGlobalVars) => {
	// get the ids present in the formFields
	const formIds = formFields.map((field) => field.id);
	// get the PIDs 
	const currentExisitingVars = currentGlobalVars.map((obj) => obj.processIdentifier);
	const unaccountedPIDs = _.difference(formIds, currentExisitingVars);
	return [...unaccountedPIDs];
}









