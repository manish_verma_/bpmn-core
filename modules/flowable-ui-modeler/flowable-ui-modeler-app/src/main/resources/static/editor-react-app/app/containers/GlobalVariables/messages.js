/*
 * GlobalVariables Messages
 *
 * This contains all the text for the GlobalVariables container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.GlobalVariables';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the GlobalVariables container!',
  },
});
