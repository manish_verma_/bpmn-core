/*
 *
 * FieldConfigurationBlock reducer
 *
 */

import { fromJS } from 'immutable';
import update from 'immutability-helper';
import {
  DEFAULT_ACTION,
  UPDATE_META,
  UPDATE_FIELD,
  UPDATE_FIELD_META,
  SET_SELECTED_INDEX,
  SET_RENDER_DATA,
  SET_META_VALID,
  SET_FORM_META,
  SET_SORTABLE_FORM_FIELDS,
  TOGGLE_META_ERROR
} from './constants';
import { primaryKeys } from './helper';

export const initialState = fromJS({
  formMeta: [],
  sortableFormFields: [],
  selectedIndex: null,
  renderData: null,
  isMetaValid: true,
  metaErrorFlag: false,
});

function fieldConfigurationBlockReducer(state = initialState, action) {
  const selectedIndex = state.get('selectedIndex');
  const formMeta = state.get('formMeta');
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case SET_FORM_META:
      return state.set('formMeta', action.data);
    case UPDATE_META: {
      const { key, value } = action;
      const updatedFormMeta =
        primaryKeys.indexOf(key) !== -1
          ? update(formMeta, {
            [selectedIndex]: { [key]: { $set: value } },
          })
          : update(formMeta, {
            [selectedIndex]: { params: { meta: { [key]: { $set: value } } } },
          });
      return state.set('formMeta', updatedFormMeta);
    }
    case UPDATE_FIELD_META: {
      const { value } = action;
      const updatedFormMeta = update( formMeta, {
        [selectedIndex]: { params: { meta: { $set: value } } },
      });
      return state.set('formMeta', updatedFormMeta);
    }
    case UPDATE_FIELD: {
      const { data: value } = action;
      const updatedFormMeta = update(formMeta, {
        [selectedIndex]: { $set: value },
      });
      return state.set('formMeta', updatedFormMeta);
    }
    case SET_SELECTED_INDEX: {
      return state.set('selectedIndex', action.data);
    }
    case SET_RENDER_DATA: {
      return state.set('renderData', action.data);
    }
    case SET_META_VALID:
      return state.set('isMetaValid', action.data);
    case TOGGLE_META_ERROR:
      return state.set('metaErrorFlag', action.data);
    default:
      return state;
  }
}

export default fieldConfigurationBlockReducer;
