import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tooltip } from 'antd';

import { Wrapper, Card, Button, Name, NameTag } from './styles';

export class TrashCard extends Component {
  static propTypes = {
    restoreMaster: PropTypes.func,
    details: PropTypes.object,
  };

  state = {};

  render() {
    const { details } = this.props;
    return (
      <Wrapper>
        <Card>
          <Tooltip title={details.masterName}>
            <NameTag>Master Name:</NameTag>
            <Name>{details.masterName}</Name>
          </Tooltip>
          <Tooltip title={details.masterSlug}>
            <NameTag>Master Slug:</NameTag>
            <Name>{details.masterSlug}</Name>
          </Tooltip>
          <Button
            type="primary"
            icon="undo"
            onClick={() => this.props.restoreMaster(details)}
          >
            Restore
          </Button>
        </Card>
      </Wrapper>
    );
  }
}

export default TrashCard;
