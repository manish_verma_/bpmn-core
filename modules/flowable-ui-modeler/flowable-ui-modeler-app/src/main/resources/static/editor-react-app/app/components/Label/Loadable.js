/**
 *
 * Asynchronously loads the component for Label
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
