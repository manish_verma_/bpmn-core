/**
 *
 * CheckboxField
 *
 */

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import { Checkbox } from 'antd';
// import messages from './messages';
import { checkBoxStyles, FieldWrapper } from './CheckboxStyles';
import RenderError from '../RenderError';
import InfoText from '../InfoText';

export default class CheckboxField extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  onChangeHandler = e => {
    const { onChangeHandler, metaId } = this.props;
    e.stopPropagation();
    onChangeHandler && onChangeHandler(e.target.checked, metaId);
  };

  // onBlurHandler = e => {
  //   const { metaId } = this.props;
  //   e.stopPropagation();
  //   this.props.onBlurHandler && this.props.onBlurHandler(e.target.checked, metaId);
  // };

  render() {
    // props is expected to have a componentProps object:

    const { errorMessage, compProps, className } = this.props;
    const { helperText, label, ...componentProps } = compProps;
    let error = null;

    const radioButtons = [];

    if (errorMessage) {
      error = <RenderError errorMessage={errorMessage} />;
    }

    return (
      <FieldWrapper key={componentProps.id} className={className}>
        <div className={className}>
          <Checkbox
            {...this.props.checkBoxStyles}
            {...componentProps}
            onChange={this.onChangeHandler}
            onBlur={this.onBlurHandler}
          >
            {label}
          </Checkbox>
          <InfoText helperText={compProps.helperText} />
          {error}
        </div>
      </FieldWrapper>
    );
  }
}

CheckboxField.propTypes = {};

CheckboxField.defaultProps = {
  checkBoxStyles,
};
