// export const PV_LIST = [
//   'firstName',
//   'lastName',
//   'middleName',
//   'age',
//   'gender',
//   'loanAmount',
//   'roi',
//   'tenure',
//   'pan',
//   'dob',
//   'city',
//   'state',
//   'country',
// ];

let PV_LIST = [];

export const VALIDATORS = [
  {
    id: 'isMinMax',
    name: 'Min Max',
  },
  {
    id: 'length',
    name: 'Length',
  },
  {
    id: 'pan',
    name: 'PAN',
  },
  {
    id: 'personalPan',
    name: 'Personal PAN',
  },
  {
    id: 'businessPan',
    name: 'Business PAN',
  },
  {
    id: 'percentage',
    name: 'Percentage'
  },
  {
    id: 'required',
    name: 'Required',
  },
  {
    id: 'mobile',
    name: 'Mobile Number',
  },
  // {
  //   id: 'date',
  //   name: 'Check for date between min and max date provided',
  // },
  {
    id: 'email',
    name: 'Email',
  },
  {
    id: 'pin-code',
    name: 'Pin Code',
  },
  {
    id: 'dob',
    name: 'DOB',
  },
  {
    id: 'business-tin',
    name: 'Business Tin',
  },
  {
    id: 'ifsc-code',
    name: 'IFSC Code',
  },
  {
    id: 'aadhar',
    name: 'Aadhar',
  },
  {
    id: 'otp',
    name: 'OTP',
  },
  {
    id: 'mpin',
    name: 'MPIN',
  },
];

export const CONSTANTS = {
  PV_LIST,
  VALIDATORS,
};

export const getOptionList = type => {
  const options = CONSTANTS[type];
  if (options && options.length) {
    return typeof options[0] === 'object'
      ? options
      : options.map(o => ({
          id: o,
          name: o,
        }));
  }
  return [];
};


export const setGlobalVariablesList = (data) => {
  const list = data.map((obj) => {
    return obj.processIdentifier;
  });
  CONSTANTS.PV_LIST = list;
}

export const updateGlobalVariablesList = (data) => {
  const list = data.map((obj) => {
    return obj.processIdentifier;
  });
  CONSTANTS.PV_LIST = Array.prototype.push.apply(CONSTANTS.PV_LIST, list);
}
