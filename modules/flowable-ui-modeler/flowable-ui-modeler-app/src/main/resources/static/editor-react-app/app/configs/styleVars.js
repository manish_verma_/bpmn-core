
// Color Variable:
export const globalColors = {
  green: '#009356',
  red: '#D2000D',
  dark: '#000000',
  orange: '#F59B4A',
  darkGray: '#5E5E5E',
  gray: '#ABABAB',
  lightGray: '#DDDDDD',
  white: '#FFFFFF',
};
const primaryRed = '#E31E24';

export const colors = {
  white: '#FFF',
  black: '#000',

  basicFontColor: '#363C44',
  basicLightFontColor: '#D7D7D7',

  primaryBGColor: '#FCA830',
  headerColor: '#FCA830',

  fillColor: '#f39a52',
  // primaryBGColor: '#183883',
  primaryFontColor: '#FFF',
  primaryDisableColor: '#dddddd',

  secondaryBGColor: '#F5F5F5',
  secondaryFontColor: '#4A4A4A',
  lighterColor: globalColors.lightGray,
  borderLight: '#F5F5F5',

  grey96: '#969696',
  errorColor: globalColors.red,
  requiredColor: globalColors.red,
  // appHeaderBG: '#183883',
  appHeaderBG: '#FFF',
  stepWrapperBGColor: '#FFF',
  journeyWrapperBGColor: '#F1F1F1',
  appHeaderFontColor: '#5B6168',

  transparentColor: 'transparent',
  linkColor: '#3b99fc',
  disabledLinkColor: '#83c0e0',
  hintTextColor: globalColors.gray,
  descriptionColor: '#6f6f6f',
  fbColor: '#3b5998',

  // TODO: remove redundant colors
  amountHeading: '#F59B4A',
  tabHeaderColor: '#b6b6b6',
  progressUnfilled: 'rgba(0, 0, 0, 0.12)',
  overlayColor: 'rgba(0,0,0,0.6)',
  labelColor: globalColors.darkGray,
  inputColor: globalColors.dark,
  underlineColor: globalColors.lightGray,
  reminderColor: 'rgba(245, 245, 245, 0.57)',
  inProgressColor: '#75bee9',
  closedColor: '#ababab',
  tableRowBG: '#FAFAFA',
  tableActionsLinkColor: primaryRed,
};

// TODO: move fonts to separate files for web and native
export const fonts = {
  primaryFontFamily: 'Lato-Regular',
  fontSize: 16,

  // Buttons
  buttonFontSize: 18,
  // buttonLineHeight: '36px',
  buttonLineHeight: 2,
  labelSize: 12,
  inputSize: 16,
  subHeadingSize: 18,
  mainHeadingSize: 24,
  paraSize: 14,
  headerSize: 24,
  nativeButtonFontSize: 14,

  mainHeader: 40,
  h1: 32,
  h2: 24,
  h3: 20,
  h4: 18,
  body: 16,
  description: 14,
  label: 12,
  tableHead: 14,

  subJourneyHeading: 20,

  hintOpacity: 0.7,
  hintFontSize: 12,
  hintFontStyle: 'italic',
};

// TODO: combine font and lineHeight
export const lineHeight = {
  h1: 40,
  h2: 32,
  h3: 30,
  h4: 26,
  body: 24,
  description: 22,
  label: 18,
};

export const sizes = {
  containerWidth: '100%',
  appPaddingLeft: 20,
  appPaddingRight: 20,
  appPaddingHorizontal: 20,
  headerPadding: 50,
};

const vars = {
  colors,
  fonts,
  sizes,
};

export default vars;
