import React from 'react';
import PropTypes from 'prop-types';
import { Collapse } from 'antd';

const { Panel } = Collapse;

const customPanelStyle = {
  background: 'transparent',
  borderRadius: 0,
  marginBottom: 24,
  overflow: 'hidden',
};

const wrapperStyle = {
  background: 'transparent',
};

const AccordionField = props => (
  <Collapse bordered={false} style={wrapperStyle}>
    <Panel header={props.label} style={customPanelStyle} />
  </Collapse>
);

AccordionField.propTypes = {
  label: PropTypes.string,
};

export default AccordionField;
