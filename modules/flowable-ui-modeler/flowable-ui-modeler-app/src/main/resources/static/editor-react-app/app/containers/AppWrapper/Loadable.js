/**
 *
 * Asynchronously loads the component for AppWrapper
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
