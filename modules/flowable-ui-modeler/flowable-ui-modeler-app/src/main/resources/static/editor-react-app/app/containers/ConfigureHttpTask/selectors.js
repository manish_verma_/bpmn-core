import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the configureHttpTask state domain
 */

const selectConfigureHttpTaskDomain = state =>
  state.get('configureHttpTask', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by ConfigureHttpTask
 */

const makeSelectConfigureHttpTask = () =>
  createSelector(selectConfigureHttpTaskDomain, substate => substate.toJS());

export default makeSelectConfigureHttpTask;
export { selectConfigureHttpTaskDomain };
