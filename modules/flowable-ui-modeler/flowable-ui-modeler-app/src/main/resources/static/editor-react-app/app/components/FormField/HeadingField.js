/**
 *
 * HeadingField
 *
 */

import React from 'react';
import styled from 'styled-components';

const TextWrapper = styled.h4`
  font-size: 16px;
  font-weight: 500;
`;

export default class HeadingField extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    const { label } = this.props;
    return <TextWrapper>{label}</TextWrapper>;
  }
}
