import { IsJsonString } from '../utilities/commonUtils';

/*
This service contains the props passed to renderer components
based upon their form_type
*/
const MATERIAL_KEY_MAP = {
  placeholder: 'hintText',
  label: 'floatingLabelText',
};

export const inputRenderSetting = {
  placeHolder: true,
  floatingText: true,
  floatingLabelFixed: true,
  materialTheme: false,
};

export const dropdownRenderSetting = {
  hintText: true,
  floatingText: true,
  floatingLabelFixed: true,
};

// Render behaviour of components
// This return an object in compliance with Material UI setting of input text
export function getInputBasicRenderObj(data) {
  const obj = {};
  if (inputRenderSetting.materialTheme) {
    obj.floatingLabelText = inputRenderSetting.floatingText && data.label;
    obj.floatingLabelFixed = inputRenderSetting.floatingLabelFixed;
    obj.hintText = data.placeHolder || '';
    obj.helperText = data.description || '';
    if (data.required || data.required === 'true') {
      obj.floatingLabelText = `${obj.floatingLabelText}*`;
    }
  } else {
    obj.label =
      data.label && (data.required || data.required === 'true')
        ? `${data.label}*`
        : data.label;
    obj.placeholder = data.placeholder || '';
    obj.helperText = data.description || '';
  }

  return obj;
}

// Render behaviour of components
// This return an object in compliance with Material UI setting of dropdown
export function getDropdownBasicRenderObj(data) {
  const obj = {};

  if (inputRenderSetting.materialTheme) {
    obj.floatingLabelText = inputRenderSetting.floatingText && data.label;
    obj.floatingLabelFixed = inputRenderSetting.floatingLabelFixed;
    obj.hintText = data.placeHolder || '';
    obj.helperText = data.description || '';
    if (data.required || data.required === 'true') {
      obj.floatingLabelText = `${obj.floatingLabelText}*`;
    }
  } else {
    obj.label =
      data.label && (data.required || data.required === 'true')
        ? `${data.label}*`
        : data.label;
    obj.placeholder = data.placeholder || '';
    obj.helperText = data.description || '';
  }

  return obj;
}

const getInputFieldProps = (data, elmId, defaultValue) => {
  const basicRenderObj = getInputBasicRenderObj(data);

  const propObj = {
    ...basicRenderObj,
    id: `ku-text-${elmId}`,
    value: (typeof defaultValue === 'object') ? JSON.stringify(defaultValue) : defaultValue,
    disabled: !!data.disabled,
    type: data.type,
  };
  return propObj;
};

const getCheckboxProps = (data, elmId, defaultValue) => {
  const propObj = {
    id: `ku-checkbox-${elmId}`,
    checked: !!defaultValue,
    disabled: !!data.disabled,
    label: data.label,
    helperText: data.description || '',
  };
  return propObj;
};

const getDropdownProps = (data, elmId, defaultValue) => {
  const basicRenderObj = getDropdownBasicRenderObj(data);
  const propObj = {
    ...basicRenderObj,
    id: `ku-dropdown-${elmId}`,
    value: defaultValue,
    disabled: !!data.disabled,
  };
  return propObj;
};

const getArrayProps = (data, elmId, defaultValue) => {
  const propObj = {
    id: `ku-array-${elmId}`,
    value: defaultValue,
    disabled: !!data.disabled,
  };
  return propObj;
};

const getObjectProps = (data, elmId, defaultValue) => {
  const propObj = {
    id: `ku-object-${elmId}`,
    value: defaultValue,
    disabled: !!data.disabled,
  };
  return propObj;
};

const getEnumProps = (data, elmId, defaultValue) => {
  const propObj = {
    id: `ku-enum-${elmId}`,
    value: defaultValue,
    disabled: !!data.disabled,
  };
  return propObj;
};

const getIconProps = (data, elmId, defaultValue) => {
  const propObj = {
    label: data.label,
    type: data.type,
  };

  return propObj;
};

const getEnumDependentProps = (data, elmId, defaultValue) => {
  const propObj = {
    id: `ku-object-${elmId}`,
    value: defaultValue,
    name: data.label,
    disabled: !!data.disabled,
  };
  return propObj;
};

export default {
  string: getInputFieldProps,
  number: getInputFieldProps,
  boolean: getCheckboxProps,
  dropdown: getDropdownProps,
  multiselect: getDropdownProps,
  array: getArrayProps,
  object: getObjectProps,
  enum: getEnumProps,
  icon: getIconProps,
  enumDependent: getEnumDependentProps,
};
