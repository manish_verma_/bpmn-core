import styled from 'styled-components';

export const LabelWrapper = styled.div`
  padding: 4px 0px;
`;

export const LabelText = styled.label`
  margin-bottom: 0px;
  margin-right: 5px;
  vertical-align: middle;
  font-size: 11px;
`;

export const iconStyles = {
  verticalAlign: 'middle',
};
