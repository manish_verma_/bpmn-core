import styleVars from 'configs/styleVars';
import styled from 'styled-components';

const fonts = styleVars.fonts,
  colors = styleVars.colors;

export const checkBoxStyles = {
  style: { // This is the root element style
    //width: '100%',
  },
  labelStyle: {
    fontSize: fonts.fontSize,
    color: colors.basicFontColor,
  },
  iconStyle: {
    color: colors.primaryBGColor,
    fill: colors.primaryBGColor,
  },
};

export const FieldWrapper = styled.div`
  position: relative;
  margin-bottom: 10px;
  padding-top: 10px;
  span.tooltip-wrapper {
    position: absolute;
    right: 8px;
    top: 0px;
  }
`;