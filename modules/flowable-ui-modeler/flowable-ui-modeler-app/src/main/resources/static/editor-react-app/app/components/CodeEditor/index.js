import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AceEditor from 'react-ace';

import 'brace/mode/json';
import 'brace/mode/java';
import 'brace/mode/xml';
import 'brace/theme/monokai';
import 'brace/theme/github';
import 'brace/ext/language_tools';

import { Wrapper, EditorTitle, ExpandIcon } from './styles';

export class CodeEditor extends Component {
  static propTypes = {
    title: PropTypes.string,
    height: PropTypes.string,
    placeholder: PropTypes.string,
    mode: PropTypes.string,
    onChange: PropTypes.func,
    code: PropTypes.string,
    readOnly: PropTypes.bool,
  };

  static defaultProps = {
    title: '',
    placeholder: 'Enter your script.',
    height: '',
    onChange: () => {},
    readOnly: false,
  };

  state = {
    showInfo: false,
    min: true,
  };

  toggleModal = () => {
    this.setState(prevState => ({
      showInfo: !prevState.showInfo,
    }));
  };

  toggleExpand = () => {
    this.setState(prevState => ({
      min: !prevState.min,
    }));
  };

  render() {
    const { title, placeholder, mode, theme } = this.props;
    return (
      <Wrapper>
        {!!title && <EditorTitle>{title}</EditorTitle>}
        <ExpandIcon
          type={this.state.min ? 'arrows-alt' : 'shrink'}
          onClick={this.toggleExpand}
        />

        <AceEditor
          {...this.props}
          mode={mode}
          name="script-editor"
          theme={theme || "monokai"}
          fontSize={14}
          onLoad={this.onLoad}
          placeholder={placeholder}
          onChange={this.props.onChange}
          width="100%"
          newLineMode="auto"
          height={this.state.min ? '200px' : '300px'}
          value={this.props.code}
          editorProps={{ $blockScrolling: true }}
          setOptions={{
            enableLiveAutocompletion: true,
            showLineNumbers: true,
            tabSize: 2,
          }}
        />
      </Wrapper>
    );
  }
}

export default CodeEditor;
