import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Upload, Icon, message } from 'antd';

class FileUpload extends Component {
  static propTypes = {
    putRequest: PropTypes.func,
    postRequest: PropTypes.func,
    record: PropTypes.object,
    masterData: PropTypes.object,
    toggleEditModal: PropTypes.func,
    isNewVersion: PropTypes.bool,
  };

  static defaultProps = {};

  state = {
    fileList: [],
  };

  handleUpload = (uploadFile, fileList) => {
    // Limit the number of uploaded files
    // Only to show One recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-1);
    this.setState(() => ({ fileList }));
  };

  onRemoveFile = () => {
    this.setState(() => ({ fileList: [] }));
  };

  uploadFile = ({ saveToDB }) => {
    const { fileList } = this.state;
    const { record, masterData } = this.props;

    const file = fileList[0];
    const formData = new FormData();
    formData.append('file', file);
    let query;
    if (this.props.isNewVersion) {
      query = `${
        record.masterSlug
      }?saveToDatabase=${saveToDB}&createNewVersion=true`;
    } else {
      query = `${record.masterSlug}?saveToDatabase=${saveToDB}`;
    }

    const url = MASTER.APP_URL.updateDataStorage(query);

    let requestType = 'put';
    if (!masterData.dataStorage) {
      requestType = 'post';
    }
    // Build the request data to update dataDefinition
    const reqData = {
      url,
      contentType: 'multipart/form-data',
      params: formData,
      successCb: response => {
        if (response && saveToDB) {
          const SuccessMessage = this.props.isNewVersion
            ? 'New Version Created'
            : 'File Updated';
          message.success(SuccessMessage);
          this.props.toggleEditModal();
        } else {
          message.success('File Validation SUCCESS');
        }
      },
      errorCb: () => {
        // notifUtils.openNotificationWithIcon(errorNotifObj);
        message.error('Check Data Storage');
      },
    };
    if (requestType === 'put') {
      this.props.putRequest({ data: reqData });
    } else {
      this.props.postRequest({ data: reqData });
    }
  };

  render() {
    return (
      <div>
        <div>
          <Upload
            onRemove={removefile => this.onRemoveFile(removefile)}
            beforeUpload={(uploadFile, fileList) => {
              this.handleUpload(uploadFile, fileList);
              return false;
            }}
            accept=".xls,.csv"
            fileList={this.state.fileList}
          >
            <Button style={{ marginTop: '10px' }}>
              <Icon type="upload" /> Select File
            </Button>
            <p style={{ color: 'darkgreen' }}>Select only csv or xls File...</p>
          </Upload>
        </div>
        <div style={{ marginTop: '10px' }}>
          <Button
            onClick={() => this.uploadFile({ saveToDB: false })}
            disabled={this.state.fileList.length === 0}
          >
            Validation
          </Button>
          <Button
            onClick={() => this.uploadFile({ saveToDB: true })}
            disabled={this.state.fileList.length === 0}
            style={{ marginLeft: 10 }}
          >
            Send
          </Button>
        </div>
      </div>
    );
  }
}

export default Form.create({ name: 'FileUpload' })(FileUpload);
