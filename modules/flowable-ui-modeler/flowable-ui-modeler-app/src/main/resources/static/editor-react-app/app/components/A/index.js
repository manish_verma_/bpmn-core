/**
 * A link to a certain page, an anchor tag
 */

import styled from 'styled-components';

const A = styled.a`
  color: #1890ff;

  &:hover {
    color: #40a9ff;
  }
`;

export default A;
