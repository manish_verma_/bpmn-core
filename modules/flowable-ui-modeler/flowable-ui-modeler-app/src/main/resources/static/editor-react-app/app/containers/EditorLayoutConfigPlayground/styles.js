import styled from 'styled-components';
import { Draggable, Container } from 'react-smooth-dnd';

export const ComponentColumnWrapper = styled(Draggable)`
  border: 1px solid transparent;
  min-height: 50px;
  position: relative;
  &:hover,
  &.selectedComponent {
    border: 1px dashed #d9d9d9;
    .removeBlock {
      opacity: 1;
    }
  }
  &.selectedComponent {
    background-color: #EFF4F8;
  }
  .removeBlock {
    opacity: 0;
    position: absolute;
    top: 4px;
    right: 10px;
    z-index: 10;
    transition: 0.2s all linear;
  }
`;

export const FormDetailsSection = styled.div`
    margin-bottom: 15px;
    box-shadow: 0px 1px 15px rgba(33, 26, 26, 0.2);
    margin-right: -15px;
    margin-left: -15px;
    margin-top: -15px;
    padding: 15px 15px 15px 15px;
    background: #FFF;
    font-size: 12px;
`;

export const ComponentWrapper = styled.div`
  height: 100%;
  padding: 0px 15px;
  margin: 0px -15px;
`;

export const Wrapper = styled.div`
  background-color: transparent;
  padding: 15px;
  margin-bottom: 50px;
  border-bottom: 1px solid #Ccc;
  padding-bottom: 40px;
`;

export const SecondaryWrapper = styled.div`
  background-color: #F9F9F9;
`

export const PlaceholderContainer = styled.div`
  font-size: 20px;
  text-align: center;
  color: #c3c3c3;
  min-height: 80px;
  border: 1px dashed #0a8ea0;
  background: #FFF;
`;
