/**
 *
 * Asynchronously loads the component for EditorFormSave
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
