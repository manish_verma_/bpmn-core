import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the globalVariables state domain
 */

const selectGlobalVariablesDomain = state =>
  state.get('globalVariables', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by GlobalVariables
 */

const makeSelectGlobalVariables = () =>
  createSelector(selectGlobalVariablesDomain, substate => substate.toJS());

export default makeSelectGlobalVariables;
export { selectGlobalVariablesDomain };
