import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the editorFormSave state domain
 */

const selectEditorFormSaveDomain = state =>
  state.get('editorFormSave', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by EditorFormSave
 */

const makeSelectEditorFormSave = () =>
  createSelector(selectEditorFormSaveDomain, substate => substate.toJS());

export default makeSelectEditorFormSave;
export { selectEditorFormSaveDomain };
