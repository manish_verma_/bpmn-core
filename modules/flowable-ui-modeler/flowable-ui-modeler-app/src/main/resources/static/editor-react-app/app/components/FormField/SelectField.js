/**
 *
 * SelectField
 *
 */

import React from 'react';
import { Select } from 'antd';
import Field from '../Field';
const { Option } = Select;
import { definedValueCheck } from '../../utilities/commonUtils';

export default class SelectField extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    const {
      errorMessage,
      className,
      helperText,
      options,
      label,
      value,
      ...otherProps
    } = this.props;

    const menuItems =
      options && options.length
        ? options.map(
          option =>
            definedValueCheck(option.name) && definedValueCheck(option.id) ? (
            // option.name && option.id ? (
              <Option value={option.id} key={option.id}>
                {option.name}
              </Option>
            ) : null,
        )
        : [];
    let valueProp = {};
    // if (value) {
      valueProp = {
        value,
      };
    // }
    return (
      <Field label={label} errorMessage={errorMessage} helperText={helperText}>
        <Select style={{ width: '100px' }} {...otherProps} {...valueProp}>
          {menuItems}
        </Select>
      </Field>
    );
  }
}

SelectField.defaultProps = {
  mode: 'default',
};
