import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the editorLayoutConfigPlayground state domain
 */

const selectEditorLayoutConfigPlaygroundDomain = state =>
  state.get('editorLayoutConfigPlayground', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by EditorLayoutConfigPlayground
 */

const makeSelectEditorLayoutConfigPlayground = () =>
  createSelector(selectEditorLayoutConfigPlaygroundDomain, substate =>
    substate.toJS(),
  );

export default makeSelectEditorLayoutConfigPlayground;
export { selectEditorLayoutConfigPlaygroundDomain };
