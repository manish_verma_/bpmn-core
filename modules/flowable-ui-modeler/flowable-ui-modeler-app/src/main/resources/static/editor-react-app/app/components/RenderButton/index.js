/**
 *
 * RenderButton
 *
 */

import React from 'react';
// import styled from 'styled-components';

import RenderButtonComponent from './RenderButtonComponent';

class RenderButton extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  render() {
    return <RenderButtonComponent {...this.props} />;
  }
}

RenderButton.propTypes = {};

export default RenderButton;
