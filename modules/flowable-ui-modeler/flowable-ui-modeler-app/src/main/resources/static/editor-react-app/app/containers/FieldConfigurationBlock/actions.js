/*
 *
 * FieldConfigurationBlock actions
 *
 */

import {
  DEFAULT_ACTION,
  VALIDATE_AND_UPDATE_FIELD,
  VALIDATE_ALL_FIELDS,
  UPDATE_FIELD,
  UPDATE_META,
  UPDATE_FIELD_META,
  SET_RENDER_DATA,
  SET_SELECTED_INDEX,
  UPDATE_SELECTED_INDEX,
  REMOVE_SELECTED_INDEX,
  SET_META_VALID,
  SET_FORM_META,
  UPDATE_RENDER_DATA,
  SET_SORTABLE_FORM_FIELDS,
  TOGGLE_META_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

// to validate single meta key
export function validateAndUpdateField(value, key) {
  return {
    type: VALIDATE_AND_UPDATE_FIELD,
    value,
    key,
  };
}

// to update all meta keys in a field
export function validateAllFields(data) {
  return {
    type: VALIDATE_ALL_FIELDS,
    data,
  };
}

// to update entire meta object
export function updateField(data) {
  return {
    type: UPDATE_FIELD,
    data,
  };
}

// to update any prop in meta
export function updateMeta(value, key) {
  return {
    type: UPDATE_META,
    key,
    value,
  };
}

// to update the whole meta for a field
export function updateFieldMeta(value) {
  return {
    type: UPDATE_FIELD_META,
    value,
  };
}

export function setRenderData(data) {
  return {
    type: SET_RENDER_DATA,
    data,
  };
}

export function setSelectedIndex(data) {
  return {
    type: SET_SELECTED_INDEX,
    data,
  };
}

// handle updation of render data before setting selected field
export function updateSelectedIndex(data) {
  return {
    type: UPDATE_SELECTED_INDEX,
    data,
  };
}

export function removeSelectedIndex() {
  return {
    type: REMOVE_SELECTED_INDEX,
  };
}

export function setIsMetaValid(data) {
  return {
    type: SET_META_VALID,
    data,
  };
}

export function toggleMetaError(data) {
  return {
    type: TOGGLE_META_ERROR,
    data,
  };
}

// set meta for complete form
export function setFormMeta(data) {
  return {
    type: SET_FORM_META,
    data,
  };
}

export function updateRenderData(value, key) {
  return {
    type: UPDATE_RENDER_DATA,
    value,
    key,
  };
}
