/**
 * 
 */
package org.flowable.engine.impl.form;

import org.flowable.engine.form.AbstractFormType;

/**
 * @author manjunathch
 *
 */
public class EncryptFormTypes extends AbstractFormType {

    private static final long serialVersionUID = 1L;

    @Override
    public String getName() {
        return "encrypt";
    }

    public String getMimeType() {
        return "text/plain";
    }

    @Override
    public Object convertFormValueToModelValue(String propertyValue) {
        return propertyValue;
    }

    @Override
    public String convertModelValueToFormValue(Object modelValue) {
        return (String) modelValue;
    }

}
