
 mvn clean install -DskipTests=true -f ../modules/flowable-ui-idm/
 mvn clean install -DskipTests=true -f ../modules/flowable-ui-modeler/
 mvn clean install -DskipTests=true -f ../modules/flowable-ui-task/
 mvn clean install -DskipTests=true -f ../modules/flowable-ui-admin/

echo "================================================"
read -p "Build completed successfullym do you want to push to artifact Y(Yes), N(No)?" isPush
#convert to upppercase
isPush=${isPush,,}
if [ $isPush == "Y" ]
then
echo "Do you want to push as new version? Enter version"
read version
echo "started pushing IDM to artifactory"
curl -H 'X-JFrog-Art-Api:AKCp5dKENhKiLkJdDRjSEg1y9qRpRWp1E4ycb9JgYoR2Bc1hWoKMpErxyJr5xKTQTeYdCHWk9' -T ../modules/flowable-ui-idm/flowable-ui-idm-app/target/*.war "http://artifactory.getlend.in/artifactory/product/workflow/$version/wf-iam.war"
echo "started pushing PCP to artifactory"
curl -H 'X-JFrog-Art-Api:AKCp5dKENhKiLkJdDRjSEg1y9qRpRWp1E4ycb9JgYoR2Bc1hWoKMpErxyJr5xKTQTeYdCHWk9' -T ../modules/flowable-ui-modeler/flowable-ui-modeler-app/target/*.war "http://artifactory.getlend.in/artifactory/product/workflow/$version/wf-pcp.war"
echo "started pushing TASK to artifactory"
curl -H 'X-JFrog-Art-Api:AKCp5dKENhKiLkJdDRjSEg1y9qRpRWp1E4ycb9JgYoR2Bc1hWoKMpErxyJr5xKTQTeYdCHWk9' -T ../modules/flowable-ui-task/flowable-ui-task-app/target/*.war "http://artifactory.getlend.in/artifactory/product/workflow/$version/wf-task.war"
echo "started pushing ADMIN to artifactory"
curl -H 'X-JFrog-Art-Api:AKCp5dKENhKiLkJdDRjSEg1y9qRpRWp1E4ycb9JgYoR2Bc1hWoKMpErxyJr5xKTQTeYdCHWk9' -T ../modules/flowable-ui-admin/flowable-ui-admin-app/target/*.war "http://artifactory.getlend.in/artifactory/product/workflow/$version/wf-admin.war"
else
 echo "NP"
fi



